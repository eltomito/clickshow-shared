#define BOOST_TEST_MODULE TextEncoder test
#include <boost/test/unit_test.hpp>

#include "shared/src/serialize_double.h"

#include <string>
#include <cfloat>

void test_one( double d )
{
	unsigned char buf[ DBL_SERIALIZED_BYTES ];

	size_t len = SerializeDouble::serialize_double( d, buf, sizeof( buf ) );
	BOOST_TEST( ( len == sizeof( buf ) ), "serialization of " << d << " failed! len = " << len << "!");

	size_t bytes_used;
	double e = SerializeDouble::deserialize_double( buf, sizeof( buf ), bytes_used );
	BOOST_TEST( ( bytes_used == sizeof( buf ) ), "deserialization of " << d << " failed! bytes_used = " << bytes_used << "!");

	if( !std::isnan( d ) ) {
		BOOST_TEST( ( d == e ), "orig = " << d << " but new = " << e << "!" );
	} else {
		BOOST_TEST( std::isnan( e ), "original is NaN but the result is " << e << "!");
	}
};

void test_array( double *d, size_t len )
{
	for( int i = 0; i < len; ++i ) {
		test_one( d[i] );
	}
}

BOOST_AUTO_TEST_CASE( Test_serialize_double )
{
	double ddd[] = {
		1.0, -1.0, 2.0, -2.0, 1000, -1000, 132456789, -123456789,
		0.1, (double)1/(double)1000, (double)1/(double)666, (double)DBL_MAX, (double)DBL_MIN,
		std::nan(""), (double)0.00001 / (double)DBL_MAX, (double)DBL_MAX / (double)0.00001,
		0.0, (double)0.00001 / (double)DBL_MIN, (double)DBL_MIN / (double)0.00001
	};

//	test_one( 0.1 );
	test_array( ddd, sizeof(ddd) / sizeof(double) );
};

