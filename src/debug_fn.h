/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Debug_Fn_h
#define _Debug_Fn_h

/*
#include <FL/Fl_Group.H>
#include <FL/names.h>
*/

//#include "clicky_config.h"

/*
 */
void __clicky_debug_message(
		const char* file, 
		int line, 
		const char* function, 
		const char *string, ...);

void __clicky_debug_message2(
		const char* file, 
		int line, 
		const char* function,
		unsigned int logModule,
		const char *string, ...);

/*
void clicky_debug_list_group( Fl_Group *g );
 */

#ifndef WIN32

#define clicky_debug_message(...) __clicky_debug_message( __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#define clicky_debug_message2( _logModule,...) __clicky_debug_message2( __FILE__, __LINE__, __FUNCTION__, _logModule, __VA_ARGS__)

#else //WIN32

#define clicky_debug_message(...) __clicky_debug_message( __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#define clicky_debug_message2( _logModule,...) __clicky_debug_message2( __FILE__, __LINE__, __FUNCTION__, _logModule, __VA_ARGS__)

#endif //WIN32

/*
#else //CLICKY_LOG_LEVEL
#define clicky_debug_message(...)
#endif //CLICKY_LOG_LEVEL
*/

#endif	//Clicky_Debug_h
