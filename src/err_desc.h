/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Err_Desc_h_
#define _Err_Desc_h_

#include "err.h"
#include <string>
#include <vector>

class ErrDesc;

typedef std::vector<ErrDesc> ErrDescsT;

class ErrDesc
{
public:
	static const int LOC_LINE = 0;
	static const int LOC_SUB = 1;
	static const int NUM_LOC_TYPES = 2;

	ErrDesc( int _err, int _where, const std::string &_line = "", int _loc_type = LOC_SUB )
		: err(_err), where(_where), line(_line), loc_type(_loc_type) {};
	~ErrDesc() {};
	void ToString( std::string &dst ) const;
	std::string ToString() const { std::string dst; ToString( dst ); return dst; };

	static void DescsToString( const ErrDescsT &errs, std::string &dst );
	static std::string DescsToString( const ErrDescsT &errs ) { std::string dst; DescsToString( errs, dst ); return dst; };;

	int			err;
	int			where;
	int 			loc_type;
	std::string	line;

	static const char *loc_type_names[NUM_LOC_TYPES + 1];
};

#endif //_Err_Desc_h_
