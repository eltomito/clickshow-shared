/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _United_Types_h_
#define _United_Types_h_ 1

// ============== arbitrary type union =================

/** an union of any types including classes with non-trivial constructors.
 * 
 * This template declares a chunk of memory as long as the longest type supplied.
 * No constructors are ever called.
 * @param Type1, Type2, ... the types to make a union of.
 */
template <class T, class... Types> union UnitedTypes
{
	char	bytes[ sizeof(T) ];
	union {
		UnitedTypes<Types...> moreBytesMaybe;
	};
};

template <class T> union UnitedTypes<T>
{
	union {
		long	alignment;
		char	bytes[ sizeof(T) ];
	};
};

#endif //_United_Types_h_
