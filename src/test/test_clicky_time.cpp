/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../clicky_time.h"

#include <string>

int main() {

	ClickyTime now( true );
	std::string res = now.ToString();
	printf("Now is the time! %s\n", res.c_str() );

	ClickyTime other;
	char buf[ ClickyTime::serializedLength ];
	size_t outlen = now.Serialize( buf );
	size_t inlen = other.Unserialize( buf );

	std::string res2 = other.ToString();
	printf("Now anotha time! %s\n", res2.c_str() );
	printf("inlen = %ld, outlen = %ld. Are they the same? %s!\n", inlen, outlen, now == other ? "YES" : "NO" );

	ClickyTime t3 = now;
	t3 += 1000;
	printf("Now anotha time! %s\n", t3.ToString().c_str() );

	ClickyTime t4 = t3;
	t4 -= 2000;
	printf("Now anotha time! %s\n", t4.ToString().c_str() );

	printf("time diff: %ld\n", (long)( t4 - t3 ));
}

