/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../list_searcher.h"

#include <list>
#include <string>
#include <algorithm>

class NumberedString
{
public:
	NumberedString( int n = -1, const std::string &s="" ) : num(n), str(s) {};
	std::string	str;
	int					num;
};

typedef std::list<NumberedString> listT;
typedef listT::iterator iterT;
typedef listT::const_iterator citerT;

class findByNum
{
public:
	findByNum( int n ) : num(n) {};
	bool operator()(const NumberedString &ns) { return ns.num == num; };
	int num;
};

NumberedString *findNumInList( int n, const listT &lst )
{
	findByNum fbn(n);
	citerT cit;
	NumberedString *res;

	cit = std::find_if( lst.begin(), lst.end(), fbn );
	if( cit == lst.end() ) {
		printf("not found: #%d\n.", n);
		res = NULL;
	} else {
		printf("found: #%d: \"%s\".\n", cit->num, cit->str.c_str() );
		res = (NumberedString *)&(*cit);
	}
	return res;
}

NumberedString *findNumInList_Short( int n, const listT &lst )
{
	findByNum fbn(n);
	typedef ListSearcher<listT, findByNum, int, NumberedString> myListSearcher;
	NumberedString *res = myListSearcher::findPtr( n, lst );
	if( res == NULL ) {
		printf("not found: #%d\n.", n);
	} else {
		printf("found: #%d: \"%s\".\n", res->num, res->str.c_str() );
	}
	return res;
}

NumberedString *findNumInList_Iter( int n, listT &lst )
{
	findByNum fbn(n);
	typedef ListSearcher<listT, findByNum, int, NumberedString> myListSearcher;
	NumberedString *res;
	listT::iterator it = myListSearcher::findIter( n, lst );
	if( it == lst.end() ) {
		printf("not found: #%d\n.", n);
		res = NULL;
	} else {
		res = &(*it);
		printf("found: #%d: \"%s\".\n", res->num, res->str.c_str() );
	}
	return res;
}

int main() {

	listT lst;

	lst.push_back( NumberedString(1,"ahoj.") );
	lst.push_back( NumberedString(2,"jak se mas?") );
	lst.push_back( NumberedString(3,"tak cau.") );

	findNumInList_Short( 2, lst );
	findNumInList_Short( 7, lst );

}

