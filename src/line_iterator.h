/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Line_Iterator_h
#define _Line_Iterator_h 1

#include <string>

class const_line_iterator
{
public:
	const_line_iterator( std::string &_str ) : str(&_str), pos(0) {};
	const_line_iterator() : str(NULL), pos(-1) {};
	~const_line_iterator() {};

	void operator=( const const_line_iterator &other ) { str = other.str; pos = other.pos; };
	const_line_iterator &operator++();

	const std::string &operator *();
	std::string get() const;

	bool operator<( const const_line_iterator &other ) const { return (( str->c_str() == other.str->c_str() ) && ( pos < other.pos )); };
	bool operator>( const const_line_iterator &other ) const { return (( str->c_str() == other.str->c_str() ) && ( pos > other.pos )); };
	bool operator<=( const const_line_iterator &other ) const { return (( str->c_str() == other.str->c_str() ) && ( pos <= other.pos )); };
	bool operator>=( const const_line_iterator &other ) const { return (( str->c_str() == other.str->c_str() ) && ( pos >= other.pos )); };

	bool operator==( const const_line_iterator &other ) const;
	bool operator!=( const const_line_iterator &other ) const { return !( *this == other ); };

private:
	std::string *str;
	int pos;
	std::string line;
};

#endif //Line_Iterator
