#include "str_line_iterator.h"
#include <cstring>

str_line_iterator &str_line_iterator::operator++()
{
	const char *a, *b;
	if( pos < 0 ) { return *this; };
	a = str + pos;
	b = strchr( a, '\n' );
	if( !b ) {
		pos = -1;
	} else {
		++b;
		if( b[0] == 0 ) {
			pos =-1;
		} else {
			pos += (size_t)(b - a);
		}
	}
	return *this;
}

int str_line_iterator::size() const
{
	const char *a, *b;
	if( pos < 0 ) { return -1; };
	a = str + pos;
	b = a;
	while(( b[0] != '\n' )&&( b[0] != 0 )) {
		++b;
	}
	return (int)(b - a);
}

std::string str_line_iterator::get() const
{
	if( pos < 0 ) { return ""; }
	const char *a = str + pos;
	int len = size();
	return std::string( a, (size_t)len );
}

const std::string &str_line_iterator::operator *()
{
	line = get();
	return line;
}

bool str_line_iterator::operator==( const str_line_iterator &other ) const
{
	if(( str == NULL )||( other.str == NULL )) {
		return ( pos == -1 )&&( other.pos == -1 );
	}
	return (( str == other.str ) && ( pos == other.pos ));
};
