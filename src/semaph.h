/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Semaph_h
#define _Semaph_h 1

#include "default_log.h"

#ifdef CPLUSPLUS11
#include <mutex>
#else
#include <boost/thread/mutex.hpp>
#endif //CPLUSPLUS11

class Semaph {
public:
	Semaph() {};
	~Semaph() {};
	bool Try() { return mtx.try_lock(); };
	void Lock() { mtx.lock(); };
	void Unlock() { mtx.unlock(); };
protected:
#ifdef CPLUSPLUS11
	std::mutex mtx;
#else
	boost::mutex mtx;
#endif //CPLUSPLUS11
};

class Lockable
{
public:
	Lockable() {};
	~Lockable() {};

	bool Try() { return smp.Try(); };
	void Lock() { smp.Lock(); };
	void Unlock() { smp.Unlock(); };

protected:
	Semaph smp;
};

#ifdef LOG_LOCKME

class LockHolder
{
public:
	LockHolder( Lockable &_lock, const char *_fnName = "", const char *_clName = "", void *_obj = (void*)0x12345678 )
	: lock(_lock), fnName(_fnName), clName(_clName), objPtr(_obj)
	{
		LOG_LOCKME("LOCK \"%s\" (%lx) ON  in %s\n", clName.c_str(), (unsigned long)objPtr, fnName.c_str() );
		lock.Lock();
	};
	~LockHolder()
	{
		LOG_LOCKME("LOCK \"%s\" (%lx) OFF in %s\n", clName.c_str(), (unsigned long)objPtr, fnName.c_str() );
		lock.Unlock();
	};
protected:
	Lockable &lock;
	std::string	clName;
	std::string	fnName;
	void			*objPtr;
};

#ifndef WIN32
#define LOCK_ME LockHolder lockMeNowUntilReturn( *this, __PRETTY_FUNCTION__, typeid(*this).name(), this );
#else
#define LOCK_ME LockHolder lockMeNowUntilReturn( *this, __FUNCTION__, typeid(*this).name(), this );
#endif

#else //LOG_LOCKME

class LockHolder
{
public:
	LockHolder( Lockable &_lock ) : lock(_lock)
	{
		lock.Lock();
	};
	~LockHolder()
	{
		lock.Unlock();
	};
protected:
	Lockable &lock;
};

#define LOCK_ME LockHolder lockMeNowUntilReturn( *this );

#endif //LOG_LOCKME


#endif //_Semaph_h
