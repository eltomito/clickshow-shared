/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Clicky_Time_h
#define _Clicky_Time_h 1

#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/date_time/gregorian/gregorian.hpp"
#include "str_utils.h"
#include "net/packets/net_utils.h"

/** Millisecond-precision time
 *
 */
class ClickyTime
{
public:
	ClickyTime( bool setToCurrentTime = false )
	{
		if( setToCurrentTime) {
			SetToNow();
		} else {
			SetToInvalid();
		}
	};

	ClickyTime( boost::int64_t milli ) {
		SetMilli( milli );
	};

	ClickyTime( const ClickyTime &other ) {
		value = other.value;
	};

	ClickyTime( const ClickyTime *other ) {
		if( other ) {
			value = other->value;
		} else {
			SetToInvalid();
		}
	};

	~ClickyTime() {};

	static const size_t serializedLength = 6;
	static const boost::int64_t INVALID_MILLI = 0x7fffffffffffffff;
	
	size_t Serialize( char *dst ) const
	{
		char *ptr = dst;
		boost::int64_t milli = GetMilli();
		serializeInt32( milli & 0xffffffff, &ptr );
		serializeInt16( ( milli >> 32 ) & 0xffff , &ptr );
		return ptr - dst;
	};

	size_t Unserialize( const char *src )
	{
		const char *ptr = src;
		boost::int64_t low = unserializeInt32( &ptr );
		boost::int64_t hi = unserializeInt16( &ptr );
		boost::int64_t milli = low | ( hi << 32 );
		if( milli == ( INVALID_MILLI & 0xffffffffffff ) ) {
			SetToInvalid();
		} else {
			SetMilli( milli );
		}
		return ptr - src;
	};

	void SetToNow()
	{
			value = boost::posix_time::microsec_clock::universal_time();
			SetMilli( GetMilli() );	//round to whole milliseconds
	};

	void SetToInvalid() { value = boost::posix_time::ptime(); };

	bool IsValid() const { return !value.is_special(); };

	boost::int64_t GetMilli() const {
		if( !IsValid() ) {
			return INVALID_MILLI;
		};
		boost::posix_time::time_period tp( getBaseTime(), value );
		boost::posix_time::time_duration td  = tp.length();

		boost::int64_t ticks = td.ticks();
		return ticks / ( boost::posix_time::time_duration::ticks_per_second() / 1000 );
	};

	void SetMilli( boost::int64_t milli ) {
		if( milli != INVALID_MILLI ) {
			boost::int64_t milliMultiplier = boost::posix_time::time_duration::ticks_per_second() / 1000;
			boost::posix_time::time_duration td( 0, 0, 0, milli * milliMultiplier );
			value = getBaseTime() + td;
		} else {
			SetToInvalid();
		};
	};

	bool operator==( const ClickyTime &other ) const
	{
		return( IsValid() && other.IsValid() && ( value == other.value ) );
	};
	bool operator!=( const ClickyTime &other ) const
	{
		return !(*this == other);
	};

	/**
	 * In the time ordering, an invalid value is the largest value possible.
	 */
	bool operator<( const ClickyTime &other ) const
	{
		if( IsValid() && other.IsValid() ) {
			return value < other.value;
		}
		return ( IsValid() && !other.IsValid() );
	};
	bool operator>( const ClickyTime &other ) const
	{
		return other < *this;
	};
	bool operator<=( const ClickyTime &other ) const
	{
		return (( *this == other )||( *this < other ));
	};
	bool operator>=( const ClickyTime &other ) const
	{
		return (( *this == other )||( other < *this ));
	};

	boost::int64_t operator-( const ClickyTime &other ) const {
		if( !IsValid() || !other.IsValid() ) { return INVALID_MILLI; }
		return GetMilli() - other.GetMilli();
	};

	ClickyTime &operator+=( boost::int64_t milli ) {
		if( milli == INVALID_MILLI ) {
			SetToInvalid();
		} else if( IsValid() ) {
			SetMilli( GetMilli() + milli );
		}
		return *this;
	};

	ClickyTime &operator-=( boost::int64_t milli ) {
		if( milli == INVALID_MILLI ) {
			SetToInvalid();
		} else if( IsValid() ) {
			SetMilli( GetMilli() - milli );
		}
		return *this;
	};

	typedef unsigned long ToStringFlagT;
	static const ToStringFlagT PUT_DATE = 1;
	static const ToStringFlagT PUT_MILLI = 1 << 1;
	static const ToStringFlagT LONG_FORMAT = 1 << 2;

	std::string _ToString(	ToStringFlagT flags,
									char milliSep = '.',
									char timePartSep = ':',
									char dateTimeSep = ' ',
									char datePartSep = '-'
									 ) const
	{
		if( !IsValid() ) { return "-?-";	}
		std::string dst;
		try {
			if( 0 != (flags & LONG_FORMAT) ) {
				return boost::posix_time::to_simple_string( value );
			}
			std::string tmp = boost::posix_time::to_iso_string( value ); //YYYYMMDDTHHMMSS,fffffffff
	
			size_t msi = std::string::npos;
			if( tmp.size() > 15 ) {
				msi = 16;
				if( tmp.size() > 19 ) {
					tmp.erase( 19, std::string::npos );
				}
			}
	
			if( 0 != (flags & PUT_DATE) ) {
				if( datePartSep != 0 ) {
					dst.append( tmp, 0, 4 );
					dst.push_back( datePartSep );
					dst.append( tmp, 2, 2 );
					dst.push_back( datePartSep );
					dst.append( tmp, 4, 2 );
				} else {
					dst.append( tmp, 0, 8 );
				}
			}
	
			if( dateTimeSep != 0 ) {
				dst.push_back( dateTimeSep );
			}
	
			if( timePartSep != 0 ) {
				dst.append( tmp, 9, 2 );
				dst.push_back( timePartSep );
				dst.append( tmp, 11, 2 );
				dst.push_back( timePartSep );
				dst.append( tmp, 13, 2 );
			} else {
				dst.append( tmp, 9, 6 );
			}
	
			if( 0 != (flags & PUT_MILLI) ) {
				if( milliSep != 0 ) {
					dst.push_back( milliSep );
				}
				if( msi != std::string::npos ) {
					dst.append( tmp, msi, 3 );
				} else {
					dst.append("000");
				}
			}
		} catch (...) {
			return "-??-";
		}

		return dst;
	};

	std::string ToString() const
	{
		return _ToString( LONG_FORMAT );
/*
		if( !IsValid() ) { return "-?-";	}
		return boost::posix_time::to_simple_string( value );
*/
	};

	std::string ToNiceString() const
	{
		return _ToString( PUT_MILLI, '.', ':', 0, 0 );
	};

	std::string ToTimeOnlyString( bool milli = false, char milliSep = 0 ) const
	{
		return _ToString( milli ? PUT_MILLI : 0, milliSep, 0, 0, 0 );
/*
		if( !IsValid() ) { return "-?-"; }
		std::string dst = ToDenseString( milli, milliSep );
		dst.erase( 0, 9 );
		return dst;
*/
	};

	std::string ToDenseString( bool milli = false, char milliSep = 0 ) const {
		return _ToString( PUT_DATE | (milli ? PUT_MILLI : 0), milliSep, 0, 0, 0 );
/*

		if( !IsValid() ) { return "-?-"; }
		std::string dst;
		dst = boost::posix_time::to_iso_string( value ); //YYYYMMDDTHHMMSS,fffffffff
		if( milli ) {
			if( dst.size() > 19 ) { dst.erase( 19, std::string::npos ); }
			if( dst.size() > 15 ) {
				if( milliSep != 0 ) {
					dst[15] = milliSep;
				} else {
					dst.erase( 15, 1 );
				}
			}
			return dst;
		}
		if( dst.size() > 15 ) {
			dst.erase( 15, std::string::npos );
		}
		return dst;
*/
	};

protected:
	boost::posix_time::ptime getBaseTime() const {
		return boost::posix_time::ptime( boost::gregorian::date(2016,boost::gregorian::Jul,21), boost::posix_time::time_duration(0,0,0) );
	};

	boost::posix_time::ptime	value;
};

#endif //_Clicky_Time_h
