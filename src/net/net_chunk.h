/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Net_Chunk_h
#define _Net_Chunk_h 1

class NetChunk
{
protected:
	const char 		*data;
	bool				own;
	size_t 			dstOffset;
	size_t 			len;

public:
	NetChunk( size_t _dstOffset = 0, char *_src = NULL, size_t _len = 0, bool _own = false )
	: dstOffset(_dstOffset), data(_src), len(_len), own(_own) {};

	NetChunk( const NetChunk &other ) {	*this = other; };

	NetChunk( const NetChunk &other, size_t _offset, size_t _len )
	{
		*this = other;
		data += _offset;
		dstOffset += _offset;
		len = _len;
	};

	/** Constructor from a std::string.
	 */	
	NetChunk( const std::string &src, size_t _start = 0, int _len = -1, bool _own = false )
	{
		if( _len < 0 ) {
			_len = src.size() - _start;
		}
			else if( src.size() < _start + _len ) {
			LOG_NET_ERROR("CANNOT take %d bytes at offset %ld from a string that's only %ld long!\n", _len, _start, src.size() );
			setData();
			return;
		}
		Copy( src.c_str() + _start, _len, _start, _own );
	};
	
	~NetChunk()
	{
		setData();
	};

	/** The assignment operator makes a NON-OWNED copy of the source chunk.
	 */
	NetChunk &operator=( const NetChunk &other )
	{
		data = other.data;
		len = other.len;
		dstOffset = other.dstOffset;
		own = false;
		return *this;
	};

	bool operator==( const NetChunk &other )
	{
		if(( dstOffset != other.dstOffset ) || ( len != other.len )) { return false; }
		if( len == 0 ) { return true; }
		if(	  ( data == NULL )&&( other.data != NULL )
				||( data != NULL )&&( other.data == NULL ) ) { return false; }
		return 0 == memcmp( data, other.data, len );
	};

	/** Sets or Copies data from a source.
	 */
	void Copy( const char *src, size_t srcLen, size_t _dstOffset, bool _own )
	{
		if( !_own ) {
			setData( src, srcLen, false );
		} else {
			CopyData( src, srcLen );
		}
		dstOffset = _dstOffset;
	};

	void Disown() { own = false; };
	void Own() { own = true; };

	void Clear() { setData(); };

	/** If the data inside this packed isn't owned by it, an owned copy of it is made.
	 */
	void CopyUnowned()
	{
		if( own ) { return; }
		const char *d = data;
		size_t l = len;
		data = NULL;
		len = 0;
		CopyData( d, l );
	};

	const char *GetData() { return data; };
	size_t GetLen() { return len; };
	size_t GetDstOffset() { return dstOffset; };
	bool IsOwn() { return own; };

	/** Makes a copy of the source data and stores it.
	 */
	void CopyData( const char *src = NULL, size_t _len = 0 ) {
		if(( src == NULL )||( _len == 0 )) {
			setData();
			return;
		};
		char *d = (char*)malloc( _len );
		if( d == NULL ) {
			LOG_NET_ERROR("FAILED to allocate %ld bytes for a NetChunk!\n", _len );
			return;
		}
		memcpy( d, src, _len );
		data = d;
		len = _len;
		own = true;
	};

	/** Discard a number of bytes from the beginning of the chunk.
	  */
	void TrimStart( size_t howMuch )
	{
		if( IsOwn() ) {
			LOG_NET_ERROR("TRAGEDY!!! Asked to trim the start of an owned chunk. That's gonna be a CRASH!\n");
		}
		if( len < howMuch ) { howMuch = len; }
		len -= howMuch;
		data += howMuch;
		dstOffset += howMuch;
	};

	/** Copies buffer content into dst ignoring dstOffset.
	 */
	bool PasteData( char *dst, size_t dstLen )
	{
		if( len > dstLen ) {
			LOG_NET_ERROR("Destination not big enough! dstLen = %ld, source length = %ld\n", dstLen, len );
			return false;
		}
		memcpy( dst, data, len );
		return true;
	};

	/** Pastes the content of this buffer to dst at dstOffset and fails if dstLen would be exceeded.
	 */
	bool Paste( char *dst, size_t dstLen ) { return PasteData( dst + dstOffset, dstLen - dstOffset ); };
	
	bool Paste( std::string &dst ) {
		if( dstOffset + len > dst.size() ) {
			dst.append( dstOffset + len - dst.size(), ' ' );
		}
		dst.replace( dstOffset, len, data );
		return true;
	};

	/** Do these two chunks overlap?
	 */
	bool Overlap( const NetChunk &other )
	{
		const NetChunk *low, *hi;
		if( dstOffset <= other.dstOffset ) {
			low = this;
			hi = &other;
		} else {
			hi = this;
			low = &other;
		}
		if( low->dstOffset + low->len > hi->dstOffset ) {
			return true;	//low->dstOffset + low->len - hi->dstOffset;
		}
		return false;
	};

	std::string ToString( const std::string &sep = "," )
	{
		std::string dst;
		StrUtils::Printf( dst, "dstOffset=%ld%slen=%ld%sown=%s",
			dstOffset, sep.c_str(),
			len, sep.c_str(),
			own ? "true" : "false"
		);
		return dst;
	};

protected:
	/** Sets data to given values while freeing old data if it was owned. No copying is performed.
	 */
	void setData( const char *_data = NULL, size_t _len = 0, bool _own = false )
	{
		if( own && (data!=NULL) ) {
			free( (void *)data );
			data = NULL;
		}
		data = _data;
		len = _len;
		own = _own;
	};
};

#endif //_Net_Chunk_h
