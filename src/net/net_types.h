/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Net_Types_h
#define _Net_Types_h 1

#include "boost/cstdint.hpp"

typedef boost::uint32_t			netFileIdT;
typedef boost::uint32_t			netFileRoleT;
typedef unsigned int		netFileStateT;

typedef boost::uint32_t	NodeConIdT;
typedef boost::uint32_t	ChannelIdT;
typedef boost::uint32_t	NodeLogoutReasonT;

const NodeConIdT NODE_CON_ID_INVALID = 0;

const netFileIdT FILE_ID_INVALID = 0;

const netFileRoleT FILE_ROLE_INVALID = 0;
const netFileRoleT FILE_ROLE_SUBTITLES = 1;

const netFileStateT FILE_STATE_NEW = 0;
const netFileStateT FILE_STATE_CONT = 1;
const netFileStateT FILE_STATE_DONE = 2;
const netFileStateT FILE_STATE_ABORTED = 3;

const NodeLogoutReasonT WHY_LOGOUT_UNKNOWN = 0;
const NodeLogoutReasonT WHY_LOGOUT_CLIENT_BYE = 1;
const NodeLogoutReasonT WHY_LOGOUT_SERVER_BYE = 2;
const NodeLogoutReasonT WHY_LOGOUT_TIMEOUT = 3;
const NodeLogoutReasonT WHY_LOGOUT_FAILED_LOGIN = 4;
const NodeLogoutReasonT WHY_LOGOUT_NULL_USER = 5;
const NodeLogoutReasonT WHY_LOGOUT_SERVER_FORCED = 6;

 #endif //_Net_Types_h
 