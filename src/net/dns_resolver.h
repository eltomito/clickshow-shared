/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Dns_Resolver_h
#define _Dns_Resolver_h 1

#include "boost/asio.hpp"
#include "shared/src/default_log.h"
#include "shared/src/str_utils.h"
#include "shared/src/net/asio_utils.h"

using boost::asio::ip::udp;

class DNSResolver
{
public:
	typedef void (resolvedFnT)( const udp::endpoint &ep, bool okay, void *data );

	DNSResolver( boost::asio::io_service	&_ioService )
	: ioService(_ioService), udpResolver(_ioService) {};
	~DNSResolver() {};

	/** Resolves an address string into a udp endpoint.
	 * @returns true if resolution can begin, false if there's something obviously wrong with the address string.
	 */
	bool ResolveToUdp( const std::string &addr, resolvedFnT *_resolvedFn, void *_resolvedFnData, unsigned short defaultPort = 0 )
	{
		std::string hostName;
		std::string portStr;

		if( _resolvedFn == NULL ) {
			LOG_NET_ERROR("_resolvedFn == NULL.\n");
			return false;
		}
		resolvedFn = _resolvedFn;
		resolvedFnData = _resolvedFnData;

		if( !AsioUtils::SplitHostAndPort( addr, hostName, portStr ) ) {
			LOG_NET_ERROR("This doesn't look like a valid address: \"%s\".\n", addr.c_str() );
			return false;
		}
		if( portStr.empty() ) { StrUtils::Printf( portStr, "%d", defaultPort ); }

		udpResolver.cancel();

		boost::asio::ip::udp::resolver::query query( hostName, portStr );
		udpResolver.async_resolve( query,
											boost::bind(	&DNSResolver::handleUdpResolve, this,
																boost::asio::placeholders::error,
																boost::asio::placeholders::iterator ));
		return true;
	};

protected:

	void handleUdpResolve( const boost::system::error_code& errc, boost::asio::ip::udp::resolver::iterator it )
	{
		udp::endpoint ep;
		if( (long)errc.value() != 0 ) {
			LOG_NET_ERROR("error code %ld: \"%s\" trying to resolve host.\n",
								(long)errc.value(),
								errc.message().c_str() );
			(*resolvedFn)( ep, false, resolvedFnData );
			return;
		}
		LOG_NET_DETAIL("Resolved udp endpoint: host: \"%s\" service: \"%s\", ip: %s\n",
							it->host_name().c_str(),
							it->service_name().c_str(),
							AsioUtils::EndpointToString( it->endpoint() ).c_str() );
		(*resolvedFn)( it->endpoint(), true, resolvedFnData );
	};

	resolvedFnT					*resolvedFn;
	void							*resolvedFnData;
	boost::asio::ip::udp::resolver	udpResolver;
	boost::asio::io_service 			&ioService;
};

#endif //_Dns_Resolver_h