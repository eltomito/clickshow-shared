/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Net_Msg_h
#define _Net_Msg_h 1

typedef int NetMsgT;

const NetMsgT	I_OK = 0;		//probably useless, but still...
const NetMsgT	E_LOGIN_FAILED = 1;
const NetMsgT	E_NO_RIGHT_TO_CLICK = 2;
const NetMsgT	E_CONNECT_FAILED = 1;

#endif //_Net_Msg_h
