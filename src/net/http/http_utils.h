/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef Http_Utils_h
#define Http_Utils_h 1

#include <map>
#include <string>

typedef std::map<std::string, std::string> HttpHeadersT;
typedef unsigned int HttpMethodT;

static const HttpMethodT	METH_GET = 0;
static const HttpMethodT	METH_PUT = 1;
static const HttpMethodT	METH_END = 2;

extern const char *httpMethodNames[ METH_END ];

class HttpUtils
{
public:
	static std::string &HeadersToString( HttpHeadersT &headers, std::string &dst );
	static HttpMethodT ParseMethod( const std::string &methstr );
	static std::string GetMethodName( HttpMethodT meth );
	static const char *GetStatusMsg( int statusCode );
	static bool IsBinaryContentType( const std::string &contentType );
};

std::string MethodName( HttpMethodT meth );

#endif //Http_Utils_h
