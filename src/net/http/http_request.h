/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Http_Request_h
#define _Http_Request_h 1

#include "boost/asio.hpp"
#include "boost/bind.hpp"

#include "shared/src/str_utils.h"
#include "shared/src/default_log.h"
#include "shared/src/net/http/http_utils.h"
#include "shared/src/semaph.h"
#include "shared/src/clicky_time.h"

#include <list>
#include <string>

using boost::asio::ip::tcp;

class HttpRequest
{
public:
	typedef int ProgressT;

	static const ProgressT HPR_READY = 0;
	static const ProgressT HPR_RESOLVING = 1;
	static const ProgressT HPR_CONNECTING = 2;
	static const ProgressT HPR_SENDING = 3;
	static const ProgressT HPR_RECEIVING_STATUS = 5;
	static const ProgressT HPR_RECEIVING_HEADERS = 6;
	static const ProgressT HPR_RECEIVING_BODY = 7;
	static const ProgressT HPR_OK = 8;

	static const ProgressT HPR_ERR_RESOLVE = -1;
	static const ProgressT HPR_ERR_CONNECT = -2;
	static const ProgressT HPR_ERR_SEND = -3;
	static const ProgressT HPR_ERR_RECEIVE = -4;
	static const ProgressT HPR_ERR_BAD_METHOD = -5;
	static const ProgressT HPR_FAILED = -6;
	static const ProgressT HPR_CANCELED = -7;

	HttpRequest(	HttpMethodT _method = METH_GET,
						const std::string &_server = "",
						const std::string &_path = "",
						const HttpHeadersT &_headers = HttpHeadersT(),
						const std::string &_body = ""
	)
	{
		Init( _method, _server, _path, _headers, _body );
	};

	bool Init( HttpMethodT _method = METH_GET,
						const std::string &_server = "",
						const std::string &_path = "",
						const HttpHeadersT &_headers = HttpHeadersT(),
						const std::string &_body = "" );

	bool Init( HttpMethodT _method,
						const std::string &_server,
						const std::string &_path,
						const HttpHeadersT &_headers,
						const char *_body,
						size_t _len );

	HttpRequest &operator=( const HttpRequest &other );

	bool IsActive() const { return (( progress > HPR_READY )&&( progress < HPR_OK )); };
	bool IsFinished() const { return (( progress == HPR_OK )||( progress < 0 )); };
	bool IsReady() const { return ( progress == HPR_READY); };
	void SetProgress( ProgressT prg );
	boost::asio::streambuf &GetInStreamBuf() { return inStreamBuf; };

	void ParseStatusLine();
	void ParseInHeaders();
	void CopyInBody();

	void ProcessInHeaders();

	ClickyTime	&GetStartedTime() { return startedTime; };

	std::string GetUrl();
	const std::string &GetOutHeadersAsString();
	const std::string &GetInHeadersAsString();

	const std::string &GetRequestAsString();
	boost::asio::streambuf &GetRequestAsStreamBuf();

/*
	void SetStringBody( const char *buf, size_t len );
	void SetBinaryBody( const char *buf, size_t len );
*/
	//void ClearInBuffer() { if( inStreamBuf.size() > 0 ) { inStreamBuf.consume( INT_MAX ); } };

	std::string		server;
	std::string		path;
	HttpMethodT		method;
	ProgressT		progress;

	HttpHeadersT	outHeaders;
	std::string		outBody;

	HttpHeadersT	inHeaders;
	std::string		inBody;
	long				inBodyContentLength;

	long				inBodyExpContentLength;
	long				inBodyCurContentLength;

	unsigned int	statusCode;
	std::string		statusMsg;
	std::string		httpVersion;
	ClickyTime		startedTime;

protected:
	std::string rqStr;
	std::string hdStr;
	boost::asio::streambuf inStreamBuf;
	boost::asio::streambuf outStreamBuf;
};

#endif //_Http_Request_h
