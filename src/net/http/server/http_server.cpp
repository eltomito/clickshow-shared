/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "http_server.h"
#include "http_server_con.h"
#include "http_server_con_pool.h"
#include "boost/exception/exception.hpp"

HttpServer::HttpServer( boost::asio::io_service &_ioService, unsigned short _port, HttpServerReqHandler &_reqHandler )
	:	ioService( _ioService ),
		acceptor( _ioService ),
		port( _port ),
		endpoint( boost::asio::ip::tcp::v4(), _port ),
		newCon(NULL),
		reqHandler( _reqHandler ),
		pool( *this )
{
	SetStatus(STATUS_STOPPED);
	acceptor.open( endpoint.protocol() );
	acceptor.set_option( boost::asio::ip::tcp::acceptor::reuse_address(true) );
	try {
		acceptor.bind( endpoint );
	} catch(...) {
			LOG_HTTP_ERROR("FAILED to bind to endpoint.\n" );
			SetStatus(STATUS_ERR_BIND);
	}
}

HttpServer::~HttpServer()
{
	Stop();
	acceptor.close();
}

void HttpServer::Start()
{
	acceptor.listen();
	startListen();
	LOG_HTTP_INFO("Listening for http connections on port %d.\n", port );
	SetStatus(STATUS_LISTENING);
}

bool HttpServer::startListen()
{
	newCon = pool.AddCon( reqHandler );
	if( newCon == NULL ) {
		LOG_HTTP_ERROR("FAILED to add a new connection.\n");
		return false;
	}
	
	acceptor.async_accept( newCon->GetSocket(),
      boost::bind( &HttpServer::handleAccept, this,
        boost::asio::placeholders::error));

	return true;
}

void HttpServer::handleAccept( const boost::system::error_code& e )
{
	LOG_HTTP_TRAFFIC("Starting a new connection!\n");
	newCon->Start();
	startListen();
}

void HttpServer::Stop()
{
	acceptor.cancel();
	pool.Clear();
}

const char *goodMsg[] = {"OK","STOPPED","LISTENING"};
const char *badMsg[] = {"ERROR","BIND FAILED"};

const char *HttpServer::getStatusMsg( int s )
{
	if(( s >= 0 )&&( (s+1) < sizeof( goodMsg ) )) {
		return goodMsg[ s+1 ];
	}
	if(( s < 0 )&&( (-1)*s < sizeof( badMsg ) )) {
		return badMsg[ (-1)*s ];
	}
	if( s >= 0 ) return goodMsg[0];
	return badMsg[0];
}
