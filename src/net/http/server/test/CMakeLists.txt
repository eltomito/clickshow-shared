cmake_minimum_required(VERSION 2.8)
project(TestHttpServer)

## Compiler flags
## if(CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "-O0 ${CMAKE_CXX_FLAGS}")        ## Optimize
    set(CMAKE_CXX_FLAGS "-fmax-errors=2 ${CMAKE_CXX_FLAGS}")  ## Maximum errors
    set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}")        ## C++11
## endif()

include_directories( ../../../../.. ../../../../../3rd/asio-1.10.6/include )
find_package (Threads)

set(Boost_USE_STATIC_LIBS        ON) # only find static libs
set(Boost_USE_MULTITHREADED      ON)
set(Boost_USE_STATIC_RUNTIME    OFF)
find_package(Boost COMPONENTS date_time regex)

if(Boost_FOUND)

  include_directories(${Boost_INCLUDE_DIRS})

  add_executable(test_http_server	test_http_server.cpp
  												../../../../../shared/str_utils.cpp
  												../../../../../shared/debug_fn.cpp
  												../http_in_request.cpp
  												../http_server.cpp
  												../http_server_con.cpp
  												../http_server_req_handler.cpp
  												../../http_utils.cpp
  												../../http_url.cpp
  )
 
  target_link_libraries(test_http_server ${Boost_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})

endif()
