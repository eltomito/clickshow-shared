/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../http_server.h"
#include "../http_server_req_handler.h"

class Reqh : public HttpServerReqHandler
{
public:
	Reqh() {};
	~Reqh() {};

	bool Process( HttpInRequest &req )
	{
		req.GetUrl().Parse();
		if( 0 == req.GetUrl().path.compare("/index.html") ) {
			req.RespondPlainText("Hahahahahahah!\n");
			return true;
		}
		if( 0 == req.GetUrl().path.compare("/index.bin") ) {
			const long data[] = {1, 2, 3, 4};
			req.RespondBinary( (const char *)&data[0], sizeof( data ) );
			return true;
		}
		return false;
	};
};

int main( int argc, const char *argv[] )
{
	boost::asio::io_service ioService;
	Reqh rh;

	unsigned short port = 8080;
	if( argc > 1 ) {
		port = atoi( argv[1] );
	}

	HttpServer server( ioService, port, rh );
	if( server.IsError() ) {
		printf("Something went wrong: %s\n", server.GetStatusMsg() );
		return 1;
	}
	server.Start();
	ioService.run();

	return 0;
}