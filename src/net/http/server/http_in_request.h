/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Http_In_Request_h
#define _Http_In_Request_h 1

#include "boost/asio.hpp"
#include "boost/bind.hpp"

#include "shared/src/str_utils.h"
#include "shared/src/default_log.h"
#include "shared/src/net/http/http_url.h"
#include "shared/src/net/http/http_utils.h"

#include <list>
#include <string>

using boost::asio::ip::tcp;

typedef std::map<std::string, std::string> HttpHeadersT;

class HttpInRequest
{
public:
	typedef int ProgressT;

	static const ProgressT HIR_READY = 0;
	static const ProgressT HIR_REC_REQ = 1;
	static const ProgressT HIR_REC_HEADER = 2;
	static const ProgressT HIR_REC_BODY = 3;
	static const ProgressT HIR_PROCESSING = 4;
	static const ProgressT HIR_RESPONDING = 5;
	static const ProgressT HIR_OK = 6;

	static const ProgressT HIR_ERR_RECEIVE = -1;
	static const ProgressT HIR_ERR_PROCESS = -2;
	static const ProgressT HIR_ERR_RESPOND = -3;
	static const ProgressT HIR_ERR_BAD_METHOD = -4;
	static const ProgressT HIR_CANCELED = -5;
	static const ProgressT HIR_ERR_PARSE = -6;

	static const long ED_ONE_LINE = -1;
	static const long ED_UNKNOWN = -2;

	HttpInRequest() {};
	~HttpInRequest() {};

	//HttpRequest &operator=( const HttpRequest &other );

	bool IsActive() const { return (( progress > HIR_READY )&&( progress < HIR_OK )); };
	bool IsFinished() const { return (( progress == HIR_OK )||( progress < 0 )); };
	bool IsReady() const { return ( progress == HIR_READY); };
	bool IsError() const { return ( progress < 0 ); };

	void Start() { progress = HIR_REC_REQ; };
	void Cancel() { progress = HIR_CANCELED; };
	void Finish() { progress = HIR_OK; };
	void SetError( ProgressT e ) { progress = e; };

	long ExpectedDataLen();

	//bool	ParseIncoming();
	bool Digest( boost::asio::streambuf &buf );
	bool	ParseFirstLine( std::string &ln );
	bool	ParseHeaderLine( std::string &ln );

	bool ProcessInHeaders();
	void PrepareOutHeaders();

	//-- access to incoming
	HttpUrl 			&GetUrl() { return url; };
	HttpHeadersT	&GetInHeaders() { return inHeaders; };
	HttpMethodT		GetMethod() { return method; };
	std::string		&GetInBody() { return inBody; };

	//-- access to outgoing
	HttpHeadersT &GetOutHeaders() { return outHeaders; };
	std::string GetOutHeadersAsString() { std::string dst; return HttpUtils::HeadersToString( outHeaders, dst ); }; 
	std::string GetInHeadersAsString() { std::string dst; return HttpUtils::HeadersToString( inHeaders, dst ); }; 
	void SetStatus( unsigned int _code ) { statusCode = _code; };
	std::string &GetResponse( std::string &dst );
	std::string &GetOutBody() { return outBody; };

	//shortcuts
	void RespondPlainText( const std::string &text );
	void RespondBinary( const char *buf, size_t len );

protected:
	HttpUrl			url;
	HttpMethodT		method;
	ProgressT		progress;

	HttpHeadersT	outHeaders;
	std::string		outBody;

	HttpHeadersT	inHeaders;
	std::string		inBody;

	long				expInBodyLen;

	unsigned int	statusCode;
	std::string		httpVersion;

protected:
	std::string hdStr;
	char	tmpBuf[4000];
	//boost::asio::streambuf outStreamBuf;
};

#endif //_Http_In_Request_h
