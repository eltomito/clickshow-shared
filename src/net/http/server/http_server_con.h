/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Http_Server_Con_h
#define _Http_Server_Con_h 1

#include "http_in_request.h"

class HttpServerConPool;
class HttpServerReqHandler;


class HttpServerCon
{
protected:
	HttpServerConPool			&pool;
	HttpServerReqHandler		&reqHandler;
	boost::asio::ip::tcp::socket	tcpSocket;
	HttpInRequest				request;

	boost::asio::streambuf			inBuf;
	boost::asio::streambuf			outBuf;
	std::string					responseStr;

	//char							inCharBuf[4000];

public:
	HttpServerCon( HttpServerConPool &_pool,
						HttpServerReqHandler &_reqHandler);
	~HttpServerCon();

	boost::asio::ip::tcp::socket &GetSocket() { return tcpSocket; };

	void Start();
	void Stop();
	void Kill();

protected:

	void receive();
	void process();
	void respond();
	void handleRead( const boost::system::error_code& e, std::size_t byteCount );
	void handleWrite( const boost::system::error_code& e, std::size_t bytes_written );
};

#endif //_Http_Server_Con_h
