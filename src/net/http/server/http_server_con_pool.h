/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Http_Server_Con_Pool_h
#define _Http_Server_Con_Pool_h 1

#include "shared/src/async_pool.h"
#include "http_server_con.h"

class HttpServerReqHandler;
class HttpServer;

class HttpServerConPool : public AsyncPool<HttpServerCon>
{
public:
	HttpServerConPool( HttpServer &_server ):	server(_server)
	{};
	~HttpServerConPool() {};

	HttpServerCon	*AddCon( HttpServerReqHandler &_reqHandler )
	{
		HttpServerCon *nc = new HttpServerCon( *this, _reqHandler );
		if( nc == NULL ) {
			LOG_HTTP_ERROR("FAILED to allocate a new connection!");
			return NULL;
		}
		AsyncPool<HttpServerCon>::AddCon( nc );
		return nc;
	};

	HttpServer		&GetServer() { return server; };

protected:
	HttpServer		&server;
};

#endif //_Http_Server_Con_Pool_h
