/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "http_server_con.h"
#include "http_server_con_pool.h"
#include "http_server_req_handler.h"
#include "http_server.h"

HttpServerCon::HttpServerCon(	HttpServerConPool &_pool,
										HttpServerReqHandler &_reqHandler
									) :	pool(_pool),
											tcpSocket( _pool.GetServer().GetIoService() ),
											reqHandler(_reqHandler)
									{};

HttpServerCon::~HttpServerCon()
{
	Stop();
};

void HttpServerCon::Start()
{
	request.Start();
	receive();
}

void HttpServerCon::receive()
{

	long exlen = request.ExpectedDataLen();

	LOG_HTTP_DETAIL2("Expecting this much data: %ld\n", exlen );

	if( exlen == HttpInRequest::ED_ONE_LINE ) {
		boost::asio::async_read_until( tcpSocket, inBuf, "\n",
			boost::bind( &HttpServerCon::handleRead, this,
			boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred ));
		return;
	}

	if( exlen == 0 ) {
		process();
		return;
	}

	if( exlen < 0 ) {
		LOG_HTTP_ERROR("Reading this much data is unimplemented: %ld\n", exlen);
		process();
		return;
	}

/*
	boost::asio::streambuf::mutable_buffers_type mutableBuffer = inBuf.prepare(10);
	tcpSocket.async_read_some( mutableBuffer,
								boost::bind( &HttpServerCon::handleRead, this,
								boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
*/
	boost::asio::async_read( tcpSocket, inBuf, boost::asio::transfer_at_least(1),
		boost::bind( &HttpServerCon::handleRead, this,
		boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void HttpServerCon::process() {
	if( !reqHandler.Process( request ) ) {
		LOG_HTTP_ERROR("FAILED to process request. Responding with 400.\n");
		request.SetStatus(400);
	} else {
		LOG_HTTP_TRAFFIC("Request OK! Responding with 200! Req body len: %ld, Response body len: %ld\n",
							request.GetInBody().size(),
							request.GetOutBody().size() );
#if LOG_LEVEL > 4
		std::string tmpstr;
		StrUtils::HexDump( tmpstr, (void*)request.GetOutBody().c_str(), request.GetOutBody().size() );
		LOG_HTTP_DETAIL2("--- Response body ---\n%s\n-----------\n", tmpstr.c_str() );
#endif
		request.SetStatus(200);
	}
	respond();
}

void HttpServerCon::Stop()
{
	LOG_HTTP_DETAIL2("Stopping connection.\n");
	tcpSocket.close();
}

void HttpServerCon::Kill()
{
	Stop();
	pool.DeleteMe( this );
}

void HttpServerCon::handleRead( const boost::system::error_code& e, std::size_t byteCount )
{
	LOG_HTTP_DETAIL2("Some data was read (%ld bytes)!\n", byteCount);

	if( !e ) {
		bool moreDataWanted; 
		while( inBuf.size() > 0 ) {
			moreDataWanted = request.Digest( inBuf );
			if( !moreDataWanted ) { break; };
		}
		if( moreDataWanted ) {
			receive();
		} else {
			process();
		}
		return;
	}

	if( e == boost::asio::error::eof ) {
		request.Cancel();
		Stop();
	}

	request.SetError( HttpInRequest::HIR_ERR_RECEIVE );
	LOG_HTTP_ERROR("Error: %s\n", e.message().c_str() );
	Stop();
}

void HttpServerCon::handleWrite( const boost::system::error_code& e, std::size_t bytes_written )
{
	LOG_HTTP_DETAIL2("Written %ld bytes, status: %s\n", bytes_written, e ? "ERROR":"OK" ); 
	if(!e) {
		request.Finish();
	} else {
		request.SetError( HttpInRequest::HIR_ERR_RESPOND );
		LOG_HTTP_ERROR("Error: %s\n", e.message().c_str() );
	};
	Stop();
}

void HttpServerCon::respond()
{
	request.GetOutHeaders()["Connection"] = "close";
	
	std::ostream rqStream( &outBuf );
	rqStream << request.GetResponse( responseStr );

	boost::asio::async_write(tcpSocket, outBuf,
		boost::bind(&HttpServerCon::handleWrite, this,
		boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}
