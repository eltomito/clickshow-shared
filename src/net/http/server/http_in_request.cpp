/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "http_in_request.h"
#include "boost/regex.hpp"

long HttpInRequest::ExpectedDataLen()
{
	switch( progress ) {

		case HIR_REC_REQ:
		case HIR_REC_HEADER:
			return ED_ONE_LINE;
		break;

		case HIR_REC_BODY:
			switch( method ) {

				case METH_GET:
					return 0;
				break;

				case METH_PUT:
					return ( expInBodyLen >= 0 ) ? ( expInBodyLen - inBody.size() ): ED_UNKNOWN;
				break;
			}
		break;
	}
	return 0;
}

bool HttpInRequest::ProcessInHeaders()
{
	std::string lenStr = inHeaders["Content-Length"];
	bool res;
	expInBodyLen = StrUtils::ParseLong( lenStr, res );
	LOG_HTTP_DETAIL("Content-Length = %ld\n", expInBodyLen );
	if( !res ) { expInBodyLen = -1; };
	return true;
}

/**
 * @returns true if more data is expected, false otherwise.
 */
bool HttpInRequest::Digest( boost::asio::streambuf &buf )
{
	std::string ln;

	int explen = ExpectedDataLen();
	if( explen == ED_ONE_LINE ) {
			std::istream strm( &buf );
			std::getline( strm, ln );
	}

	switch( progress ) {
		case HIR_REC_REQ:
			StrUtils::TrimInPlace( ln );
			LOG_HTTP_DETAIL("Parsing request \"%s\"\n", ln.c_str() );
			if( !ParseFirstLine( ln ) ) { return false; }
			progress = HIR_REC_HEADER;
			return true;
		break;

		case HIR_REC_HEADER:
			StrUtils::TrimInPlace( ln );
			LOG_HTTP_DETAIL("Parsing header \"%s\"\n", ln.c_str() );
			if( ln.empty() ) {
				if( !ProcessInHeaders() )
				{
					LOG_HTTP_ERROR("Header processing FAILED!\n");
					return false;
				}
				progress = HIR_REC_BODY;
				LOG_HTTP_DETAIL2("Expecting this much body: %ld, inBuf.size() = %ld\n", ExpectedDataLen(), buf.size() );
				return ( ExpectedDataLen() > 0 );
			}
			if( !ParseHeaderLine( ln ) ) { return false; }
			return true;
		break;

		case HIR_REC_BODY:
		{
			LOG_HTTP_DETAIL2("Reading some body...\n");

			std::istream strm( &buf );
			while( strm.read( tmpBuf, sizeof(tmpBuf) ) && ( ExpectedDataLen() > 0 )) {
				inBody.append( tmpBuf, sizeof(tmpBuf) );
			}
			inBody.append( tmpBuf, strm.gcount() );

			explen = ExpectedDataLen();
			LOG_HTTP_DETAIL2("...and expecting %d more.\n", explen);
			return ( explen > 0 );
		}
		break;

		default:
		break;
	}

	return false;
}

bool HttpInRequest::ParseHeaderLine( std::string &ln )
{
	boost::regex rgx( "^([-a-zA-Z0-9_.,]+): *(.*)$" );
	boost::match_results<std::string::const_iterator> m;
	std::string key;
	std::string value;
	if( boost::regex_search( ln, m, rgx ) ) {
		key.assign( m[1].first, m[1].second );
		value.assign( m[2].first, m[2].second );
		inHeaders[key] = value;
		LOG_HTTP_DETAIL2("Header line: \"%s\" = \"%s\"\n", key.c_str(), value.c_str() ); 
		return true;
	}
	SetError( HIR_ERR_PARSE );
	return false;
}

bool HttpInRequest::ParseFirstLine( std::string &ln )
{
	//method
	size_t i = ln.find(' ');
	if( i == std::string::npos ) {
		SetError( HIR_ERR_PARSE );
		return false;
	}
	std::string methstr = ln.substr( 0, i );
	method = HttpUtils::ParseMethod( methstr );
	if( method == METH_END ) {
		SetError( HIR_ERR_BAD_METHOD );
		return false;
	}

	//protocol (ignored)
	ln.erase( 0, i+1 );
	i = ln.rfind(' ');
	if( i == std::string::npos ) {
		SetError( HIR_ERR_PARSE );
		return false;
	}
	ln.erase( i, std::string::npos );

	//url
	if( !url.Set( ln ) ) {
		SetError( HIR_ERR_PARSE );
		return false;
	}
	return true;
}

void HttpInRequest::PrepareOutHeaders()
{
	std::string dst;
	StrUtils::Printf( dst, "%d", outBody.size() );
	outHeaders["Content-Length"] = dst;
	outHeaders["Connection:"] = "close";
}

std::string &HttpInRequest::GetResponse( std::string &dst )
{
	PrepareOutHeaders();

	dst.clear();
	StrUtils::Printf(dst, "HTTP/1.1 %d %s\r\n", statusCode, HttpUtils::GetStatusMsg( statusCode ) );
	dst.append( GetOutHeadersAsString() );
	dst.append("\r\n");
	dst.append( outBody );
	return dst;
}

/*
boost::asio::streambuf &HttpInRequest::GetResponseAsStreamBuf()
{
	std::ostream rqStream( &outStreamBuf );
	rqStream << GetResponseAsString();
	return outStreamBuf;
}
*/

void HttpInRequest::RespondPlainText( const std::string &text )
{
	outBody = text;
	outHeaders["Content-Encoding"] = "utf-8";
	outHeaders["Content-Type"] = "text/plain";
}

void HttpInRequest::RespondBinary( const char *buf, size_t len )
{
	outBody.assign( buf, len );
	outHeaders["Content-Type"] = "application/octet-stream";
}
