/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef Http_Server_Req_Handler_h 
#define Http_Server_Req_Handler_h 1

#include "http_in_request.h"

class HttpServerReqHandler
{
public:
	HttpServerReqHandler() {};
	~HttpServerReqHandler() {};

	virtual bool Process( HttpInRequest &req );
};

#endif //Http_Server_Req_Handler_h
