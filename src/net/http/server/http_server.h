/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Http_Server_h
#define _Http_Server_h 1

#include "boost/asio.hpp"
#include "http_server_con_pool.h"

class HttpServerCon;
class HttpServerReqHandler;

class HttpServer
{
public:
	static const int	STATUS_STOPPED = 0;
	static const int	STATUS_LISTENING = 1;
	static const int	STATUS_ERR_BIND = -1;

	HttpServer( boost::asio::io_service &_ioService, unsigned short _port, HttpServerReqHandler &_reqHandler );
	~HttpServer();

	void Start();
	void Stop();

	boost::asio::io_service &GetIoService() { return ioService; };

	void SetStatus( int s ) { status = s; };
	int GetStatus() { return status; };
	const char *GetStatusMsg() { return getStatusMsg( status ); };
	bool IsError() { return (status < 0); };

	bool IsListening() { return status == STATUS_LISTENING; };

protected:
	bool startListen();
	void handleAccept( const boost::system::error_code& e );	
	const char *getStatusMsg( int s );

protected:
	boost::asio::io_service			&ioService;
	boost::asio::ip::tcp::acceptor	acceptor;
	boost::asio::ip::tcp::endpoint	endpoint;

	HttpServerCon				*newCon;
	HttpServerConPool			pool;
	unsigned short				port;
	HttpServerReqHandler		&reqHandler;
	
	int status;
};

#endif //_Http_Server_h
