/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "http_utils.h"

const char *httpMethodNames[ METH_END ] = {"GET","PUT"};

std::string &HttpUtils::HeadersToString( HttpHeadersT &headers, std::string &dst )
{
	HttpHeadersT::iterator it = headers.begin();
	HttpHeadersT::iterator endit = headers.end();
	dst.clear();
	while( it != endit ) {
		dst.append( it->first );
		dst.append(": ");
		dst.append( it->second );
		dst.append("\r\n");
		++it;
	}
	return dst;
}

std::string HttpUtils::GetMethodName( HttpMethodT meth )
{
	if( meth >= sizeof( httpMethodNames ) ) {
		return "";
	}
	return httpMethodNames[ meth ];	
}

HttpMethodT HttpUtils::ParseMethod( const std::string &methstr )
{
	if( 0 == methstr.compare("GET") ) { return METH_GET; };
	if( 0 == methstr.compare("PUT") ) { return METH_PUT; };
	return METH_END;
}

const char *HttpUtils::GetStatusMsg( int statusCode )
{
	if(( statusCode >= 200 )&&( statusCode < 300 )) {
			return "OK";
	}
	if(( statusCode >= 400 )&&( statusCode < 500 )) {
			return "Bad Request";
	}
	return "Go Figure.";
}

bool HttpUtils::IsBinaryContentType( const std::string &contentType )
{
	if( contentType.size() < 4 ) { return true; }
	return( 0 != contentType.substr(0,4).compare("text") );
}
