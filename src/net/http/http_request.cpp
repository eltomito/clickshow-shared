/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "http_request.h"

bool HttpRequest::Init( HttpMethodT _method,
					const std::string &_server,
					const std::string &_path,
					const HttpHeadersT &_headers,
					const std::string &_body
					)
{
	return Init(	_method,
						_server,
						_path,
						_headers,
						_body.c_str(),
						_body.size()
					);
}

bool HttpRequest::Init( HttpMethodT _method,
						const std::string &_server,
						const std::string &_path,
						const HttpHeadersT &_headers,
						const char *_body,
						size_t _len )
{
	if( _len > 0 ) {
		outBody.assign( _body, _len );
	} else {
		outBody.clear();
	}
	method = _method;
	server = StrUtils::Trim( _server );
	path = StrUtils::Trim( _path );
	outHeaders = _headers;
	inHeaders.clear();
	inBody.clear();
	inBodyContentLength = -1;

	inBodyExpContentLength = -1;
	inBodyCurContentLength = 0;

	progress = HPR_READY;
	statusCode = 0;
	statusMsg = "";
	httpVersion = "";
	rqStr = "";
	hdStr = "";

	outHeaders["Host"] = server;
	std::string lenStr;
	StrUtils::Printf( lenStr, "%d", _len );
	LOG_HOLE_DETAIL("Content-Length: %s (originally %d)\n", lenStr.c_str(), _len );
	outHeaders["Content-Length"] = lenStr;

	if( method >= METH_END ) {
		SetProgress( HPR_ERR_BAD_METHOD );
		return false;
	}
	return true;
}

void HttpRequest::SetProgress( ProgressT prg )
{
	bool wasActive = IsActive();
	progress = prg;
	if( !wasActive && IsActive() ) {
		startedTime.SetToNow();
	}
};

HttpRequest &HttpRequest::operator=( const HttpRequest &other )
{
	server = other.server;
	path = other.path;
	method = other.method;
	progress = other.progress;
	statusCode = other.statusCode;
	statusMsg = other.statusMsg;
	httpVersion = other.httpVersion;

	outHeaders = other.outHeaders;
	outBody = other.outBody;
	inHeaders = other.inHeaders;
	inBody = other.inBody;

	rqStr = other.rqStr;
	hdStr = other.hdStr;

	return *this;
}

std::string HttpRequest::GetUrl()
{
	std::string dst = server;
	StrUtils::PathAppend( dst, path );
	return dst;
}

const std::string &HttpRequest::GetRequestAsString()
{
	rqStr.clear();
	rqStr.append( HttpUtils::GetMethodName( method ) );
	rqStr.append(" http://");
	rqStr.append( GetUrl() );
	rqStr.append(" HTTP/1.1\r\n");
	rqStr.append( GetOutHeadersAsString() );
	rqStr.append("\r\n");
	rqStr.append( outBody );

	LOG_HTTP_DETAIL("Request body len = %ld, total request len = %ld\n", outBody.size(), rqStr.size() );

	return rqStr;
}

const std::string &HttpRequest::GetOutHeadersAsString()
{
	return HttpUtils::HeadersToString( outHeaders, hdStr );
}

const std::string &HttpRequest::GetInHeadersAsString()
{
	return HttpUtils::HeadersToString( inHeaders, hdStr );
}

boost::asio::streambuf &HttpRequest::GetRequestAsStreamBuf()
{
	std::ostream rqStream( &outStreamBuf );
	rqStream << GetRequestAsString();
	return outStreamBuf;
}

void HttpRequest::ParseStatusLine()
{
	std::istream rstream( &inStreamBuf );
	rstream >> httpVersion;
	rstream >> statusCode;
	std::getline( rstream, statusMsg );
	StrUtils::TrimInPlace( statusMsg );
}

void HttpRequest::ProcessInHeaders()
{
	bool res;
	inBodyContentLength = StrUtils::ParseLong( inHeaders["Content-Length"], res );
	if( !res ) { inBodyContentLength = -1; }

	inBodyExpContentLength = inBodyContentLength;
}

/** Parse the incoming headers
 * @returns true if there are headers, false if it's actually the body
 */
void HttpRequest::ParseInHeaders()
{
	std::istream rstream( &inStreamBuf );
	std::string hstr;
	size_t i;
	std::string key;
	std::string value;

	while( std::getline( rstream, hstr ) && ( hstr != "\r" )) {
		i = hstr.find(':');
		if( i != std::string::npos ) {
			key = hstr.substr( 0, i );
			value = ( hstr.size() > (i+1) ) ? hstr.substr( i+1, std::string::npos ) : "";
			StrUtils::TrimInPlace( key );
			StrUtils::TrimInPlace( value );
			inHeaders.insert( std::pair<std::string, std::string>( key, value ) );
			LOG_HTTP_DETAIL("Header: \"%s\" : \"%s\"\n", key.c_str(), value.c_str() );
		}
	}
}

bool getChunkSize( size_t &chunkSize, const std::string &str )
{
	std::stringstream ss;
	try {
		ss << std::hex << str;
		ss >> chunkSize;
	} catch( int e ) {
		return false;
	}
	return true;
}

void HttpRequest::CopyInBody()
{
	bool binary;
	std::istream rstream( &inStreamBuf );
	std::string bstr;
	size_t	chunkSize;
	size_t	curSize = 0;

	binary = HttpUtils::IsBinaryContentType( inHeaders["Content-Type"] );

	if( !binary ) {

		bool chunked = ( 0 == ( inHeaders["Transfer-Encoding"].compare("chunked") ) );

		if( chunked ) {
			if( std::getline( rstream, bstr ) ) {
				chunked = getChunkSize( chunkSize, bstr );
			} else {
				return;
			}
		}
		
		while( std::getline( rstream, bstr ) ) {
		
			if(( bstr.size() > 0 )&&( bstr[bstr.size()-1] == '\r' )) {
				bstr.erase( bstr.size()-1, 1 );
				curSize += 1;
			}
		
			inBody.append( bstr );
			inBody.push_back('\n');
			if( chunked ) {
				curSize += bstr.size() + 1;	//CR is the +1
				if( curSize >= chunkSize ) {
					if( !getChunkSize( chunkSize, bstr ) ) {
						chunked = false;
					} else {
						if( chunkSize == 0 ) { break; }
					}
				}
			}
		}
	} else { //binary
		char tmpbuf[ 1024 ];
		while( rstream.read( tmpbuf, sizeof( tmpbuf) )) {
			inBody.append( tmpbuf, sizeof( tmpbuf ));
		}
		inBody.append( tmpbuf, rstream.gcount() );
	}
}

