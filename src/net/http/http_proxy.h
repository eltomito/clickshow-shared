/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

class HttpProxyClient
{
	bool Connect( const std::string &addr, boost::asio::udp_endpoint &onBehalfOf );
	void Reset();

	bool Send( const unsigned char *buf, size_t len );
//overload
	virtual void receive( UdpPacket &pkt );
};

class HttpProxyServer
{
	bool Send( const unsigned char *buf, size_t len, boost::asio::udp_endpoint &ep  );
//overload
	virtual void receive( const unsigned char *buf, size_t len, boost::asio::udp_endpoint &ep );
};

class HttpClient
{
	bool Send( MethodT meth, const std::string &url );
	virtual void receive( const std::string &url, 



};