/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Http_Node_h
#define _Http_Node_h 1

#include "boost/asio.hpp"
#include "boost/bind.hpp"

using boost::asio::ip::tcp;

class HttpNode
{
public:
	HttpNode( boost::asio::io_service &_ioService )
	:	ioService( _ioService ),
		tcpSocket( _ioService ) {};

protected:
	boost::asio::io_service	&ioService;
	tcp::socket	tcpSocket;
};

#endif //_Http_Node_h
