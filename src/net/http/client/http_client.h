/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Http_Client_h
#define _Http_Client_h 1

#include "boost/asio.hpp"
#include "shared/src/str_utils.h"
#include "shared/src/net/http/http_request.h"
#include "shared/src/net/http/client/http_node.h"
#include "shared/src/default_log.h"
#include "shared/src/semaph.h"

#include <list>
#include <string>

using boost::asio::ip::tcp;

class HttpReceiver
{
public:
	HttpReceiver() {};
	~HttpReceiver() {};
	virtual void Receive( HttpRequest &req ) {};
};

class HttpClient : public HttpNode, public Lockable
{
public:
	HttpClient( boost::asio::io_service &_ioService )
	:	HttpNode( _ioService ),
		tcpResolver( _ioService ),
		request(NULL),
		receiver(NULL) {};
	~HttpClient() {};

	bool MakeRequest(	HttpReceiver *rec, HttpRequest *req );

	bool RequestInProgress() { return (( request != NULL )&&( request->IsActive() )); };
	void CancelRequest();

	void SetProxy( const std::string &_proxy_server = "" ) { proxy_server = _proxy_server; };

protected:
	void sendToReceiver() { if( receiver != NULL ) { receiver->Receive( *request ); } };

	void handleResolve( const boost::system::error_code& err, tcp::resolver::iterator endpoint_iterator );
	void handleConnect( const boost::system::error_code& err );
	void handleWriteRequest( const boost::system::error_code& err );
	void handleReadContent( const boost::system::error_code& err, size_t bytes_read );
	void handleReceive( const boost::system::error_code& err ); 

protected:
	tcp::resolver	tcpResolver;
	HttpRequest		*request;
	HttpReceiver	*receiver;
	std::string		proxy_server;
};

#endif //_Http_Client_h
