/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "http_client.h"
#include "shared/src/net/asio_utils.h"

void HttpClient::CancelRequest()
{
	LOCK_ME;

	tcpSocket.shutdown( boost::asio::ip::tcp::socket::shutdown_send );
	tcpSocket.close();
}

bool HttpClient::MakeRequest(	HttpReceiver *rec, HttpRequest *req )
{
	LOCK_ME;

	if( RequestInProgress() ) { return false; };
	if( !req->IsReady() ) { return false; };

	req->outHeaders["Connection:"] = "close";

	receiver = rec;
	request = req;
	request->SetProgress( HttpRequest::HPR_RESOLVING );

	std::string hostName;
	std::string portStr;
	if( !AsioUtils::SplitHostAndPort(
		proxy_server.empty() ? request->server : proxy_server,
		hostName, portStr ) ) {
		request->SetProgress( HttpRequest::HPR_ERR_RESOLVE );
		return false;
	}
	if( portStr.empty() ) { portStr = "http"; }

    // Start an asynchronous resolve to translate the server and service names
    // into a list of endpoints.
    tcp::resolver::query query( hostName, portStr );
    tcpResolver.async_resolve(query,
        boost::bind( &HttpClient::handleResolve, this,
          boost::asio::placeholders::error,
          boost::asio::placeholders::iterator));
	return true;
}

void HttpClient::handleResolve(	const boost::system::error_code& err,
      				tcp::resolver::iterator endpoint_iterator)
{
	LOCK_ME;

	if (!err) {
		LOG_HTTP_DETAIL2("Resolved!\n");
		// Attempt a connection to each endpoint in the list until we
		// successfully establish a connection.
		request->SetProgress( HttpRequest::HPR_CONNECTING );
		boost::asio::async_connect( tcpSocket, endpoint_iterator,
			boost::bind(&HttpClient::handleConnect, this,
			boost::asio::placeholders::error));
	} else {
		request->SetProgress( HttpRequest::HPR_ERR_RESOLVE );
		LOG_HTTP_ERROR("Error: %s\n", err.message().c_str() );
		sendToReceiver();
	}
}

void HttpClient::handleConnect( const boost::system::error_code& err )
{
	LOCK_ME;

	if (!err) {
		LOG_HTTP_DETAIL2("Connected!\n");
		// The connection was successful. Send the request.
		request->SetProgress( HttpRequest::HPR_SENDING );
		boost::asio::async_write(tcpSocket, request->GetRequestAsStreamBuf(),
			boost::bind(&HttpClient::handleWriteRequest, this,
			boost::asio::placeholders::error));
	} else {
		request->SetProgress( HttpRequest::HPR_ERR_CONNECT );
		LOG_HTTP_ERROR("Error: %s\n", err.message().c_str() );
		sendToReceiver();
	}
}

void HttpClient::handleWriteRequest( const boost::system::error_code& err )
{
	LOCK_ME;

	if (!err) {
		LOG_HTTP_DETAIL2("Request sent!\n");
		// Read the response status line. The response_ streambuf will
		// automatically grow to accommodate the entire line. The growth may be
		// limited by passing a maximum size to the streambuf constructor.
		request->SetProgress( HttpRequest::HPR_RECEIVING_STATUS );

		boost::asio::async_read_until( tcpSocket, request->GetInStreamBuf(), "\r\n",
			boost::bind( &HttpClient::handleReadContent, this,
			boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred ));

	} else {
		request->SetProgress( HttpRequest::HPR_ERR_SEND );
		LOG_HTTP_ERROR("Error: %s\n", err.message().c_str() );
		sendToReceiver();
	}
}

void HttpClient::handleReadContent( const boost::system::error_code& err, size_t bytes_read ) 
{
	LOCK_ME;

	if (!err) {
		switch( request->progress ) {
			case HttpRequest::HPR_RECEIVING_STATUS:
				LOG_HTTP_DETAIL2("Status line received!\n");
				// Check that response is OK.
				request->ParseStatusLine();
				if( request->statusCode == 200 ) {
					// Read the response headers, which are terminated by a blank line.
					request->SetProgress( HttpRequest::HPR_RECEIVING_HEADERS );
					boost::asio::async_read_until(tcpSocket, request->GetInStreamBuf(), "\r\n\r\n",
					boost::bind( &HttpClient::handleReadContent, this,
					boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred ));
				} else {
					request->SetProgress( HttpRequest::HPR_FAILED );
					LOG_HTTP_DETAIL2("Invalid response: %d\n", request->statusCode );
					sendToReceiver();
				}
			break;
			case HttpRequest::HPR_RECEIVING_HEADERS:
				LOG_HTTP_DETAIL2("Headers received!\n");
				request->ParseInHeaders();
				request->ProcessInHeaders();
				//request->ClearInBuffer();
				// Read the body
				request->SetProgress( HttpRequest::HPR_RECEIVING_BODY );
				request->inBodyCurContentLength = request->GetInStreamBuf().size();
				boost::asio::async_read(tcpSocket, request->GetInStreamBuf(), boost::asio::transfer_at_least(1),
					boost::bind( &HttpClient::handleReadContent, this,
					boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
			break;
			case HttpRequest::HPR_RECEIVING_BODY:

				request->inBodyCurContentLength += bytes_read;

				LOG_HTTP_DETAIL2("Received %ld bytes of response body! (total: %ld, exp: %ld\n", bytes_read, request->inBodyCurContentLength, request->inBodyExpContentLength);

				if( (request->inBodyExpContentLength >= 0 ) && ( request->inBodyCurContentLength >= request->inBodyExpContentLength ) ) {
					LOG_HTTP_DETAIL2("Response body finished! (%ld bytes)\n", request->inBodyCurContentLength );
					request->CopyInBody();
					request->SetProgress( HttpRequest::HPR_OK );
					tcpSocket.shutdown( boost::asio::ip::tcp::socket::shutdown_send );
					tcpSocket.close();
					sendToReceiver();
				}

				boost::asio::async_read(tcpSocket, request->GetInStreamBuf(), boost::asio::transfer_at_least(1),
					boost::bind( &HttpClient::handleReadContent, this,
					boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
			break;
			default:
				request->SetProgress( HttpRequest::HPR_FAILED );
				LOG_HTTP_ERROR("Read something but what? progress = %d\n", request->progress);
				sendToReceiver();
			break;
		}
	} else {
		if(err == boost::asio::error::eof) {
			request->inBodyCurContentLength += bytes_read;

			LOG_HTTP_DETAIL2("Response received (EOF)! (bytes recvd = %ld, total recvd = %ld, expected = %ld)\n",
									bytes_read, request->inBodyCurContentLength, request->inBodyExpContentLength );
			request->CopyInBody();
			request->SetProgress( HttpRequest::HPR_OK );
			sendToReceiver();
		} else {
			request->SetProgress( HttpRequest::HPR_ERR_RECEIVE );
			LOG_HTTP_ERROR("Error: %s\n", err.message().c_str() );
			sendToReceiver();
		}
	}
}
