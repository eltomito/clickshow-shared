/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "boost/asio.hpp"
#include "boost/bind.hpp"

#include "shared/src/str_utils.h"
#include "shared/src/net/http/client/http_client.h"
#include "shared/src/default_log.h"

#include <list>
#include <string>

using boost::asio::ip::tcp;

class TestRec : public HttpReceiver
{
public:
	TestRec() {};
	~TestRec() {};
	void Receive( HttpRequest &req )
	{
		if( req.IsFinished() ) {
			printf("--- done! ---\nstatus: %d - \"%s\"\n---headers---\n%s\n--- body ---\n%s\n--- body end ---\n",
				req.statusCode,
				req.statusMsg.c_str(),
				req.GetInHeadersAsString().c_str(),
				req.inBody.c_str() );
			return;
		}
		printf("Somehing went wrong! Result code: %d, status code: %d, status msg: \"%s\"\n", req.progress, req.statusCode, req.statusMsg.c_str() );
	};
};

int main( int argc, char* argv[] )
{
	boost::asio::io_service ioService;
	HttpClient client( ioService );

	std::string server = "clickshow.xf.cz";
	std::string path = "index.html";

	if( argc > 1 ) {
		server = argv[1];
		if( argc > 2 ) {
			path = argv[2];
		}
	}

	TestRec tr;
	HttpRequest req( METH_GET, server, path );
	client.MakeRequest( &tr, &req );

	ioService.run();

	return 0;
};
