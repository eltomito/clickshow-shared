/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "http_url.h"
#include "boost/regex.hpp"

HttpUrl::HttpUrl( const std::string _url, bool parse )
{
	Set( _url, parse );
}

bool findAssignErase( std::string &src, const boost::regex &rgx, std::string &dst, const std::string &debugName = "" ) 
{
	boost::match_results<std::string::const_iterator> m;
	if( boost::regex_search( src, m, rgx ) ) {
		dst.assign( m[1].first, m[1].second );
/*
		if( !debugName.empty() ) {
			printf("%s: %s\n", debugName.c_str(), dst.c_str() );
		}
*/
		src.erase( 0, std::string( m[0].first, m[0].second ).size() );
		return true;
	}
	return false;
}

bool findAssignParamErase( std::string &src, std::map<std::string, std::string> &dst ) 
{
	boost::regex rgx( "^([-a-zA-Z0-9_.,]+)=([^&#]+)" );
	boost::match_results<std::string::const_iterator> m;
	std::string key;
	std::string value;
	if( boost::regex_search( src, m, rgx ) ) {
		key.assign( m[1].first, m[1].second );
		value.assign( m[2].first, m[2].second );
		dst[key] = value;
		src.erase( 0, key.size() + value.size() + 1 );
		return true;
	}
	return false;
}

bool HttpUrl::Set( const std::string &_url, bool parse )
{
	url = _url;
	if( parse ) {
		return Parse();
	} else {
		errorCode = ERR_UNPARSED;
	}
	return true;
}

bool HttpUrl::Parse()
{
	std::string u = url;
	errorCode = ERR_OK;

	server.clear();
	path.clear();
	params.clear();
	fragment.clear();

	//protocol
	boost::regex protoRegex( "^([[:word:]]+)://" );
	std::string protocol;
	if( findAssignErase( u, protoRegex, protocol, "Protocol" ) ) {
		if( 0 != protocol.compare("http") ) {
			errorCode = ERR_PROTOCOL;
			return false;
		}
	}

	//server
	if( u.empty() ) { return true; }
	if( u[0] != '/' ) {
		boost::regex serverRegex( "^([[:word:]][-a-zA-Z0-9_.:]+)" );
		if( !findAssignErase( u, serverRegex, server, "Server" ) ) {
			errorCode = ERR_SERVER;
			return false;
		}
	}

	//path
	if( u.empty() ) { return true; }
	boost::regex pathRegex( "^([^?#]+)" );
	findAssignErase( u, pathRegex, path, "Path" );

	//query
	if( u.empty() ) { return true; }
	std::string qa;
	boost::regex paramRegex( "^([^&#]+)" );
	while(( u[0] == '?' )||( u[0] == '&' )) {
		u.erase(0,1);
		if( !findAssignParamErase( u, params ) ) {
			if( findAssignErase( u, paramRegex, qa, "Query Parameter" ) ) {
				params[qa]="1";
			} else {
				break;
			}
		}
	}

	//fragment
	if( u.empty() ) { return true; }
	if( u[0] != '#' ) {
		errorCode = ERR_FRAGMENT;
		return false;
	}
	fragment = u.substr( 1, std::string::npos );

	return true;
}

const std::string &HttpUrl::Compose()
{
	url.clear();
	if( !server.empty() ) {
		url = "http://";
		url.append( server );
	}
	url.append(path);
	if( params.size() > 0 ) {
		url.push_back('?');
		ParamsT::iterator it = params.begin();
		ParamsT::iterator endit = params.end();
		while( it != endit ) {
			url.append( it->first );
			url.push_back('=');
			url.append( it->second );
			url.push_back('&');
			++it;
		}
		url.pop_back();
	}
	if( !fragment.empty() ) {
		url.push_back('#');
		url.append(fragment);
	}
	return url;
}

std::string HttpUrl::GetParam( const std::string &name, const std::string &defaultValue )
{
	ParamsT::iterator it = params.find( name );
	if( it == params.end() ) {
		return defaultValue;
	}
	return it->second;
}
