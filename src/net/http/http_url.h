/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef Http_Url_h
#define Http_Url_h 1

#include <map>
#include <string>

class HttpUrl
{
public:
	static const int ERR_UNPARSED = -1;
	static const int ERR_OK = 0;
	static const int ERR_PROTOCOL = 1;
	static const int ERR_SERVER = 2;
	static const int ERR_PATH = 3;
	static const int ERR_QUERY = 4;
	static const int ERR_FRAGMENT = 5;

	typedef std::map<std::string, std::string> ParamsT;

	HttpUrl( const std::string _url = "", bool parse = true );
	~HttpUrl() {};

	bool					Set( const std::string &_url, bool parse = true );
	bool					Parse();
	const std::string	&Compose();

	bool					IsParsed() { return errorCode != ERR_UNPARSED; };
	bool					IsValid() { return errorCode == ERR_OK; };
	bool					HasParam( const std::string &name ) { return ( params.end() != params.find(name) ); };
	std::string			GetParam( const std::string &name, const std::string &defaultValue = "" );

public:
	int			errorCode;
	std::string	url;
	std::string	server;
	std::string	path;
	std::string	fragment;
	ParamsT		params;
};

#endif //Http_Url_h
