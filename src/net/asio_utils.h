/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Asio_Utils_h
#define _Asio_Utils_h 1

#include "boost/asio.hpp"
#include "shared/src/str_utils.h"
#include <string>

using boost::asio::ip::udp;

class AsioUtils {
public:
	static std::string EndpointToString( const boost::asio::ip::udp::endpoint &ep ) {
		std::string res = "";
		if( ep.address().is_v4() ) {
			StrUtils::Printf(res, "%s:%d", ep.address().to_string().c_str(), ep.port() );
		} else { //ipv6
			StrUtils::Printf(res, "[%s]:%d", ep.address().to_string().c_str(), ep.port() );
		}
		return res;
	}

	static bool StringToEndpoint( const std::string &src, boost::asio::ip::udp::endpoint &dst, unsigned short default_port )
	{
		boost::asio::ip::address	addr;
		unsigned short port;
		bool isv4 = true;

		size_t asi = src.find_first_of('[');
		size_t psi = std::string::npos;

		size_t i = src.find_first_of(':');
		size_t j;
		if( i == std::string::npos ) {
			isv4 = true;
		} else {
			j = src.find_first_of(':', i + 1);
			if( j == std::string::npos ) {
				isv4 = true;
			} else {
				isv4 = false;
			}
		}

		if( isv4 ) {
			if( i != std::string::npos ) {

				try {	addr = boost::asio::ip::address::from_string( src.substr(0,i) ); }
				catch(...) { return false; }

				port = atoi( src.substr(i+1).c_str() );
			} else {

				try { addr = boost::asio::ip::address::from_string( src ); }
				catch(...) { return false; }

				port = default_port;
			}
		} else {
			i = src.find_first_of( '[' );
			if( i != std::string::npos ) {
				j = src.find_first_of( ']', i + 1 );
				if( j == std::string::npos ) { return false; }
			}
			++i;

			try {	addr = boost::asio::ip::address::from_string( src.substr(i, j - i) ); }
			catch(...) { return false; }

			i = src.find_first_of(':', j + 1 );
			if( j != std::string::npos ) {
				port = atoi( src.substr(j+1).c_str() );
			} else {
				port = default_port;
			}
		}

		if( isv4 && !addr.is_v4() ) { return false; }
		if( !isv4 && !addr.is_v6() ) { return false; }
		dst.address( addr );
		dst.port( port );
		return true;
	};

	static bool IsSameEndpoint( const boost::asio::ip::udp::endpoint &ep1, const boost::asio::ip::udp::endpoint &ep2 ) {
		return ( ep1.address() == ep2.address() )&&( ep1.port() == ep2.port() );
	};

	static udp::endpoint MakeEndpoint( unsigned char ip1, unsigned char ip2, unsigned char ip3, unsigned char ip4, unsigned short port )
	{
		udp::endpoint ep;
		unsigned long ip = (unsigned long)ip1;
		ip = ip << 8;
		ip = ip | (unsigned long)ip2;
		ip = ip << 8;
		ip = ip | (unsigned long)ip3;
		ip = ip << 8;
		ip = ip | (unsigned long)ip4;
		ep.address( boost::asio::ip::address_v4( ip ) );
		ep.port( port );
		return ep;
	};


	/** Splits an address into its host and port parts.
	 * Feel free to call this with symbolic names (cooldomain.com:123),
	 * ipv4 addresses (127.0.0.1:555) or ipv6 addresses (abcd:1234:def0:9999).
	 * @param addr - The host address string including port number. "clickshow.tk:9876", "127.0.0.1", "[abcd:1234:00ff:654c]:9876", etc.
	 * @param hostName - where the host name will be written.
	 * @portStr - where the port part of addr will be written.
	 * @returns true if the address seems to make sense, false otherwise.
	 */
	static bool SplitHostAndPort( const std::string &addr, std::string &hostName, std::string &portStr )
	{
		std::string a = StrUtils::Trim( addr );
		if( a.size() == 0 ) { return false;	}

		bool noport = false;
		size_t bracki, brackj, porti;
		if( IsIpv6( a, bracki, brackj, porti ) ) {
			//it's an ipv6 address
			noport = (porti == std::string::npos);
			if( bracki != std::string::npos ) {
				//erase the brackets
				a.erase(brackj,1);
				a.erase(bracki,1);
			}
		};

		size_t coli = noport ? std::string::npos : a.find_last_of(':');
		if( coli == std::string::npos ) {
			hostName = a;
			portStr = "";
			return true;
		}
		if( coli == 0 ) { return false; };
		hostName = a.substr( 0, coli );
		if( coli+1 < a.size() ) {
			portStr = a.substr( coli+1, std::string::npos );
		} else {
			portStr = "";
		}
		return true;
	};

	/** Decides whether a string is a valid ipv6 address.
	 * @param addr - the address to consider. The format is something like "1234:abcd:6541:ffff" or "[1234:abcd:6541:ffff]:12345". 
	 * @param bracki - where the offset of the opening square bracket will be written. std::string::npos means no square brackets.
	 * @param brackj - where the offset of the closing square bracket will be written. std::string::npos means no square brackets.
	 * @param porti - where the offset of the port part of the address will be written. std::string::npos means no port number given.
	 * @returns true if addr looks like a valid ipv6 address, false otherwise.
	 */
	static bool IsIpv6( const std::string &addr, size_t &bracki, size_t &brackj, size_t &porti   )
	{
		bracki = std::string::npos;
		brackj = std::string::npos;
		porti = std::string::npos;

		size_t endi = addr.size();
//		int maxParts = 8;
		size_t i = 0;
		if( addr[0] == '[' ) {
			endi = addr.find_last_of(']');
			if( endi == std::string::npos ) { return false; } //no closing bracket
			bracki = 0;
			brackj = endi;
			++i;
		}

		size_t j;

		while( i < endi ) {
			j = addr.find_first_not_of( "0123456789ABCDEFabcdef", i );
			if( j == std::string::npos ) {
				if( endi - i == 4 ) {
					i = endi;
					break; //the final four digits are valid
				}
				return false; //there are less or more than 4 digits at the end
			}
			if( i - j != 4 ) { return false; } //this part of the address isn't 4 digits.
			i = j + 1;
		}

		//the address part is okay. What about the port?
		i = endi + 1;
		if( i >= addr.size() ) { return true; } //there is no port

		if( addr[i] != ':' ) { return false; } //there are some weird characters after the closing bracket
		++i;
		if( i >= addr.size() ) { return false; } //no port number after semicolon
		size_t badi = addr.find_first_not_of( "1234567890", i );
		if( badi != std::string::npos ) { return false; } //there are non-digits in the port number
		porti = i;
		return true;
	};

};

#endif //_Asio_Utils_h
