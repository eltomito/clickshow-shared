/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "udp_packet_hi.h"
#include "../../str_utils.h"

#include <cstring>
#include <climits>

size_t UdpPacketHi::serialize( char *buffer, int max_len ) const
{
	char *dst = buffer;
	serializeStr( clientId, &dst, &max_len );
	serializeStr( loginName, &dst, &max_len );
	serializeStr( loginPwd, &dst, &max_len );
	return dst - buffer;
}

size_t UdpPacketHi::unserialize( const char *buffer, int len )
{
	const char *src = buffer;
	clientId = unserializeStr( &src, &len );
	loginName = unserializeStr( &src, &len );
	loginPwd = unserializeStr( &src, &len );
	return (src - buffer);
};
size_t UdpPacketHi::serializedLength() const
{
	int max_len = MAX_PAYLOAD_SIZE;
	return serializedStrLen( clientId, &max_len )
			+ serializedStrLen( loginName, &max_len )
			+ serializedStrLen( loginPwd, &max_len );
}

void UdpPacketHi::toString( std::string &dst, const char *separator ) const
{
	StrUtils::Printf( dst, "clientId: %s%sloginName: %s%sloginPwd: %s%s", clientId.c_str(), separator, loginName.c_str(), separator, loginPwd.c_str(), separator );
};
