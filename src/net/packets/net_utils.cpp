/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "net_utils.h"

#include <cstring>

/** writes a string into a buffer and returns the number of bytes written.
 *
 */
size_t serializeStr( const std::string &src, char **buffer, int *max_len )
{
	if( *max_len < 0 ) { *max_len = MAX_UDP_PAYLOAD_SIZE; }
	if( *max_len == 0 ) { return 0; }

	size_t len = src.size() + 1;
	len = len <= *max_len ? len : *max_len;

	strncpy( *buffer, src.c_str(), len );
	(*buffer)[ len - 1 ] = 0;

	*max_len -= len;
	*buffer += len;
	return len;
}

size_t serializedStrLen( const std::string &src, int *max_len )
{
	if( *max_len < 0 ) { *max_len = MAX_UDP_PAYLOAD_SIZE; }
	if( *max_len == 0 ) { return 0; }

	size_t len = src.size() + 1;
	len = (len < *max_len) ? len : *max_len;

	*max_len -= len;
	return len;
}

std::string unserializeStr( const char **buffer, int *len )
{
	std::string res;

	*len = *len < 0 ? MAX_UDP_PAYLOAD_SIZE : *len;
	const char *pos = *buffer;
	const char *e = *buffer + *len;
	while(( pos[0] != 0)&&( pos < e )) {
		pos++;
	}

	res.assign( *buffer, (size_t)(pos - *buffer) );

	if( pos < e ) { pos++; };

	*len -= (pos - *buffer);
	*buffer = pos;
	
	return res;
}

/** Serializes an integer into a desired number of bytes.
 *
 */
size_t serializeInt( unsigned long int value, char **dst, size_t byteLen )
{
	for( int i = 0; i < byteLen; ++i ) {
		(*dst)[0] = (char)(value & 0xff);
		++(*dst);
		value = value>>8;
	}
	return byteLen;
}

/** Serializes an integer into a desired number of bytes.
 * This version keeps track of how many bytes are left in the input buffer.
 */
size_t serializeInt( unsigned long int value, char **dst, size_t byteLen, int *maxLen )
{
	if( byteLen > *maxLen ) {
		*dst += *maxLen;
		*maxLen = 0;
		return -1;
	}
	serializeInt( value, dst, byteLen );
	*maxLen += byteLen;
	return byteLen;
};

unsigned long int unserializeInt( const char **src, size_t byteLen )
{
	unsigned long int value = 0;
	for( int i = 0; i < byteLen; ++i ) {
		value |= ( 0xff & ((unsigned long int)((*src)[0])) ) << (i*8);
		++(*src);
	}
	return value;
}

unsigned long int unserializeInt( const char **src, size_t byteLen, int *maxLen )
{
	if( byteLen > *maxLen ) {
		*src += *maxLen;
		*maxLen = 0;
		return 0;
	}
	unsigned long int value = unserializeInt( src, byteLen );
	*maxLen -= byteLen;
	return value;
}

#define DEF_SERIALIZE_INT( _numBits_, _numBytes_ ) \
size_t serializeInt##_numBits_ ( unsigned long int value, char **dst ) {	return serializeInt( value, dst, _numBytes_ ); };
#define DEF_SERIALIZE_INT_MAXLEN( _numBits_, _numBytes_ ) \
size_t serializeInt##_numBits_ ( unsigned long int value, char **dst, int *max_len ) {	return serializeInt( value, dst, _numBytes_ , max_len ); };
#define DEF_UNSERIALIZE_INT( _numBits_, _numBytes_ ) \
unsigned long int unserializeInt##_numBits_ ( const char **src ) { return unserializeInt( src, _numBytes_ ); };
#define DEF_UNSERIALIZE_INT_LEN( _numBits_, _numBytes_ ) \
unsigned long int unserializeInt##_numBits_ ( const char **src, int *len ) { return unserializeInt( src, _numBytes_, len ); };

DEF_SERIALIZE_INT( 16, 2 )
DEF_SERIALIZE_INT( 32, 4 )
DEF_SERIALIZE_INT( 64, 8 )

DEF_UNSERIALIZE_INT( 16, 2 )
DEF_UNSERIALIZE_INT( 32, 4 )
DEF_UNSERIALIZE_INT( 64, 8 )

DEF_SERIALIZE_INT_MAXLEN( 16, 2 )
DEF_SERIALIZE_INT_MAXLEN( 32, 4 )
DEF_SERIALIZE_INT_MAXLEN( 64, 8 )

DEF_UNSERIALIZE_INT_LEN( 16, 2 )
DEF_UNSERIALIZE_INT_LEN( 32, 4 )
DEF_UNSERIALIZE_INT_LEN( 64, 8 )
