/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Udp_Packet_FileHeader_h
#define _Udp_Packet_FileHeader_h 1

#include "udp_packet.h"
#include "shared/src/net/node/user_perms.h"
#include "auto/auto_packet.h"

/** The first packet of a file being transfered.
 */
class UdpPacketFileHeader : public AutoPacket
{
public:
	UdpPacketFileHeader( netFileIdT _id = 0, netFileRoleT _role = 0, size_t _length = 0,
								unsigned int _number = UdpPacket::NO_NUMBER,
						FlagsT _flags = (FlagsT)0 )
						: AutoPacket( AutoPacket::pdFileHeader, _number, _flags ),
						id(_id), role(_role), length(_length) {};

	~UdpPacketFileHeader() {};

	netFileIdT		id;
	netFileRoleT	role;
	size_t			length;
};

#endif //_Udp_Packet_FileHeader_h
