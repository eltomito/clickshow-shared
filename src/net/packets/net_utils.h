/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Net_Utils_h
#define _Net_Utils_h 1

#include <string>

#include "shared/src/net/net_constants.h"

size_t serializeStr( const std::string &src, char **buffer, int *max_len );
size_t serializedStrLen( const std::string &src, int *max_len );
std::string unserializeStr( const char **buffer, int *len );

size_t serializeInt( unsigned long int value, char **dst, size_t byteLen );
unsigned long int unserializeInt( const char **src, size_t byteLen );

#define DECL_SERIALIZE_INT( _numBits_, _numBytes_ ) \
size_t serializeInt##_numBits_ ( unsigned long int value, char **dst );
#define DECL_SERIALIZE_INT_MAXLEN( _numBits_, _numBytes_ ) \
size_t serializeInt##_numBits_ ( unsigned long int value, char **dst, int *max_len );
#define DECL_UNSERIALIZE_INT( _numBits_, _numBytes_ ) \
unsigned long int unserializeInt##_numBits_ ( const char **src );
#define DECL_UNSERIALIZE_INT_LEN( _numBits_, _numBytes_ ) \
unsigned long int unserializeInt##_numBits_ ( const char **src, int *len );

/*
size_t serializeInt16( unsigned long int value, char **dst );
unsigned long int unserializeInt16( const char **src );
size_t serializeInt16( unsigned long int value, char **dst, int *max_len );
unsigned long int unserializeInt16( const char **src, int *len );
...Also in 32 and 64-bit versions.
*/

DECL_SERIALIZE_INT( 16, 2 )
DECL_SERIALIZE_INT( 32, 4 )
DECL_SERIALIZE_INT( 64, 8 )

DECL_SERIALIZE_INT_MAXLEN( 16, 2 )
DECL_SERIALIZE_INT_MAXLEN( 32, 4 )
DECL_SERIALIZE_INT_MAXLEN( 64, 8 )

DECL_UNSERIALIZE_INT( 16, 2 )
DECL_UNSERIALIZE_INT( 32, 4 )
DECL_UNSERIALIZE_INT( 64, 8 )

DECL_UNSERIALIZE_INT_LEN( 16, 2 )
DECL_UNSERIALIZE_INT_LEN( 32, 4 )
DECL_UNSERIALIZE_INT_LEN( 64, 8 )

#endif //_Net_Utils_h
