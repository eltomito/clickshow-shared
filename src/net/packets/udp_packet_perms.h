/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Udp_Packet_Perms_h
#define _Udp_Packet_Perms_h 1

#include "udp_packet.h"
#include "shared/src/net/node/user_perms.h"
#include "auto/auto_packet.h"

class UdpPacketPerms : public AutoPacket
{
public:
	UdpPacketPerms( UserPermsT _perms = 0, unsigned int _number = UdpPacket::NO_NUMBER,
						FlagsT _flags = (FlagsT)0 )
						: AutoPacket( AutoPacket::pdPerms, _number, _flags ), perms(_perms) {};

	~UdpPacketPerms() {};

	UserPermsT	perms;
};

#endif //_Udp_Packet_Perms_h
