/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "udp_packet_msg.h"
#include "../../str_utils.h"

#include <cstring>
#include <climits>

size_t UdpPacketMsg::serialize( char *buffer, int max_len ) const
{
	if( max_len < 0 ) { max_len = MAX_PAYLOAD_SIZE; }
	if( max_len < 4 ) { return -1; }

	char *dst = buffer;
	serializeInt32( msgCode, &dst, &max_len );
	return dst - buffer;
}

size_t UdpPacketMsg::unserialize( const char *buffer, int len )
{
	if( len < 0 ) { len = MAX_PAYLOAD_SIZE; }
	if( len < 4 ) { return -1; }

	const char *src = buffer;
	msgCode = unserializeInt32( &src, &len );
	return src - buffer;
}

size_t UdpPacketMsg::serializedLength() const
{
	return 4;
}

void UdpPacketMsg::toString( std::string &dst, const char *separator ) const
{
	StrUtils::Printf( dst, "msg code: %d", msgCode );
}
