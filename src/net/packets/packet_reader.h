/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Packet_Reader_h
#define _Packet_Reader_h 1

#include "udp_packets.h"

/*
 * UdpPacketHi *uphi = new((void*)&dst)UdpPacketHi();
 * uphi->Unserialize( buf, len );
 */
#define UNSERIALIZE_PACKET_CASE( _pktType, _pktClassName, _ptrName, _buffer, _len, _dst ) \
case _pktType : \
{ \
_pktClassName * _ptrName = new((void*) _dst ) _pktClassName (); \
_ptrName -> Unserialize( _buffer, _len ); \
} \
break;

class PacketReader
{
public:
	static PacketTypeT Read( char *buf, size_t len, UdpPacketAny &dst ) {
		UdpPacketHeader uph;
		int reallen = uph.Unserialize( buf, len );
		if( reallen <= 0 ) { return PacketTypeT::INVALID; }

		const int pkt_sizes[] = {
			-1, //invalid
			-1, //hi
			-1, //bye
			-1, //show
			-1, //touch
			-1, //chat
			-1, //pkt_msg
			24, //perms
			-1, //load
			32, //filehead
			-1, //filebody
			65 //play
		};

		const int min_sizes[] = {
			0, //invalid
			23, //hi
			20, //bye
			20, //show
			20, //touch
			20, //chat
			20, //pkt_msg
			20, //perms
			20, //load
			20, //filehead
			32, //filebody
			20 //play
		};

		if(( uph.type >= 0 )&&( uph.type < (sizeof(pkt_sizes) / sizeof( int )) )) {
			if( pkt_sizes[ uph.type ] >= 0 ) {
				if( pkt_sizes[ uph.type ] != len ) {
					LOG_NET_ERROR("Bad packet size for type %i: %i instead of %i.\n", uph.type, len, pkt_sizes[ uph.type ] );
					return PacketTypeT::INVALID;
				} else if( min_sizes[ uph.type ] > len ) {
					LOG_NET_ERROR("Bad packet size for type %i: It's %i but should be at least %i.\n", uph.type, len, min_sizes[ uph.type ] );
					return PacketTypeT::INVALID;
				}
			}
		}

		if( uph.type == PLAY ) {
			int iii = uph.type;
			LOG_NET_DETAIL2("It's the dangerous Play packet type! %i\n", iii);
		}

		switch( uph.type ) {
			UNSERIALIZE_PACKET_CASE( HI, UdpPacketHi, uphi, buf, len, &dst );
			UNSERIALIZE_PACKET_CASE( BYE, UdpPacketBye, upbye, buf, len, &dst );
			UNSERIALIZE_PACKET_CASE( SHOW, UdpPacketShow, upshow, buf, len, &dst );
			UNSERIALIZE_PACKET_CASE( TOUCH, UdpPacketTouch, uptouch, buf, len, &dst );
			UNSERIALIZE_PACKET_CASE( CHAT, UdpPacketChat, upchat, buf, len, &dst );
			UNSERIALIZE_PACKET_CASE( PKT_MSG, UdpPacketMsg, upmsg, buf, len, &dst );
			UNSERIALIZE_PACKET_CASE( PERMS, UdpPacketPerms, upperms, buf, len, &dst );
			UNSERIALIZE_PACKET_CASE( LOAD, UdpPacketPerms, upload, buf, len, &dst );
			UNSERIALIZE_PACKET_CASE( FILEHEAD, UdpPacketFileHeader, upfilehead, buf, len, &dst );
			UNSERIALIZE_PACKET_CASE( FILEBODY, UdpPacketFileBody, upfilebody, buf, len, &dst );
			case PLAY:
			{
				UdpPacketPlay *upplay = new((void*)&dst)UdpPacketPlay();
				upplay->Unserialize( buf, len );
			}
			break;
//			UNSERIALIZE_PACKET_CASE( PLAY, UdpPacketPlay, upplay, buf, len, &dst );
			default:
				LOG_NET_ERROR("CANNOT READ packet type = %d!\n", uph.type );
				return PacketTypeT::INVALID;
			break;
		};
		return uph.type;
	};
/*
	static void DumbCopy( const UdpPacket &src, UdpPacketAny &dst ) {
		char buf[ MAX_UDP_PAYLOAD_SIZE ];
		int len = src.Serialize( buf, sizeof(buf) );
		Read( buf, len, dst );
	};
*/

//new((void*)&dst)UdpPacketHi( src );
#define COPY_PACKET_CASE( _typeName, _typeExt ) \
case _typeName:\
	new((void*)&dst)UdpPacket##_typeExt ( *((UdpPacket##_typeExt *)&src) );\
break;

	static void Copy( const UdpPacket &src, UdpPacketAny &dst ) {
		switch( src.type ) {
			COPY_PACKET_CASE( HI, Hi )
			COPY_PACKET_CASE( BYE, Bye )
			COPY_PACKET_CASE( SHOW, Show )
			COPY_PACKET_CASE( TOUCH, Touch )
			COPY_PACKET_CASE( CHAT, Chat )
			COPY_PACKET_CASE( PKT_MSG, Msg )
			COPY_PACKET_CASE( PERMS, Perms )
			COPY_PACKET_CASE( LOAD, Load )
			COPY_PACKET_CASE( FILEHEAD, FileHeader )
			COPY_PACKET_CASE( FILEBODY, FileBody )
			COPY_PACKET_CASE( PLAY, Play )
			default:
				LOG_NET_ERROR("!!!!!!!!! No idea how to copy packet type %u. Your program is probably about to CRASH!!!!!", src.type);
			break;
		}
	};

};

#endif //_Packet_Reader_h
