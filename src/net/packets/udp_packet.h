/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Udp_Packet_h
#define _Udp_Packet_h 1

#include <string>
#include <climits>

#include "net_utils.h"
#include "shared/src/clicky_time.h"

enum PacketTypeT {
	INVALID = 0,
	HI,
	BYE,
	SHOW,
	TOUCH,
	CHAT,
	PKT_MSG,
	PERMS,
	LOAD,
	FILEHEAD,
	FILEBODY,
	PLAY,
	TESTAUTO,	//experimental!!!
	TYPE_TERMINATOR
};

enum UPErrT {
	OK = 0,
	GENERIC = 1,
	BUFFER_TOO_SHORT = 2
};

/*
 * ---------------------- UdpPacketHeader -----------------------
 */
class UdpPacketHeader {
public:
	enum FlagsT {
		NONE = 0,
		ACK = 1,
		ABORT = 2,
		//ERROR = 4 //deprecated, don't use it!
	};

	static const unsigned int NO_NUMBER = UINT_MAX;

	UdpPacketHeader(	PacketTypeT _type = INVALID,
							const char *_typeName = "",
							unsigned int _number = NO_NUMBER,
							FlagsT _flags = NONE )
						:	type( _type ),
							typeName( _typeName ),
							number( _number ),
							flags( _flags ),
							sentTime(false),
							origSentTime(false) {};

	~UdpPacketHeader() { typeName.clear(); };
	static UdpPacketHeader *FromRaw( const char *buffer, int len = -1 );

	static const size_t checksum_length = 2;
	static const size_t serialized_length = 6 + ( 2 * ClickyTime::serializedLength ) + checksum_length; 

	static size_t SerializedLength() { return serialized_length; };
	bool IsAck() const { return 0 != (flags & ACK ); };
	void SetAck( bool state = true ) { flags = (FlagsT)( state ? (flags | ACK) : (flags & ~ACK) ); };
	bool IsAbort() const { return 0 != (flags & ABORT ); };
	void SetAbort( bool state = true ) { flags = (FlagsT)( state ? (flags | ABORT) : (flags & ~ABORT) ); };
	//bool IsError() const { return 0 != (flags & ERROR ); };
	//void SetError( bool state = true ) { flags = (FlagsT)( state ? (flags | ERROR) : (flags & ~ERROR) ); };

	void SetSentTime( const ClickyTime &tm ) { sentTime = tm; };
	const ClickyTime &GetSentTime() const { return sentTime; };
	void SetOrigTime( const ClickyTime &tm ) { origSentTime = tm; };
	const ClickyTime &GetOrigTime() const { return origSentTime; };

	bool IsValidType() const { return ((type > INVALID)&&(type < TYPE_TERMINATOR)); };

	int Serialize( char *buffer, int max_len ) const;
	int Unserialize( const char *buffer, int len = -1 );

	unsigned int GetNextNumber() { if( number == NO_NUMBER ) { return 0; }; return number + 1; };
	void IncNum() { number = GetNextNumber(); };
	bool HasNoNumber() const { return number == NO_NUMBER; };

	bool IsLaterIndexThan( unsigned int _number ) const { return HasNoNumber() ? false
																	: (_number == NO_NUMBER) ? true : ( number > _number ); };
	bool IsLaterIndexThan( const UdpPacketHeader &hdr ) const { return IsLaterIndexThan( hdr.number ); };
/*
	bool IsFresherTimeThan( const UdpPacketHeader &hdr ) const
	{
		return !sentTime.IsValid() ? false : ( !hdr.sentTime.IsValid() ? false : ( sentTime > hdr.sentTime ) );
	};
*/
	void ToString( std::string &dst, const char *separator = "\n" ) const;
	std::string ToString( const char *separator = "\n" ) const { std::string dst; ToString( dst, separator ); return dst; };

	const std::string &GetTypeName() const { return typeName; };
	void SetTypeName( const std::string &_typeName ) { typeName = _typeName; };

	bool operator ==( const UdpPacketHeader &other )
	{
		return (number==other.number)&(type==other.type)&&(flags==other.flags)
				&&( (sentTime==other.sentTime) || (!sentTime.IsValid() && !other.sentTime.IsValid() ) )
				&&( (origSentTime==other.origSentTime) || (!origSentTime.IsValid() && !other.origSentTime.IsValid() ) );
	};
	bool operator !=( const UdpPacketHeader &other ) { return !((*this)==other); };

protected:
	static unsigned int getCRC16( const char *data, size_t len );

public:
	unsigned int	number;		//packet number that let's you determine the right order of arriving packets
	PacketTypeT		type;			//determines what is inside this packet
	FlagsT			flags;		//is this an acknowledge or error packet?

protected:
	ClickyTime		sentTime;		//the time this packet was sent
	ClickyTime		origSentTime;	//if this is an ACK, this is when the original packet we are acking was sent. For non-ACKs, it's available for any use.
	std::string		typeName;	//human-readable type of this packet
};

/*
 * ---------------- UdpPacket -------------------
 */
class UdpPacket : public UdpPacketHeader {
public:

	using UdpPacketHeader::FlagsT;
	using UdpPacketHeader::SetAck;
	using UdpPacketHeader::SetAbort;
	//using UdpPacketHeader::SetError;
	using UdpPacketHeader::IsAck;
	using UdpPacketHeader::IsAbort;
	//using UdpPacketHeader::IsError;
	using UdpPacketHeader::number;
	using UdpPacketHeader::type;
	using UdpPacketHeader::flags;
	using UdpPacketHeader::NO_NUMBER;
	using UdpPacketHeader::HasNoNumber;
	using UdpPacketHeader::IsLaterIndexThan;
	using UdpPacketHeader::GetNextNumber;
	using UdpPacketHeader::IncNum;
	using UdpPacketHeader::GetSentTime;
	using UdpPacketHeader::SetSentTime;
	using UdpPacketHeader::GetTypeName;
	using UdpPacketHeader::SetTypeName;
	using UdpPacketHeader::IsValidType;

	static const size_t MAX_PAYLOAD_SIZE = MAX_UDP_PAYLOAD_SIZE - UdpPacketHeader::serialized_length;
	static const size_t MAX_STRING_LENGTH = MAX_PAYLOAD_SIZE - 1;

protected:
	UdpPacket(	PacketTypeT _type,
					const char *_typeName,
					unsigned int _number = NO_NUMBER,
					FlagsT _flags = NONE ) : UdpPacketHeader( _type, _typeName, _number, _flags ) {};
public:
	virtual ~UdpPacket() {};

	size_t SerializedLength() const { return UdpPacketHeader::SerializedLength() + serializedLength(); };

	int Serialize( char *buffer, int max_len ) const;
	int Unserialize( const char *buffer, int len = -1 );

	void ToString( std::string &dst, const char *separator = "\n" ) const;
	std::string ToString( const char *separator = "\n" ) const { std::string dst; ToString( dst, separator ); return dst; };

	bool IsEqual( const UdpPacket &other, bool inclHeader = true );
	bool operator ==( const UdpPacket &other ) { return IsEqual( other, true ); };
	bool IsLaterIndexThan( const UdpPacket &other ) const { return IsLaterIndexThan( other.number ); };

	UdpPacketHeader &GetHeader() { return *((UdpPacketHeader*)this); };

protected:	
	virtual size_t serialize( char *buffer, int max_len ) const { return 0; };
	virtual size_t unserialize( const char *buffer, int len = -1 ) { return 0; };
	virtual size_t serializedLength() const { return 0; };
	virtual void toString( std::string &dst, const char *separator = "\n" ) const {};
	virtual bool isEqual( const UdpPacket &other ) const { return false; };
};

#endif //_Udp_Packet_h
