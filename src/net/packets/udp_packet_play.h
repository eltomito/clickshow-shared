/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Udp_Packet_Play_h_
#define _Udp_Packet_Play_h_ 1

#include "udp_packet.h"
#include "shared/src/clicky_time.h"
#include "auto/auto_packet.h"
#include "shared/src/player/player_state.h"

class UdpPacketPlay : public AutoPacket
{
public:
	UdpPacketPlay( unsigned int _number = UdpPacket::NO_NUMBER, FlagsT _flags = (FlagsT)0 )
						: AutoPacket( AutoPacket::pdPlay, _number, _flags ),
						startTimestamp(false), startTimecode(0.0), delay(0.0), speed(1.0), isPlaying(false),shownSub(-1),nextSub(-1) {};
/*
	UdpPacketPlay( ClickyTime _startTimestamp, double _startTimecode, double _delay, bool _isPlaying,
						int _shownSub, int _nextSub,
						unsigned int _number = UdpPacket::NO_NUMBER, FlagsT _flags = (FlagsT)0 )
						: AutoPacket( AutoPacket::pdPlay, _number, _flags ),
						startTimestamp(_startTimestamp), startTimecode(_startTimecode), delay(_delay),isPlaying(_isPlaying),
						shownSub(_shownSub),nextSub(_nextSub) {};
*/
	UdpPacketPlay( const PlayerState &ps,
						unsigned int _number = UdpPacket::NO_NUMBER, FlagsT _flags = (FlagsT)0 )
						: AutoPacket( AutoPacket::pdPlay, _number, _flags ),
						startTimestamp(ps.startTimestamp), startTimecode(ps.startTimecode),
						delay(ps.delay),speed(ps.speed),isPlaying(ps.isPlaying),
						shownSub(ps.shownSub),nextSub(ps.nextSub) {};

	PlayerState &ToPlayerState( PlayerState &ps ) {
		ps.startTimestamp = startTimestamp;
		ps.startTimecode = startTimecode;
		ps.delay = delay;
		ps.speed = speed;
		ps.isPlaying = isPlaying;
		ps.shownSub = shownSub;
		ps.nextSub = nextSub;
		return ps;
	};

	~UdpPacketPlay() {};

	ClickyTime	startTimestamp;
	double		startTimecode;
	double		delay;
	double		speed;
	bool			isPlaying;
	int			shownSub;
	int			nextSub;
};

#endif //_Udp_Packet_Play_h_
