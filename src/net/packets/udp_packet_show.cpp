/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "udp_packet_show.h"
#include "../../str_utils.h"

#include <cstring>
#include <climits>

size_t UdpPacketShow::serialize( char *buffer, int max_len ) const
{
	if( max_len < 0 ) { max_len = MAX_PAYLOAD_SIZE; }
	if( max_len < 4 ) { return -1; }

	char *dst = buffer;
	serializeInt32( subNum, &dst, &max_len );
	serializeStr( subText, &dst, &max_len );
	return dst - buffer;
}

size_t UdpPacketShow::unserialize( const char *buffer, int len )
{
	if( len < 0 ) { len = MAX_PAYLOAD_SIZE; }
	if( len < 4 ) { return -1; }

	const char *src = buffer;
	subNum = unserializeInt32( &src, &len );
	subText = unserializeStr( &src, &len );
	return src - buffer;
};

size_t UdpPacketShow::serializedLength() const
{
	int len = MAX_PAYLOAD_SIZE;
	return 4 + serializedStrLen( subText, &len );
};

void UdpPacketShow::toString( std::string &dst, const char *separator ) const
{
	std::string nstr;
	
	if( IsNumberless() ) {
		nstr = "<numberless>";
	} else if( IsHide() ) {
		nstr = "<hide>";
	} else {
		StrUtils::Printf( nstr, "%d", subNum );
	}
	StrUtils::Printf( dst, "sub #: %s%stext: %s", nstr.c_str(), separator, subText.c_str() );
};
