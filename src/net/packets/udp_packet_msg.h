/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Udp_Packet_Msg_h
#define _Udp_Packet_Msg_h 1

#include "udp_packet.h"
#include "shared/src/net/net_msg.h"

class UdpPacketMsg : public UdpPacket {
public:
/*
	UdpPacketMsg(	unsigned int _number = UdpPacket::NO_NUMBER,
						FlagsT _flags = (FlagsT)0 ) : UdpPacket( MSG, "Msg", _number, _flags ) {};
*/
	UdpPacketMsg(	NetMsgT _msgCode = I_OK,
						unsigned int _number = UdpPacket::NO_NUMBER,
						FlagsT _flags = (FlagsT)0 ) : UdpPacket( PKT_MSG, "Msg", _number, _flags ), msgCode(_msgCode ) {};
	~UdpPacketMsg() {};

	size_t serialize( char *buffer, int max_len ) const;
	size_t unserialize( const char *buffer, int len = -1 );
	size_t serializedLength() const;
	void toString( std::string &dst, const char *separator = "\n" ) const;
	bool isEqual( const UdpPacket &other ) const { return ( msgCode == ((UdpPacketMsg*)&other)->msgCode ); };

	NetMsgT msgCode;
};

#endif //_Udp_Packet_Msg_h
