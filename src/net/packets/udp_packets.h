/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Udp_Packets_h
#define _Udp_Packets_h 1

#include "udp_packet_hi.h"
#include "udp_packet_show.h"
#include "udp_packet_touch.h"
#include "udp_packet_bye.h"
#include "udp_packet_chat.h"
#include "udp_packet_msg.h"
#include "udp_packet_perms.h"
#include "udp_packet_load.h"
#include "udp_packet_file_header.h"
#include "udp_packet_file_body.h"
#include "udp_packet_play.h"
#include "udp_packet_invalid.h"
#include "auto/auto_packet.h"

#ifdef CPLUSPLUS11

#include "shared/src/united_types.h"

typedef UnitedTypes<	UdpPacketHi,
							UdpPacketShow,
							UdpPacketTouch,
							UdpPacketBye,
							UdpPacketChat,
							UdpPacketMsg,
							UdpPacketPerms,
							UdpPacketLoad,
							UdpPacketFileHeader,
							UdpPacketFileBody,
							UdpPacketPlay,
							UdpPacketTestAuto,
							UdpPacketInvalid
							> UdpPacketAny;

#else //CPLUSPLUS11

class UdpPacketAny
{
public:
	UdpPacketAny() {};
	~UdpPacketAny() {};
private:
	char	m_padding[1024];
};

#endif //CPLUSPLUS11

#endif //_Udp_Packets_h
