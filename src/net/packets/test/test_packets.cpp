/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../udp_packets.h"
#include "../packet_reader.h"
#include <cassert>
#include <list>

#define UPC( _pktType, _pktClassName, _ptrName, _buffer, _len, _dst ) \
case _pktType : \
{ \
_pktClassName * _ptrName = new((void*) _dst ) _pktClassName (); \
inlen = _ptrName -> Unserialize( _buffer, _len ); \
} \
break;

int unserialize( char *buf, int max_len, UdpPacketAny &dst )
{
	UdpPacketHeader uph;
	int reallen = uph.Unserialize( buf, max_len );
	int inlen;
	if( reallen <= 0 ) { return -1; }
	switch( uph.type ) {
		UPC( HI, UdpPacketHi, uphi, buf, max_len, &dst );
		UPC( BYE, UdpPacketBye, upbye, buf, max_len, &dst );
		UPC( SHOW, UdpPacketShow, upshow, buf, max_len, &dst );
		UPC( TOUCH, UdpPacketTouch, uptouch, buf, max_len, &dst );
		UPC( CHAT, UdpPacketChat, upchat, buf, max_len, &dst );
		UPC( PKT_MSG, UdpPacketMsg, upmsg, buf, max_len, &dst );
		UPC( PERMS, UdpPacketPerms, upperms, buf, max_len, &dst );
		UPC( LOAD, UdpPacketLoad, upload, buf, max_len, &dst );
		UPC( FILEHEAD, UdpPacketFileHeader, upfhead, buf, max_len, &dst );
		UPC( FILEBODY, UdpPacketFileBody, upfbody, buf, max_len, &dst );
		UPC( TESTAUTO, UdpPacketTestAuto, uptestauto, buf, max_len, &dst );
		default:
			inlen = -2;
		break;
	};
	return inlen;
};

void sendAndRecTest( UdpPacket &pout ) {
	UdpPacketAny	dst;
	UdpPacket		*pin;
	char				buf[ MAX_UDP_PAYLOAD_SIZE ];

	printf("====== testing serialize / unserialize of type %s =============\n", pout.GetTypeName().c_str() );

	int lenout = pout.Serialize( buf, sizeof( buf ) );
	printf("sent len = %d\n", lenout );
	assert( lenout > 0 );
	int lenin = unserialize( buf, sizeof(buf), dst );
	printf("received len = %d\n", lenin );
	assert( lenout == lenin );
	pin = (UdpPacket *)&dst;
	assert( lenin > 0 );
	printf("SENT: \n%s\n", pout.ToString( ", " ).c_str() );
	printf("RECEIVED: \n%s\n\n", pin->ToString( ", " ).c_str() );
	assert( pout == *pin );
	printf("OK.\n");
};

void packetReaderTest( UdpPacket &pout ) {
	UdpPacketAny	dst;
	UdpPacket		*pin;
	char				buf[ MAX_UDP_PAYLOAD_SIZE ];

	printf("====== testing PacketRader serialize / unserialize of type %s =============\n", pout.GetTypeName().c_str() );
	if( pout.type == TESTAUTO ) {
		printf("PacketReader doesn't work on the TESTAUTO type.\n");
		return;
	}

	int lenout = pout.Serialize( buf, sizeof( buf ) );
	printf("sent len = %d\n", lenout );
	assert( lenout > 0 );

	PacketTypeT type = PacketReader::Read( buf, lenout, dst );
	
	printf("read type = %d\n", type );
	assert( type != INVALID );
	pin = (UdpPacket *)&dst;
	assert( pout == *pin );
	printf("OK.\n");
};

void vandalizeBuffer( unsigned char *buf, size_t len )
{
	size_t i = rand() % len;
	unsigned char mask = rand() & 0xff;
	mask = (mask == 0) ? 1 : mask;
	printf("%ld ^ %x, ", i, mask );
	buf[ i ] ^= mask;
};

void testHeaderDamage( UdpPacket &pout, int attempts = 100 )
{
	UdpPacketAny	dst;
	UdpPacket		*pin;
	char				buf[ MAX_UDP_PAYLOAD_SIZE ];

	printf("====== testing header damage on type %s =============\n", pout.GetTypeName().c_str() );

	int lenin, lenout;
	PacketTypeT	type;

	for( int i = 0; i < attempts; i++ ) {
		lenout = pout.Serialize( buf, sizeof( buf ) );
		vandalizeBuffer( (unsigned char *)buf, UdpPacketHeader::SerializedLength() );
		lenin = unserialize( buf, sizeof(buf), dst );
		if( lenin >= 0 ) { printf("\n"); };
		assert( lenin < 0 );

		type = PacketReader::Read( buf, sizeof(buf), dst );
		if( type != INVALID ) { printf("\n"); };
		assert( type == INVALID );

	};
	printf("\nOK.\n");
};

void testNetChunk()
{
	printf("Testing Net Chunk...\n");
	std::string origStr = "Tady je neco napsano a je uplne jedno co.";
	std::string backupStr( origStr );
	NetChunk testChunk( origStr, 0, -1, true );
	origStr = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	std::string newStr;
	testChunk.Paste( newStr );
	assert( newStr.compare( backupStr ) == 0 );
	printf("OK.\n");
};

int main() {

	testNetChunk();

	AutoPacket::StaticInit();

	typedef std::list<UdpPacket *> listT;
	listT pkts;

	netFileIdT fid = 222;
	pkts.push_back( new UdpPacketFileHeader( fid, FILE_ROLE_SUBTITLES, 987654, UdpPacketHeader::ACK|UdpPacketHeader::ABORT ) );

	NetChunk testChunk( "Takže dobrý večer! Já budu vaším dnešním jelenem.", 0, -1, true );
	pkts.push_back( new UdpPacketFileBody( fid, &testChunk, 333 ) );

	UdpPacketTestAuto *uta = new UdpPacketTestAuto( "127.0.0.1:9876", "vuuul", 1 );
	uta->number = 65535;
	pkts.push_back( uta );
	uta->SetSentTime( ClickyTime(true) );

	UdpPacketPerms *upp = new UdpPacketPerms( PERMS_ALL, 456, UdpPacketHeader::ACK ); 
	pkts.push_back( upp );

	pkts.push_back( new UdpPacketHi( "kino buran", "slave", "evals" ) );
	pkts.push_back( new UdpPacketShow( 1, "Ahoj!", 1 ) );
	pkts.push_back( new UdpPacketTouch( 4568, UdpPacketHeader::ACK ) );
	pkts.push_back( new UdpPacketBye( 678 ) );
	pkts.push_back( new UdpPacketChat( "Greetings, client!", 0xffffff ) );
	pkts.push_back( new UdpPacketMsg( I_OK, 12456 ) );

	listT::iterator it = pkts.begin();
	listT::iterator endit = pkts.end();
	while( it != endit ) {
		sendAndRecTest( **it );
		packetReaderTest( **it );
		++it;
	}

	testHeaderDamage( **pkts.begin() );

	AutoPacket::StaticCleanup();

	return 0;
};
