cmake_minimum_required(VERSION 2.8)
project(PacketTest)

## Compiler flags
## if(CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "-O0 ${CMAKE_CXX_FLAGS}")        ## Optimize
    set(CMAKE_CXX_FLAGS "-fmax-errors=2 ${CMAKE_CXX_FLAGS}")  ## Maximum errors
    set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}")        ## C++11
## endif()

set(Boost_USE_STATIC_LIBS        ON) # only find static libs
set(Boost_USE_MULTITHREADED      ON)
set(Boost_USE_STATIC_RUNTIME    OFF)
find_package(Boost COMPONENTS date_time)

 include_directories( ../../../.. )

if(Boost_FOUND)
  include_directories(${Boost_INCLUDE_DIRS})
  add_executable(test_packets test_packets.cpp
  ../udp_packet.cpp
  ../udp_packet_hi.cpp
  ../udp_packet_show.cpp
  ../udp_packet_chat.cpp
  ../udp_packet_msg.cpp
  ../net_utils.cpp
  ../auto/auto_packet.cpp
  ../../../debug_fn.cpp
  ../../../str_utils.cpp
  )
  target_link_libraries(test_packets ${Boost_LIBRARIES})

  add_executable(pkshark pkshark.cpp
  ../udp_packet.cpp
  ../udp_packet_hi.cpp
  ../udp_packet_show.cpp
  ../udp_packet_chat.cpp
  ../udp_packet_msg.cpp
  ../net_utils.cpp
  ../auto/auto_packet.cpp
  ../../../debug_fn.cpp
  ../../../str_utils.cpp
  )
  target_link_libraries(pkshark ${Boost_LIBRARIES})

endif()


