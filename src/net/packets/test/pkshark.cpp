/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../udp_packets.h"
#include "../packet_reader.h"
#include <cassert>
#include <list>

int hexDigitValue( unsigned char c )
{
	int res;
	if(( c >= '0' )&&( c <= '9' )) {
		res = (int)c - (int)'0';
	} else if(( c >= 'a' )&&( c <= 'f' )) {
		res = 10 + (int)c - (int)'a';
	} else {
		res = -1;
	}
	return res;
}

int readNum( const std::string &src, int i, int digits = 2, int multiplier = 16 )
{
	int d;
	int res = 0;

	if( (src.size() - i) < digits ) {
		printf("Strange input ending: \"%s\".\n", src.substr(i).c_str() );
		return -1;
	}

	for( int j = 0; j < digits; j++ ) {
		d = hexDigitValue( src[i+j] );
		if( d < 0 ) {
			return -1;
		}
		res += d;
		if( (j+1) < digits ) {
			res *= multiplier;
		}
	}

	printf("%u,", res );

	return res;
}

#define BACKSLASH_CASE( ccase, cchar ) \
		case ccase: \
			return cchar; \
		break;

int readSpecial( unsigned char c )
{
	switch( c ) {
		BACKSLASH_CASE( 'a', '\a' )
		BACKSLASH_CASE( 'b', '\b' )
		BACKSLASH_CASE( 'f', '\f' )
		BACKSLASH_CASE( 'n', '\n' )
		BACKSLASH_CASE( 'r', '\r' )
		BACKSLASH_CASE( 't', '\t' )
		BACKSLASH_CASE( 'v', '\v' )
		BACKSLASH_CASE( '"', '"' )
		BACKSLASH_CASE( '\'', '\'' )
		BACKSLASH_CASE( '\\', '\\' )
		BACKSLASH_CASE( '?', '?' )
	}
	return -1;
}

size_t readGdb(std::string &src, unsigned char *dst, size_t maxLen )
{
	size_t srcLen = src.size();
	size_t i = 0;
	size_t curLen = 0;
	int c;

	if( src.empty() ) { return 0; }

	while(( i < srcLen )&&( curLen < maxLen )) {
		if( src[i] == '\\' ) {
			++i;
			if( i < srcLen ) {
				if(( src[i] >= '0' )&&( src[i] <= '9' )) {
					c = readNum( src, i, 3, 8 );
					i += 3;
				} else {
					c = readSpecial( src[i] );
					++i;
				}
			} else{
				printf("Bad string ending \"%s\" at offset %ld\n", src.substr(i-1).c_str(), i );
				exit(1);
			}
			if( c < 0 ) {
				printf("Bad input \"%s\" at offset %ld\n", src.substr(i,2).c_str(), i );
				exit(1);
			}
		} else {
			c = src[i];
			++i;
		}
		dst[0] = (unsigned char)( c );
		++dst;
		++curLen;
	}
	return curLen;
}

size_t readShark( std::string &src, unsigned char *dst, size_t maxLen )
{
	size_t srcLen = src.size();
	size_t i = 0;
	size_t curLen = 0;
	int c;

	if( src.empty() ) { return 0; }

	while(( i < srcLen )&&( curLen < maxLen )) {
		c = readNum( src, i, 2, 16 );
		if( c < 0 ) {
			printf("Bad input \"%s\" at offset %ld\n", src.substr(i,2).c_str(), i );
			exit(1);
		}
		dst[0] = (unsigned char)( c );
		++dst;
		++curLen;
			i += 3;
	}
	return curLen;
}


int main( int argc, char *argv[] ) {

	AutoPacket::StaticInit();

	if( argc < 2 ) {
		printf("Usage: wirepacket [g] <data from wireshark>\n");
		exit(1);
	}

	std::string sharkStr;
	char	pkBuf[2048];
	size_t pkLen;

	if(( argc > 2 )&&( argv[1][0] == 'g' )) { 
		sharkStr = argv[2];
		pkLen = readGdb(  sharkStr, (unsigned char*)pkBuf, sizeof(pkBuf) );
	} else {
		sharkStr = argv[1];
		size_t pkLen = readShark( sharkStr, (unsigned char*)pkBuf, sizeof(pkBuf) );
	}
	if( pkLen == 0 ) {
		printf("I don't understand what the shark meant by that. Sorry!\n");
		exit(1);
	}

	UdpPacketAny	dst;
	PacketTypeT type = PacketReader::Read( pkBuf, pkLen, dst );
	if( type == INVALID ) {
		printf("The packet is invalid.\n");
		exit(1);
	}

	UdpPacket *pk = (UdpPacket*)&dst;
	printf("\n--- Packet ---\n%s\n--- ---\n", pk->ToString("\n").c_str() );

	return 0;
}
