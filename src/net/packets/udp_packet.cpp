/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "udp_packet.h"
#include "../../str_utils.h"
#include "boost/crc.hpp"

#include <climits>

//------------------- UdpPacketHeader ----------------

int UdpPacketHeader::Serialize( char *buffer, int max_len ) const
{
	if( max_len < serialized_length ) { return -1; };

	const char *start = buffer;

	serializeInt32( number, &buffer );

	size_t timelen = sentTime.Serialize( buffer );
	buffer += timelen;

	timelen = origSentTime.Serialize( buffer );
	buffer += timelen;

	buffer[0] = (char)type;
	buffer[1] = (char)flags;
	buffer += 2;

	unsigned int crc = getCRC16( start, buffer - start );
	serializeInt16( crc, &buffer );

	return SerializedLength();
};

int UdpPacketHeader::Unserialize( const char *buffer, int len )
{
	if( len < SerializedLength() ) { return -1; }

	const char *start = buffer;

	number = unserializeInt32( &buffer );

	size_t timelen = sentTime.Unserialize( buffer );
	buffer += timelen;

	timelen = origSentTime.Unserialize( buffer );
	buffer += timelen;

	type = (PacketTypeT)((unsigned char*)buffer)[0];
	flags = (FlagsT)((unsigned char*)buffer)[1];
	buffer += 2;

	unsigned int crc = getCRC16( start, SerializedLength() );
	if( crc != 0 ) { return -2; }

	return SerializedLength();
};

void UdpPacketHeader::ToString( std::string &dst, const char *separator ) const
{
	std::string flagStr;
	if( IsAck() ) { flagStr.append("ACK "); };
	//if( IsError() ) { flagStr.append("ERR "); };
	StrUtils::Printf( dst, "#:%d%st=%d (%s)%sf=%x (%s)%ssent: %s%sorig: %s%s",
							number, separator,
							type,
							typeName.c_str(), separator,
							flags,
							flagStr.c_str(), separator,
							sentTime.ToNiceString().c_str(), separator,
							origSentTime.ToNiceString().c_str(), separator );
};

UdpPacketHeader *UdpPacketHeader::FromRaw( const char *buffer, int len )
{
	if(( len >= 0 )&&( len < serialized_length )) { return NULL; }
	UdpPacketHeader *uph = new UdpPacketHeader();
	size_t usedlen = uph->Unserialize( buffer, len );
	if( usedlen != serialized_length ) {
		delete uph;
		uph = NULL;
	}
	return uph;
};

unsigned int UdpPacketHeader::getCRC16( const char *data, size_t len )
{
	boost::crc_16_type result;
   result.process_bytes( data, len );
   return result.checksum();
};

//--------------------- UdpPacket ----------------------

int UdpPacket::Serialize( char *buffer, int max_len ) const
{
	if( max_len < 0 ) { max_len = INT_MAX; }
	size_t headerlen = UdpPacketHeader::Serialize( buffer, max_len );
	if( headerlen < 0 ) { return headerlen; };
	buffer += headerlen;
	max_len -= headerlen;
	if( max_len < serializedLength() ) { return -1; };
	size_t datalen = serialize( buffer, max_len );
	return (datalen >= 0) ? (headerlen + datalen) : datalen;
}

int UdpPacket::Unserialize( const char *buffer, int len )
{
	if( len < 0 ) { len = INT_MAX; }
	size_t headerlen = UdpPacketHeader::Unserialize( buffer, len );
	if( headerlen < 0 ) { return headerlen; }
	len -= headerlen;
	buffer += headerlen;
	if( len < 0 ) { return -1; };
	size_t datalen = unserialize( buffer, len );
	return (datalen >= 0) ? (headerlen + datalen) : datalen;
}

void UdpPacket::ToString( std::string &dst, const char *separator ) const
{
	UdpPacketHeader::ToString( dst, separator );
	toString( dst, separator );
};

bool UdpPacket::IsEqual( const UdpPacket &other, bool inclHeader )
{
	if( inclHeader ) {
		if( *((UdpPacketHeader*)this) != *((UdpPacketHeader*)&other) ) { return false; }
	} else {
		if( type != other.type ) { return false; }
	}
	return isEqual( other );
}
