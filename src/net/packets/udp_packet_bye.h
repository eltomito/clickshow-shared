/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Udp_Packet_Bye_h
#define _Udp_Packet_Bye_h 1

#include "udp_packet.h"

/*
 * This kind of packet is sent to end a connection.
 */
class UdpPacketBye : public UdpPacket {
public:
	UdpPacketBye(	unsigned int _number = UdpPacket::NO_NUMBER,
						FlagsT _flags = (FlagsT)0 ) : UdpPacket( BYE, "Bye", _number, _flags ) {};
	~UdpPacketBye() {};

	size_t serialize( char *buffer, int max_len ) const { return 0; };
	size_t unserialize( const char *buffer, int len = -1 ) { return 0; };
	size_t serializedLength() const { return 0; };
	void toString( std::string &dst, const char *separator = "\n" ) const {};
	bool isEqual( const UdpPacket &other ) const { return true; };
};

#endif //_Udp_Packet_Bye_h
