/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "udp_packet_chat.h"
#include "../../str_utils.h"

#include <cstring>
#include <climits>

size_t UdpPacketChat::serialize( char *buffer, int max_len ) const
{
	char *dst = buffer;
	return serializeStr( chatText, &dst, &max_len );
}

size_t UdpPacketChat::unserialize( const char *buffer, int len )
{
	const char *src = buffer;
	chatText = unserializeStr( &src, &len );
	return src - buffer;
};

size_t UdpPacketChat::serializedLength() const
{
	int max_len = MAX_PAYLOAD_SIZE;
	return serializedStrLen( chatText, &max_len );
};

void UdpPacketChat::toString( std::string &dst, const char *separator ) const
{
	StrUtils::Printf( dst, "chat: %s%s", chatText.c_str(), separator );
};
