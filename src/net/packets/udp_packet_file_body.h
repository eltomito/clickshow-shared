/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Udp_Packet_FileBody_h
#define _Udp_Packet_FileBody_h 1

#include "udp_packet.h"
#include "auto/auto_packet.h"
#include "shared/src/net/net_chunk.h"

/** A part of the content of a file being transfered.
 */
class UdpPacketFileBody : public AutoPacket
{
public:
	UdpPacketFileBody( netFileIdT _id = 0,
							const NetChunk *_data = NULL,
							unsigned int _number = UdpPacket::NO_NUMBER,
						FlagsT _flags = (FlagsT)0 )
						: AutoPacket( AutoPacket::pdFileBody, _number, _flags ),
						id(_id)
	{
		if( _data !=NULL ) { data = *_data; }
	};

	~UdpPacketFileBody() {};

	bool Overlap( UdpPacketFileBody *other )
	{
		if( other == NULL ) { return false; }
		return data.Overlap( other->data );
	};

	void MakeOwnDataCopy() { data.CopyUnowned(); };
	bool Paste( char *dst, size_t dstLen )	{ return data.Paste( dst, dstLen );	};
	bool Paste( std::string &dst )	{ return data.Paste( dst );	};

	static const size_t OVERHEAD_LEN = 32;
	static size_t MaxPayloadSize() { return MAX_UDP_PAYLOAD_SIZE - UdpPacketHeader::SerializedLength() - OVERHEAD_LEN; };

	netFileIdT	id;
	NetChunk		data;
};

#endif //_Udp_Packet_FileBody_h
