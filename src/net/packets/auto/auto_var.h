/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Auto_Var_h
#define _Auto_Var_h 1

#include "shared/src/default_log.h"
#include "../udp_packet.h"
#include "../net_utils.h"
#include "shared/src/net/node/user_perms.h"
#include "shared/src/net/net_types.h"
#include "shared/src/net/net_chunk.h"
#include "shared/src/clicky_time.h"
#include "shared/src/serialize_double.h"

enum VarT
{
	VART_NONE,
	VART_BYTE,
	VART_SHORT,
	VART_INT,
	VART_ULONG,
	VART_INT48,
	VART_INT64,
	VART_STRING,
	VART_CHUNK,
	VART_USER_PERMS,
	VART_FILE_ID,
	VART_FILE_ROLE,
	VART_CLICKYTIME,
	VART_DOUBLE,
	VART_BOOL
};

/** The base class for all AutoPacket variables.
*/
class PacVar
{
protected:
	PacVar( VarT _type, const std::string &_name, size_t _offset )
		: type(_type), name(_name), offset(_offset) {};
	PacVar() : type( VART_NONE ), name("") {};
public:
	virtual ~PacVar() {};

	VarT GetType() const { return type; };
	const std::string &GetName() const { return name; };

	virtual size_t Serialize( const UdpPacket &src, char **dst, int *max_len ) const { LOG_UNIMP; return 0; };
	virtual size_t Unserialize( UdpPacket &dst, const char **src, int *max_len ) const { LOG_UNIMP; return 0; };
	virtual size_t SerializedLength( const UdpPacket &src ) const { LOG_UNIMP; return 0; };
	virtual std::string ToString( const UdpPacket &src ) const { LOG_UNIMP; return "<unimplemented: ToString()>"; };
	virtual bool IsEqual( const UdpPacket &one, const UdpPacket &other ) const { LOG_UNIMP; return false; };
	virtual void copy( const UdpPacket &src, UdpPacket &dst ) const { LOG_UNIMP; };
protected:
	VarT			type;
	std::string	name;
	size_t		offset;
};

/** The String AutoPacket variable.
*/
class PacVarStr : public PacVar
{
public:
	PacVarStr( const std::string &_name, size_t _offset ) : PacVar( VART_STRING, _name, _offset ) {};
	~PacVarStr() {};

	virtual size_t Serialize( const UdpPacket &src, char **dst, int *max_len ) const
	{
		std::string *s = (std::string *)( ((char*)&src) + offset );
		return serializeStr( *s, dst, max_len );
	};
	virtual size_t Unserialize( UdpPacket &dst, const char **src, int *max_len ) const
	{
		std::string *s = (std::string *)( ((char*)&dst) + offset );
		new( (void*)s) std::string;
		const char *start = *src;
		*s = unserializeStr( src, max_len );
		return *src - start;
	};
	virtual size_t SerializedLength( const UdpPacket &src ) const
	{
		std::string *s = (std::string *)( ((char*)&src) + offset );
		return s->size() + 1;
	};
	virtual std::string ToString( const UdpPacket &src ) const
	{
		std::string *s = (std::string *)( ((char*)&src) + offset );
		return *s;
	};
	virtual bool IsEqual( const UdpPacket &one, const UdpPacket &other ) const
	{
		std::string *s = (std::string *)( ((char*)&one) + offset );
		std::string *t = (std::string *)( ((char*)&other) + offset );
		return 0 == s->compare( *t );
	};
	virtual void copy( const UdpPacket &src, UdpPacket &dst ) const
	{
		std::string *s = (std::string *)( ((char*)&src) + offset );
		std::string *t = (std::string *)( ((char*)&dst) + offset );
		new ( (void*)t )std::string(*s);
	};
};

//----------- number-serialization routines used in the template for numeric variables --------------

template<typename numT, int byteW>
size_t serializeNum( numT value, char **dst, int *max_len )
{
	for( int i = 0; i < byteW; ++i ) {
		(*dst)[0] = (char)(value & 0xff);
		++(*dst);
		value = value>>8;
	}
	*max_len -= byteW;
	return byteW;
}

template<typename numT, int byteW>
numT unserializeNum( const char **src, int *maxLen )
{
	if( byteW > *maxLen ) {
		*src += *maxLen;
		*maxLen = 0;
		return 0;
	}
	
	numT value = 0;
	for( int i = 0; i < byteW; ++i ) {
		value |= ( 0xff & ((numT)((*src)[0])) ) << (i*8);
		++(*src);
	}
	return value;
}

/** template-based itoa().
 */
template<typename numT, int byteW>
void StringFromNum( std::string &dst, numT n )
{
	if( byteW <= sizeof(int) ) {		
		StrUtils::Printf( dst, "%d", n );
		return;
	}
	if( byteW <= sizeof(long) ) {		
		StrUtils::Printf( dst, "%ld", n );
		return;
	}
	StrUtils::Printf( dst, "%ld", (long)n );
};

//--------------------------- PacVarNum template ---------------------

/** A template for deriving numeric AutoPacket variable types from.
 */
template<typename numT, VarT typeName, int byteW>
class PacVarNum : public PacVar
{
public:
	PacVarNum( const std::string &_name, size_t _offset )
	: PacVar( typeName, _name, _offset ) {};
	~PacVarNum() {};

	size_t Serialize( const UdpPacket &src, char **dst, int *max_len ) const
	{
		numT *iii = (numT *)( ((char*)&src) + offset );
		return serializeNum<numT, byteW>( *iii, dst, max_len );
	};

	size_t Unserialize( UdpPacket &dst, const char **src, int *max_len ) const
	{
		numT *iii = (numT*)( ((char*)&dst) + offset );
		const char *start = *src;
		*iii = unserializeNum<numT, byteW>( src, max_len );
		return *src - start;
	};
	size_t SerializedLength( const UdpPacket &src ) const { return byteW; };
	std::string ToString( const UdpPacket &src ) const
	{
		numT *iii = (numT*)( ((char*)&src) + offset );
		std::string dst;
		StringFromNum<numT, byteW>( dst, *iii );
		return dst;
	};
	bool IsEqual( const UdpPacket &one, const UdpPacket &other ) const
	{
		numT *iii = (numT*)( ((char*)&one) + offset );
		numT *jjj = (numT*)( ((char*)&other) + offset );
		return *iii == *jjj;
	};
	void copy( const UdpPacket &src, UdpPacket &dst ) const
	{
		numT *iii = (numT*)( ((char*)&src) + offset );
		numT *jjj = (numT*)( ((char*)&dst) + offset );
		*jjj = *iii;
	};

};

//------------------- Numeric Variable Types --------------------

#define DEF_NUM_VAR_TYPE( _typeExt, _baseType, _typeId, _byteWidth ) \
class PacVar##_typeExt : public PacVarNum< _baseType, _typeId, _byteWidth > \
{ \
public: \
	PacVar##_typeExt ( const std::string &_name, size_t _offset ) : PacVarNum< _baseType, _typeId, _byteWidth >( _name, _offset ) {}; \
	~PacVar##_typeExt () {}; \
};

DEF_NUM_VAR_TYPE( Int, int, VART_INT, 4 )
DEF_NUM_VAR_TYPE( Short, short, VART_SHORT, 2 )
DEF_NUM_VAR_TYPE( ULong, unsigned long, VART_ULONG, 4 )
DEF_NUM_VAR_TYPE( Int64, boost::int64_t, VART_INT64, 8 )
DEF_NUM_VAR_TYPE( Int48, boost::int64_t, VART_INT48, 6 )
DEF_NUM_VAR_TYPE( UserPerms, UserPermsT, VART_USER_PERMS, 4 )
DEF_NUM_VAR_TYPE( FileId, netFileIdT, VART_FILE_ID, 4 )
DEF_NUM_VAR_TYPE( FileRole, netFileRoleT, VART_FILE_ROLE, 4 )

//------------- NetChunk ---------

/** The data Chunk auto variable
 */
class PacVarChunk : public PacVar
{
protected:
	size_t maxLen;
public:
	PacVarChunk( const std::string &_name, size_t _offset,
	 				  size_t _maxLen = MAX_UDP_PAYLOAD_SIZE / 2 )
	 				  : PacVar( VART_CHUNK, _name, _offset ),
	 				  	maxLen(_maxLen)
	 				  {};
	~PacVarChunk() {};

	virtual size_t Serialize( const UdpPacket &src, char **dst, int *max_len ) const
	{
		NetChunk *ch = ( (NetChunk*) (((char*)&src) + offset) );

		char *start = *dst;

		serializeInt32( ch->GetDstOffset(), dst, max_len );
		serializeInt32( ch->GetLen(), dst, max_len );
		if( ch->PasteData( *dst, *max_len ) ) {
			*dst += ch->GetLen();
			*max_len -= ch->GetLen();
		}
		return *dst - start;
	};

	virtual size_t Unserialize( UdpPacket &dst, const char **src, int *max_len ) const
	{
		const char *start = *src;
		NetChunk *ch = ( (NetChunk*) (((char*)&dst) + offset) );
		size_t o = unserializeInt32( src, max_len );
		size_t l = unserializeInt32( src, max_len );
		ch->Copy( *src, l, o, true );
		*src += l;
		*max_len -= l;
		return *src - start;
	};

	virtual size_t SerializedLength( const UdpPacket &src ) const
	{
		NetChunk *ch = ( (NetChunk*) (((char*)&src) + offset) );
		return 8 + ch->GetLen();
	};

	virtual std::string ToString( const UdpPacket &src ) const
	{
		NetChunk *ch = ( (NetChunk*) (((char*)&src) + offset) );
		return ch->ToString(",");
	};

	virtual bool IsEqual( const UdpPacket &one, const UdpPacket &other ) const
	{
		NetChunk *s = (NetChunk *)( ((char*)&one) + offset );
		NetChunk *t = (NetChunk *)( ((char*)&other) + offset );
		return *s == *t;
	};

	virtual void copy( const UdpPacket &src, UdpPacket &dst ) const
	{
		NetChunk *s = (NetChunk *)( ((char*)&src) + offset );
		NetChunk *d = (NetChunk *)( ((char*)&dst) + offset );
		new ( (void*)d )NetChunk( *s );
	};
};

/** ClickyTime AutoPacket variable
 */
class PacVarTime : public PacVar
{
public:
	PacVarTime( const std::string &_name, size_t _offset ) : PacVar( VART_CLICKYTIME, _name, _offset ) {};
	~PacVarTime() {};

	virtual size_t Serialize( const UdpPacket &src, char **dst, int *max_len ) const
	{
		ClickyTime *ct = (ClickyTime *)( ((char*)&src) + offset );
		size_t len = ct->Serialize( *dst );
		*dst += len;
		return len;
	};

	virtual size_t Unserialize( UdpPacket &dst, const char **src, int *max_len ) const
	{
		ClickyTime *ct = (ClickyTime *)( ((char*)&dst) + offset );
		size_t len = ct->Unserialize( *src );
		*src += len;
		return len;
	};

	virtual size_t SerializedLength( const UdpPacket &src ) const
	{
		return ClickyTime::serializedLength;
	};

	virtual std::string ToString( const UdpPacket &src ) const
	{
		ClickyTime *ct = (ClickyTime *)( ((char*)&src) + offset );
		return ct->ToString();
	};

	virtual bool IsEqual( const UdpPacket &one, const UdpPacket &other ) const
	{
		ClickyTime *s = (ClickyTime *)( ((char*)&one) + offset );
		ClickyTime *t = (ClickyTime *)( ((char*)&other) + offset );
		return *s == *t;
	};

	virtual void copy( const UdpPacket &src, UdpPacket &dst ) const
	{
		ClickyTime *s = (ClickyTime *)( ((char*)&src) + offset );
		ClickyTime *t = (ClickyTime *)( ((char*)&dst) + offset );
		new ( (void*)t )ClickyTime(*s);
	};
};

/** double AutoPacket variable
 */
class PacVarDouble : public PacVar
{
public:
	PacVarDouble( const std::string &_name, size_t _offset ) : PacVar( VART_DOUBLE, _name, _offset ) {};
	~PacVarDouble() {};

	virtual size_t Serialize( const UdpPacket &src, char **dst, int *max_len ) const
	{
		double d = *((double *)( ((char*)&src) + offset ));
		size_t len = SerializeDouble::serialize_double( d, (unsigned char *)*dst, (size_t)*max_len );
		*dst += len;
		return len;
	};

	virtual size_t Unserialize( UdpPacket &dst, const char **src, int *max_len ) const
	{
		double *dptr = (double *)( ((char*)&dst) + offset );
		size_t bytes_used = 0;
		*dptr = SerializeDouble::deserialize_double( (unsigned char*)*src, (size_t)*max_len, bytes_used );
		*src += bytes_used;
		return bytes_used;
	};

	virtual size_t SerializedLength( const UdpPacket &src ) const
	{
		return DBL_SERIALIZED_BYTES;
	};

	virtual std::string ToString( const UdpPacket &src ) const
	{
		std::string dst;
		double d = *((double *)( ((char*)&src) + offset ));
		StrUtils::Printf( dst, "%lf", d );
		return dst;
	};

	virtual bool IsEqual( const UdpPacket &one, const UdpPacket &other ) const
	{
		double d = *((double *)( ((char*)&one) + offset ));
		double e = *((double *)( ((char*)&other) + offset ));
		return d == e;
	};

	virtual void copy( const UdpPacket &src, UdpPacket &dst ) const
	{
		double d = *((double *)( ((char*)&src) + offset ));
		double *eptr = (double *)( ((char*)&dst) + offset );
		*eptr = d;
	};
};

/** bool AutoPacket variable
 */
class PacVarBool : public PacVar
{
public:
	PacVarBool( const std::string &_name, size_t _offset ) : PacVar( VART_BOOL, _name, _offset ) {};
	~PacVarBool() {};

	virtual size_t Serialize( const UdpPacket &src, char **dst, int *max_len ) const
	{ 
		bool d = *((bool *)( ((char*)&src) + offset ));
		**dst = d ? (char)1: (char)0;
		*dst += 1;
		return 1;
	};

	virtual size_t Unserialize( UdpPacket &dst, const char **src, int *max_len ) const
	{
		bool *d = ((bool *)( ((char*)&dst) + offset ));
		*d = (bool)( (*src)[0] );
		*src += 1;
		return 1;
	};

	virtual size_t SerializedLength( const UdpPacket &src ) const
	{
		return 1;
	};

	virtual std::string ToString( const UdpPacket &src ) const
	{
		bool d = *((bool *)( ((char*)&src) + offset ));
		return std::string( d ? "true" : "false" );
	};

	virtual bool IsEqual( const UdpPacket &one, const UdpPacket &other ) const
	{
		bool d = *((bool *)( ((char*)&one) + offset ));
		bool e = *((bool *)( ((char*)&other) + offset ));
		return d == e;
	};

	virtual void copy( const UdpPacket &src, UdpPacket &dst ) const
	{
		bool d = *((bool *)( ((char*)&src) + offset ));
		bool *eptr = (bool *)( ((char*)&dst) + offset );
		*eptr = d;
	};
};

#endif //_Auto_Var_h
