/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Auto_Packet_h
#define _Auto_Packet_h 1

#include "shared/src/default_log.h"
#include "../udp_packet.h"
#include "../net_utils.h"
#include "auto_var.h"

// ------------------------------- PacDesc ---------------------------

/** Description of an auto-packet variable
 */
class PacDesc
{
friend class AutoPacket;
protected:
	typedef std::vector<const PacVar*> varsT;
public:
	PacDesc() : type(INVALID), typeName("Invalid") { vars.clear(); };
	PacDesc( PacketTypeT _type, const std::string &_typeName ) : type(_type), typeName(_typeName) {	vars.clear(); };
	~PacDesc() {};

	void Init( PacketTypeT _type, const std::string &_typeName ) { type = _type; typeName = _typeName; };

	PacketTypeT GetType() const { return type; };
	void AddVar( const PacVar *_var ) {	vars.push_back(_var); };
	void FreeAllVars() {
		varsT::iterator it = vars.begin();
		varsT::iterator endit = vars.end();
		while( it != endit ) {
			free( ((PacVar*)*it) );
			++it;
		};
		vars.clear();
	};

	size_t Serialize( const UdpPacket &pkt, char **dst, int *max_len ) const
	{
		varsT::const_iterator it = vars.begin();
		varsT::const_iterator endit = vars.end();
		size_t len = 0;
		while( it != endit ) {
			len += (*it)->Serialize( pkt, dst, max_len );
			++it;
		};
		return len;
	};

	size_t Unserialize( UdpPacket &pkt, const char **src, int *max_len ) const
	{
		varsT::const_iterator it = vars.begin();
		varsT::const_iterator endit = vars.end();
		size_t len = 0;
		while( it != endit ) {
			len += (*it)->Unserialize( pkt, src, max_len );
			++it;
		};
		return len;
	};

	size_t SerializedLength( const UdpPacket &pkt ) const
	{
		varsT::const_iterator it = vars.begin();
		varsT::const_iterator endit = vars.end();
		size_t len = 0;
		while( it != endit ) {
			len += (*it)->SerializedLength( pkt );
			++it;
		};
		return len;
	};
	std::string ToString( const UdpPacket &pkt, const char *sep = "\n" ) const
	{
		std::string dst;
		varsT::const_iterator it = vars.begin();
		varsT::const_iterator endit = vars.end();
		while( it != endit ) {
			dst.append( (*it)->GetName() );
			dst.append("=");
			dst.append( (*it)->ToString( pkt ) );
			++it;
			if( it != endit ) { dst.append(sep); }
		};
		return dst;
	};
	bool IsEqual( const UdpPacket &one, const UdpPacket &other ) const
	{
		varsT::const_iterator it = vars.begin();
		varsT::const_iterator endit = vars.end();
		while( it != endit ) {
			if( !(*it)->IsEqual( one, other ) ) { return false; }
			++it;
		};
		return true;
	};
	
	void copy( const UdpPacket &src, UdpPacket &dst ) const
	{
		varsT::const_iterator it = vars.begin();
		varsT::const_iterator endit = vars.end();
		while( it != endit ) {
			(*it)->copy( src, dst );
			++it;
		};
	};

protected:
	PacketTypeT		type;
	std::string		typeName;
	varsT				vars;
};

/** The base class for packets with automatic serialization, etc.
 */
class AutoPacket : public UdpPacket
{
public:
	AutoPacket( const PacDesc &_desc,
					unsigned int _number = UdpPacket::NO_NUMBER,
					FlagsT _flags = (FlagsT)0  )
					: UdpPacket( _desc.type, _desc.typeName.c_str(), _number, _flags ), desc( &_desc) {};
	~AutoPacket() {};

	size_t serialize( char *buffer, int max_len ) const
	{
		if( max_len < 0 ) { max_len = MAX_PAYLOAD_SIZE; }
		return desc->Serialize( *this, &buffer, &max_len );
	};
	size_t unserialize( const char *buffer, int len = -1 )
	{
		if( len < 0 ) { len = MAX_PAYLOAD_SIZE; }
		return desc->Unserialize( *this, &buffer, &len );
	};
	size_t serializedLength() const { return desc->SerializedLength( *this ); };
	void toString( std::string &dst, const char *separator = "," ) const { dst.append( desc->ToString(*this, separator) ); };
	bool isEqual( const UdpPacket &other ) const { return desc->IsEqual( *this, other ); };

	AutoPacket &operator=( const AutoPacket &other ) {
		//header
		number = other.number;
		type = other.type;
		flags = other.flags;
		SetSentTime( other.GetSentTime() );
		SetTypeName( other.GetTypeName() );
		//content
		desc = other.desc;
		desc->copy( other, *this );
		return *this;
	};

	//-- static
	static void StaticInit();
	static void StaticCleanup();

	//packet descriptions of individual packet types
	static PacDesc pdTestAuto;
	static PacDesc pdPerms;
	static PacDesc pdLoad;
	static PacDesc pdFileHeader;
	static PacDesc pdFileBody;
	static PacDesc pdPlay;

private:
	const PacDesc *desc;
};

/** A packet type for testing the AutoPacket class.
 */
class UdpPacketTestAuto : public AutoPacket
{
public:
	UdpPacketTestAuto( const std::string &a = "", const std::string &n = "", int x = 0 )
		: AutoPacket( AutoPacket::pdTestAuto ), address(a), name(n), id(x) {};
	~UdpPacketTestAuto() {};

	std::string address;
	std::string name;
	int			id;
};

#endif //_Auto_Packet_h
