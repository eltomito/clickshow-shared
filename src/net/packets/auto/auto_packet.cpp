/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "auto_packet.h"
#include "../udp_packet_perms.h"
#include "../udp_packet_load.h"
#include "../udp_packet_file_header.h"
#include "../udp_packet_file_body.h"
#include "../udp_packet_play.h"

//---- definitions of packet descriptions
PacDesc AutoPacket::pdPerms;
PacDesc AutoPacket::pdLoad;
PacDesc AutoPacket::pdFileHeader;
PacDesc AutoPacket::pdFileBody;
PacDesc AutoPacket::pdPlay;
PacDesc AutoPacket::pdTestAuto;

void AutoPacket::StaticInit()
{
	//UdpPacketPerms
	pdPerms.Init( PERMS, "Perms" );
	pdPerms.AddVar( new PacVarUserPerms( "perms", offsetof( class UdpPacketPerms, perms) ) );

	//UdpPacketLoad
	pdLoad.Init( LOAD, "Load" );
	pdLoad.AddVar( new PacVarFileId( "fileId", offsetof( class UdpPacketLoad, fileId) ) );

	//UdpFileHeader
	pdFileHeader.Init( FILEHEAD, "FileHeader" );
	pdFileHeader.AddVar( new PacVarFileId( "id", offsetof( class UdpPacketFileHeader, id) ) );
	pdFileHeader.AddVar( new PacVarFileRole( "role", offsetof( class UdpPacketFileHeader, role) ) );
	pdFileHeader.AddVar( new PacVarInt( "length", offsetof( class UdpPacketFileHeader, length) ) );

	//UdpFileBody
	pdFileBody.Init( FILEBODY, "FileBody" );
	pdFileBody.AddVar( new PacVarFileId( "id", offsetof( class UdpPacketFileBody, id) ) );
	pdFileBody.AddVar( new PacVarChunk( "data", offsetof( class UdpPacketFileBody, data) ) );

	//UdpPlay
	pdPlay.Init( PLAY, "Play" );
	pdPlay.AddVar( new PacVarTime( "startTimestamp", offsetof( class UdpPacketPlay, startTimestamp ) ) );
	pdPlay.AddVar( new PacVarDouble( "startTimecode", offsetof( class UdpPacketPlay, startTimecode ) ) );
	pdPlay.AddVar( new PacVarDouble( "delay", offsetof( class UdpPacketPlay, delay ) ) );
	pdPlay.AddVar( new PacVarDouble( "speed", offsetof( class UdpPacketPlay, speed ) ) );
	pdPlay.AddVar( new PacVarBool( "isPlaying", offsetof( class UdpPacketPlay, isPlaying ) ) );
	pdPlay.AddVar( new PacVarInt( "shownSub", offsetof(class UdpPacketPlay,shownSub) ) );
	pdPlay.AddVar( new PacVarInt( "nextSub", offsetof(class UdpPacketPlay,nextSub) ) );

	//--- AutoPacket test packet type
	pdTestAuto.Init( TESTAUTO, "TestAuto" );
	pdTestAuto.AddVar( new PacVarInt( "id", offsetof(class UdpPacketTestAuto,id) ) );
	pdTestAuto.AddVar( new PacVarStr( "address", offsetof(class UdpPacketTestAuto,address) ) );
	pdTestAuto.AddVar( new PacVarStr( "name", offsetof(class UdpPacketTestAuto,name) ) );
}

void AutoPacket::StaticCleanup()
{
	pdPerms.FreeAllVars();
	pdLoad.FreeAllVars();
	pdFileHeader.FreeAllVars();
	pdFileBody.FreeAllVars();
	pdPlay.FreeAllVars();
	pdTestAuto.FreeAllVars();
}
