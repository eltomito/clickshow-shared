/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Udp_Packet_Show_h
#define _Udp_Packet_Show_h 1

#include "udp_packet.h"

class UdpPacketShow : public UdpPacket {
public:
	UdpPacketShow(	unsigned int _number = UdpPacket::NO_NUMBER,
						FlagsT _flags = (FlagsT)0 ) : UdpPacket( SHOW, "Show", _number, _flags ) {};
	UdpPacketShow(	int _subNum,
						const std::string &_subText,
						unsigned int _number = UdpPacket::NO_NUMBER,
						FlagsT _flags = (FlagsT)0 ) : UdpPacket( SHOW, "Show", _number, _flags ) { subNum = _subNum; subText = _subText; };
	~UdpPacketShow() { subText.clear(); };

	bool IsNumberless() const { return subNum == -1; };
	bool IsHide() const { return subNum == -2; };
	void MakeNumberless() { subNum = -1; };
	void MakeHide() { subNum = -2; };

	size_t serialize( char *buffer, int max_len ) const;
	size_t unserialize( const char *buffer, int len = -1 );
	size_t serializedLength() const;
	void toString( std::string &dst, const char *separator = "\n" ) const;
	bool isEqual( const UdpPacket &other ) const { return (( subNum == ((UdpPacketShow*)&other)->subNum )&&( subText == ((UdpPacketShow*)&other)->subText )); };

	int			subNum;
	std::string	subText;
};

#endif //_Udp_Packet_Show_h
