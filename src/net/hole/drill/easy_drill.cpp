/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "easy_drill.h"

void EasyDrill::StateChanged( HttpConState newState )
{
	switch( newState ) {
		case HCS_CONNECTED:
			holeOpen();
		break;
		case HCS_DISCONNECTED:
		case HCS_ERR_FAILED:
			holeClosed();
		break;
	}
}

bool forceHostPort( std::string &dst, const std::string serverAddr, unsigned short forcedPort )
{
	std::string hostName;
	std::string portStr;
	if( !AsioUtils::SplitHostAndPort( serverAddr, hostName, portStr ) ) {
		return false;
	}
	dst.clear();
	StrUtils::Printf(dst, "%s:%u", hostName.c_str(), forcedPort );
	return true;
}

bool EasyDrill::Start( unsigned short inUdpPort, const std::string &serverAddr, unsigned short forcedPort, const std::string &proxyAddr )
{
	LOCK_ME;

	if( IsRunning() ) { stop(); }

	ioService.reset();

	std::string sa = serverAddr;

	if( forcedPort != 0 ) {
		if( forceHostPort( sa, serverAddr, forcedPort ) ) {
			LOG_HOLE_INFO("Forced port to %u. New server addr: \"%s\"\n", forcedPort, sa.c_str() );		
		} else {
			LOG_HOLE_ERROR("FAILED to force host port to %d on \"%s\"!\n", (int)forcedPort, serverAddr.c_str() );
			return false;
		}
	}

	drill = new HoleDrill( ioService, this, inUdpPort );
	if( drill == NULL ) {
		LOG_HOLE_ERROR("FAILED to create a new HoleDrill!\n");
		return false;
	}
	if( !drill->Start( sa, proxyAddr ) )
	{
		LOG_HOLE_ERROR("FAILED to start my brand new HoleDrill!\n");
		return false;
	}
	if( !startThread() ) {
			LOG_HOLE_ERROR("FAILED to start the net thread.\n");
			return false;
	}
	return true;
}

void EasyDrill::Stop()
{
	LOCK_ME;
	stop();
}

void EasyDrill::stop()
{
	if( drill != NULL ) { drill->Stop(); }
	ioService.stop();
	if( netThread != NULL ) {
		LOG_HOLE_INFO("Waiting for the netThread to finish...\n");
		netThread->join();
		LOG_HOLE_INFO("netThread finished.\n");
		delete netThread;
		netThread = NULL;
	}
	if( drill != NULL ) {
		delete drill;
		drill = NULL;
	}
}

bool EasyDrill::startThread()
{
	if( netThread != NULL ) {
		return false;
	}
	netThread = new boost::thread( boost::bind( &boost::asio::io_service::run, &ioService ) );
	return( netThread != NULL );
}

