/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../hole_drill.h"
#include "../easy_drill.h"
#include "shared/src/net/asio_utils.h"


class MyED : public EasyDrill
{
public:
	MyED() {};
	~MyED() {};

	void holeOpen() {
		printf("The Hole is OPEN!\n");
	};
	void holeClosed() {
		printf("The Hole is CLOSED!\n");
	};
};

/*
class MyEmitter : public HoleDrillEmitter
{
public:
	MyEmitter() {};
	~MyEmitter() {};

	void StateChanged( HttpConState newState ) {
		printf("HTTP STATE CHANGED TO %d\n", newState );
	};
};
*/

int main( int argc, char* argv[] )
{
	boost::asio::io_service ioService;
	//MyEmitter emitter;

	unsigned short udpPort = 12345;
	std::string holeServer = "localhost:8080";

	if( argc > 1 ) {
		udpPort = atoi(argv[1]);
		if( udpPort == 0 ) {
			printf("Bad port number! %s\n", argv[1]);
			printf("Usage: test_drill [<port_number>] [<serverAddress>]\n");
			exit(1);
		}
		if( argc > 2 ) {
			holeServer = argv[2];
		}
	}

	MyED med;
	
	if( !med.Start( udpPort, holeServer ) ) {
		printf("Failed to start EasyDrill!\n");
		exit(1);
	}
	printf("EasyDrill started, listening on port %u.\n", udpPort );

/*
	HoleDrill drill( ioService, &emitter, udpPort );
	drill.Start( holeServer );

	boost::thread *netThread = new boost::thread( boost::bind( &boost::asio::io_service::run, &ioService ) );
	if( netThread == NULL ) {
		printf("Thread creation failed.\n");
		exit(1);
	}

	printf("test drill started, listening on port %u.\n", udpPort );
*/

   char inBuf[ 255 ];
   ssize_t inlen;
   bool alive = true;
	while( alive ) {
		inlen = read( 0, inBuf, sizeof(inBuf) );
		if(inlen > 0) {
			if( ( inlen > 1 )&&( inBuf[0] == '/' ) ) {
				switch( inBuf[1] )
				{
					case 'q':
						printf("Bye!\n");
						alive = false;
					break;
					case 's':
						printf("Stopping the drill...\n");
						med.Stop();
					break;
					case 'r':
						printf("Restarting the drill...\n");
						if( !med.Start( udpPort, holeServer ) ) {
							printf("Failed to start EasyDrill!\n");
						}
					break;
				}
			} else {
				printf("Commands:\n/q - Quit.\n");
			}
		}
	}

	med.Stop();
/*
	ioService.stop();
	delete netThread;
	
	drill.Stop();
*/
}
