/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Easy_Drill_h
#define _Easy_Drill_h 1

#include "boost/asio.hpp"
#include "boost/thread.hpp"
#include "hole_drill.h"
#include "shared/src/net/dns_resolver.h"
#include "shared/src/net/asio_utils.h"

class EasyDrill : public Lockable, public HoleDrillEmitter
{
public:
	EasyDrill()
	:	drill(NULL),
		netThread(NULL) {};
	~EasyDrill() { Stop(); };

	bool Start( unsigned short inUdpPort, const std::string &serverAddr, unsigned short forcedPort = 0, const std::string &proxyAddr = "" );
	void Stop();

	bool IsRunning() { return (( drill!=NULL )&&( netThread!=NULL )&&( !ioService.stopped() )); };
	bool IsConnected() { return ( IsRunning() && drill->IsHttpConnected() ); };

	std::string GetServer() { return ( drill != NULL ) ? drill->GetServer() : ""; };
	unsigned short GetUdpPort() { return ( drill != NULL ) ? drill->GetUdpPort() : 0; };

//emitter
	void StateChanged( HttpConState newState );

//	unsigned short FirstFreeLocalUdpPort( unsigned short minp = 10000, unsigned short maxp = 65000, unsigned short step = 64, bool randomStep = true );

protected:
	virtual void holeOpen() {};
	virtual void holeClosed() {};
	void stop();

protected:
	bool startThread();

protected:
	boost::asio::io_service	ioService;

	HoleDrill		*drill;
	boost::thread	*netThread;
};

#endif //_Easy_Drill_h
