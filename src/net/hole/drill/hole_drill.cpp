/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "hole_drill.h"

HoleDrill::HoleDrill(	boost::asio::io_service &_ioService,
								HoleDrillEmitter *_emit,
								unsigned short _inUdpPort,
								long _minHttpMs,
								long _maxHttpMs )
:	ioService(_ioService),
	tickTimer(_ioService),
	client(_ioService),
	inUdpPort(_inUdpPort ),
	minHttpMs(_minHttpMs),
	maxHttpMs(_maxHttpMs),
	httpRec(*this),
	tickMs(defaultTickMs),
	emitter(_emit),
	lastHttpRecTime(true),
	lastHttpSendTime(true),
	httpConnectTimeout( 5000 ),
	httpTunnelTimeout( 0 ),
	holeCon( NULL )
{
	headersPkts["Content-Type"] = "application/octet-stream";
	headersCtrl["Content-Type"] = "text/plain";
	httpState = HCS_DISCONNECTED;
}

HoleDrill::~HoleDrill()
{
	if( holeCon != NULL ) {
		delete holeCon;
		holeCon = NULL;
	}
}

bool HoleDrill::Start( const std::string &_server, const std::string &_proxy )
{
	LOG_HOLE_INFO("Starting hole drill to server %s%s%s ...\n",
		_server.c_str(),
		!_proxy.empty() ? " via proxy " : "",
		_proxy.c_str() );

	server = _server;
	client.SetProxy( _proxy );

	startTickTimer();

	if( !hreqCtrl.Init( METH_PUT, server, "open", headersCtrl, "" ) ) {
		LOG_HOLE_ERROR("FAILED to init the control request.\n");
		return false;
	}
	if( client.RequestInProgress() )
	{
		client.CancelRequest();
	}
	SetHttpState( HCS_CONNECTING );
	return client.MakeRequest( &httpRec, &hreqCtrl );
}

void HoleDrill::Stop()
{
	stopTickTimer();
	SetHttpState( HCS_DISCONNECTED );
}

void HoleDrill::openCon( const std::string &_path )
{
	if( holeCon == NULL ) {
		try {
			holeCon = new HoleCon( ioService, inUdpPort, _path );
		} catch (...) {
			LOG_HOLE_ERROR("FAILED to create HoleCon with port %u path \"%s\".\n", inUdpPort, _path.c_str() );
		}
	} else {
		holeCon->SetPath(_path);
	}
	if(( holeCon != NULL )&&( holeCon->Start() )) {
		inUdpPort = holeCon->GetPort();
		SetHttpState( HCS_CONNECTED );
		LOG_HOLE_TRAFFIC("We are connected to %s as \"%s\" on udp port %u.\n", server.c_str(), _path.c_str(), inUdpPort );
	} else {
		SetHttpState( HCS_DISCONNECTED );
		LOG_HOLE_ERROR("Failed to open connection to %s as \"%s\".\n", server.c_str(), _path.c_str() );
	}
}

void HoleDrill::receiveHttp( HttpRequest &req )
{
	LOCK_ME

	lastHttpRecTime.SetToNow();

	LOG_HOLE_INFO("Request completed in %ld ms, status code = %d.\n", (long)(lastHttpRecTime - req.GetStartedTime()), req.statusCode );

	if( &req == &hreqPkts ) {

		if( req.statusCode != 200 ) {
			LOG_HOLE_ERROR("HTTP pkts status code = %d\n", req.statusCode );
			return;
		}
		if( req.inBody.empty() ) {
			LOG_HOLE_TRAFFIC("HTTP pkts contains no data.\n" );
			return;
		}
		if( holeCon == NULL ) {
			LOG_HOLE_TRAFFIC("No connection is open!\n" );
			return;
		}
		holeCon->ReceiveHttp( (char *)req.inBody.data(), req.inBody.size() );

	} else { //hreqCtrl
		if( req.statusCode != 200 ) {
			LOG_HOLE_ERROR("HTTP ctrl status code = %d\n", req.statusCode );
			SetHttpState( HCS_DISCONNECTED );
			return;
		}
		if( req.inBody.empty() ) {
			LOG_HOLE_ERROR("HTTP ctrl contains no data.\n" );
			SetHttpState( HCS_DISCONNECTED );
			return;
		}
		std::string _path = req.inBody;
		StrUtils::TrimInPlace( _path );
		openCon( _path );
	}
}

void HoleDrill::sendHttp()
{
	if( !IsHttpConnected() ) {
		LOG_HOLE_DETAIL2("Not connected.\n");
		return;
	}

	if( holeCon == NULL ) {
		LOG_HOLE_DETAIL2("No holeCon.\n");
		return;
	}

	ClickyTime nowTime(true);
	if( (nowTime - lastHttpSendTime) < minHttpMs ) {
		LOG_HOLE_DETAIL2("Too soon.\n");
		return;
	}

	if( client.RequestInProgress() ) {
		LOG_HOLE_DETAIL2("Request in progress.\n");
		return;
	}

	size_t len = holeCon->SendHttp( outData, sizeof( outData ) );

	lastHttpSendTime.SetToNow();
	hreqPkts.Init( METH_PUT, server, holeCon->GetPath(), headersPkts, outData, len ); 
	client.MakeRequest( &httpRec, &hreqPkts );
}

void HoleDrill::doChores()
{
	LOCK_ME

	ClickyTime nowTime(true);

	if( lastHttpRecTime.IsValid() ) {
		if(( httpTunnelTimeout > 0 )&&( nowTime - lastHttpRecTime >= httpTunnelTimeout )) {
			SetHttpState( HCS_DISCONNECTED );
		}
	}

	if( IsHttpConnecting() && hreqCtrl.IsActive() && ( hreqCtrl.GetStartedTime() - nowTime > httpConnectTimeout )) {
		LOG_HOLE_INFO("Connection timeout.\n");
		client.CancelRequest();
		SetHttpState( HCS_DISCONNECTED );
	}

	if( holeCon != NULL ) {
		holeCon->DoChores();
	}

	if( !lastHttpSendTime.IsValid() || ( nowTime - lastHttpSendTime >= maxHttpMs )) {
		sendHttp();
	}
}

void HoleDrill::SetHttpState( HttpConState _state )
{
	if( httpState == _state ) {
		return;
	}
	httpState = _state;
	if( emitter != NULL ) {
		emitter->StateChanged( httpState );
	}
}

void HoleDrill::startTickTimer()
{
	tickTimer.expires_from_now( boost::posix_time::milliseconds( tickMs ) );
	tickTimer.async_wait( boost::bind( &HoleDrill::handleTimer, this, boost::asio::placeholders::error ) );
}

void HoleDrill::stopTickTimer()
{
		tickTimer.cancel();
}

void HoleDrill::handleTimer(const boost::system::error_code& errc )
{
	if( (long)errc.value() != 0 ) {
		if( errc == boost::asio::error::operation_aborted ) {
			LOG_NET_INFO("Abort -> not restarting the timer.\n");
			return;
		}
		LOG_NET_ERROR("error code %ld: \"%s\"\n", (long)errc.value(), errc.message().c_str() );
		startTickTimer();
		return;
	}

	doChores();
	startTickTimer();
}

// ------ class DrillHttpReceiver -------

void DrillHttpReceiver::Receive( HttpRequest &req )
{
		drill.receiveHttp( req );
}
