/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Hole_Drill_h
#define _Hole_Drill_h 1

#include "shared/src/net/http/client/http_client.h"
#include "shared/src/net/hole/udp_pack.h"
#include "shared/src/net/hole/blob_queue.h"
#include "shared/src/net/hole/hole_store.h"
#include "shared/src/net/hole/hole_con.h"
#include "shared/src/semaph.h"
#include "shared/src/clicky_time.h"
#include "shared/src/default_log.h"
#include "hole_drill_emitter.h"

class HoleDrill;

class DrillHttpReceiver : public HttpReceiver
{
public:
	DrillHttpReceiver( HoleDrill &_drill )
	:	drill(_drill)
	{};
	~DrillHttpReceiver() {};
	void Receive( HttpRequest &req );

protected:
	HoleDrill &drill;
};

class HoleDrill : public Lockable
{
//friend class DrillUdpReceiver;
friend class DrillHttpReceiver;

public:
	static const size_t	maxOutDataSize = 10000;
	static const long		defaultTickMs = 100;
	static const long		defaultMaxUdpSpentMs = 100;

	HoleDrill(	boost::asio::io_service &_ioService,
					HoleDrillEmitter *_emit = NULL,
					unsigned short _inUdpPort = 0,
					long _minHttpMs = 100,			//100
					long _maxHttpMs = 300 );		//500
	~HoleDrill();

	bool Start( const std::string &_server, const std::string &_proxy = "" );
	void Stop();

	bool IsHttpConnected() { return httpState == HCS_CONNECTED; };
	bool IsHttpConnecting() { return httpState == HCS_CONNECTING; };
	bool IsHttpDisconnected() { return httpState == HCS_DISCONNECTED; };

	void SetHttpState( HttpConState _state );

	const std::string		&GetServer() const { return server; };
	unsigned short			GetUdpPort() const { return inUdpPort; };

protected:
	void receiveHttp( HttpRequest &req );

	void sendHttp();
	void doChores();

	void openCon( const std::string &_path );

	void handleTimer(const boost::system::error_code& errc );
	void startTickTimer();
	void stopTickTimer();

protected:
	HoleCon					*holeCon;
	HttpClient				client;
	DrillHttpReceiver		httpRec;
	HoleDrillEmitter		*emitter;

	std::string		server;
	unsigned short inUdpPort;

	HttpRequest	hreqPkts;
	HttpHeadersT headersPkts;

	HttpRequest	hreqCtrl;
	HttpHeadersT headersCtrl;

	long			minHttpMs;
	long			maxHttpMs;
	long			maxUdpSpentMs;
	long			tickMs;
	long			httpTunnelTimeout;	//if no http gets received for this many milliseconds, the tunnel is considered disconnected
	long			httpConnectTimeout;

	ClickyTime	lastHttpRecTime;
	ClickyTime	lastHttpSendTime;

	HttpConState	httpState;

	char outData[ maxOutDataSize ];

	boost::asio::io_service &ioService;
	boost::asio::deadline_timer	tickTimer;
};

#endif //_Hole_Drill_h
