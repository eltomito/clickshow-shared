/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Hole_Drill_Emitter_h
#define _Hole_Drill_Emitter_h 1


typedef int HttpConState;
static const HttpConState HCS_INVALID = -666;
static const HttpConState HCS_DISCONNECTED = 0;
static const HttpConState HCS_CONNECTING = 1;
static const HttpConState HCS_CONNECTED = 2;
static const HttpConState HCS_ERR_FAILED = -1;

class HoleDrillEmitter
{
public:
	HoleDrillEmitter() {};
	~HoleDrillEmitter() {};

	virtual void StateChanged( HttpConState newState ) {};
};

#endif //_Hole_Drill_Emitter_h
