/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Hole_Store_h
#define _Hole_Store_h 1

#include "blob_queue.h"

class HoleStore
{
public:
	HoleStore( size_t maxSize = 100000 ) : inQ( maxSize ), outQ(maxSize ) {};
	~HoleStore() {};

	void DigestHttp( const char *buf, size_t len ) { inQ.AppendSequence( buf, len ); };
	//size_t ExtractHttp( const char *buf, size_t maxLen  ) { return outQ.ExtractSequence( buf, maxLen ); };
	size_t ExtractHttp( char *dst, size_t maxLen  ) { return outQ.ExtractSequence( dst, maxLen ); };

	void DigestUdp( const char *buf, size_t len ) { outQ.AppendOne( buf, len ); };
	size_t ExtractUdp( char *dst, size_t maxLen  ) { return inQ.ExtractOne( dst, maxLen ); };

	size_t OutSize() { return outQ.Size(); };
	size_t InSize() { return inQ.Size(); };

	bool HasInData() { return InSize() > 0; };
	bool HasOutData() { return OutSize() > 0; };

	BlobQueue &GetInQueue() { return inQ; };
	BlobQueue &GetOutQueue() { return outQ; };

protected:
	BlobQueue	inQ;	//in the hole means from http to udp
	BlobQueue	outQ;	//out of the hole means from udp to http
};


#endif // _Hole_Store_h
