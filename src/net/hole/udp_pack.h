/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Udp_Pack_h
#define _Udp_Pack_h 1

#include "boost/asio.hpp"
#include <boost/bind.hpp>
#include <vector>

class UdpPackPool;

class UdpPack
{
public:
	typedef std::string dataT;

	UdpPack( UdpPackPool *_pool = NULL, const boost::asio::ip::udp::endpoint &ep = boost::asio::ip::udp::endpoint(), const char *buf = NULL, size_t len = 0 )
	: pool(_pool), udpEp( ep )
	{
		Set( buf, len );
	};
	~UdpPack() {};

	size_t Size() { return data.size(); };
	void Set( const char *buf, size_t len );
	bool Write( char *dst, size_t maxLen );
	const char *Get() { return data.c_str(); };
	boost::asio::ip::udp::endpoint &GetEndpoint() { return udpEp; };
	void HandleSent( const boost::system::error_code& errc, size_t bytesSent );

protected:
	UdpPackPool *pool;
	boost::asio::ip::udp::endpoint	udpEp;
	dataT			data;
};

#endif //_Udp_Pack_h
