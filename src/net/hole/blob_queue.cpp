/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "blob_queue.h"
#include "shared/src/default_log.h"
#include <cstring>

//--- BQHead ---

size_t BQHead::paddedLenFor( char *ptr, size_t len )
{
	size_t nx = (size_t)ptr + sizeof(BQHead) + len;
	size_t rem = nx & (paddingTo - 1);
	return ( nx + (( paddingTo - rem ) & ( paddingTo - 1 ))- (size_t)ptr );
}

size_t BQHead::WriteTo( char *dst, size_t maxLen ) const
{
	if( GetDataLen() == 0 ) {
		return 0;
	}
	if( maxLen < GetDataLen() ) {
		return -1;
	}
	memcpy( (void*)dst, (void*)GetDataStart(), GetDataLen() );
	return GetDataLen();	
}

std::string BQHead::ToString( const std::string &sep ) const
{
	std::string dst;
	StrUtils::Printf( dst, "len: %ld, skip: %ld", GetDataLen(), (size_t)GetNext() - (size_t)this );
	return dst;
}

//-- BlobQueue --

BlobQueue::BlobQueue( size_t maxSize )
{
	flags = BQF_CAN_WRAP | BQF_OWN;
	allocQueue( maxSize );
	Clear();
}

BlobQueue::BlobQueue( char *_queue, size_t _len, BQFlagsT _flags, bool keepData )
{
	flags = _flags;
	queue = _queue;
	queueSize = _len;

	if( !keepData ) {
		Clear();
	} else {
		outPtr = queue;
		inPtr = getMaxWrapPtr();
		wrapPtr = NULL;
	}
}

BlobQueue::~BlobQueue()
{
	if(( OwnsBuffer() )&&( queue != NULL )) {
		free( queue );
		queue = NULL;
	}
}

void BlobQueue::allocQueue( size_t _len )
{
	if( _len < (sizeof(BQHead) + BQHead::paddingTo) ) {
		queue = NULL;
	} else {
		queue = (char*)malloc( _len );
	}
	if( queue != NULL ) {
		queueSize = _len;
	} else {
		queueSize = 0;
	}
}

void BlobQueue::Clear()
{
	inPtr = queue;
	outPtr = queue;
	wrapPtr = NULL;
}

size_t BlobQueue::Size() const
{
	if( !isWrapped() ) {
		return (inPtr >= outPtr) ? (inPtr - outPtr) : 0;
	}
	return (size_t)( (wrapPtr - outPtr) + (inPtr - queue) );
}

BQHead *BlobQueue::pop_front()
{
	if( Empty() ) {
		return NULL;
	}
	BQHead *res = (BQHead*)outPtr;
	outPtr = (char*)( ((BQHead*)outPtr)->GetNext() );
	if( isWrapped() ) {
		if( outPtr >= wrapPtr ) {
			outPtr = queue;
			wrapPtr = NULL;
		}
	} else { //not wrapped
		if( outPtr == inPtr ) {
			inPtr = queue;
			outPtr = queue;
		}
	}
	return res;
}

BQHead *BlobQueue::push_back( size_t len )
{
	BQHead *bqh;
	size_t bytesNeeded = BQHead::GetSizeFor( len );

	if( bytesNeeded > Capacity() ) {
		return NULL;
	}

	if( !CanWrap() ) {

		if( getSpaceAtInPtr() < bytesNeeded ) {
			return NULL;
		}

	} else { //we can wrap

		while( getSpaceForPushBack() < bytesNeeded ) {
			pop_front();
		}
		if( !isWrapped() && ( getSpaceAtInPtr() < bytesNeeded )) {
			wrapPtr = inPtr;
			inPtr = queue;
		}
	}
	bqh = BQHead::CreateAt( inPtr, len );
	inPtr = (char*)bqh->GetNext();
	return bqh;
}

bool BlobQueue::AppendOne( const char *buf, size_t len )
{
	BQHead *bqh = push_back( len );
	if( bqh == NULL ) {
		return false;
	}
	memcpy( (void*)bqh->GetDataStart(), (void*)buf, len );
	return true;
}

bool BlobQueue::AppendOne( const BQHead &bqh )
{
	return AppendOne( bqh.GetDataStart(), bqh.GetDataLen() );
}

int BlobQueue::AppendSequence( BlobQueue &other )
{
	int cnt = 0;
	bool res;
	BQHead *bqh;
	while( !other.Empty() ) {
		bqh = other.pop_front();

		LOG_HOLE_DETAIL2("Appending BQHead %s\n", bqh->ToString().c_str() );

		if( bqh->GetTotalLen() <= Capacity() ) {
			res = AppendOne( *bqh );
			if( !res && !CanWrap() ) {
				break;
			}
			cnt += res ? 1 : 0;
		}
	}
	return cnt;
}

int BlobQueue::AppendSequence( const char *start, size_t dataLen )
{
	BlobQueue tmpq( (char *)start, dataLen, 0, true );
	return AppendSequence( tmpq );
}

/**
 * @returns - -1 .. not enough space,
 *          -  0 .. no data to extract
 *          - >0 .. amount of data written or size of data that would be written if dst werent NULL 
 */
size_t BlobQueue::ExtractOne( char *dst, size_t maxLen )
{
	if( Empty() ) {
		return 0;
	}
	if( dst == NULL ) {
		return ((BQHead*)outPtr)->GetDataLen();
	}
	BQHead *bqh = pop_front();
	return bqh->WriteTo( dst, maxLen );
}

size_t BlobQueue::ExtractSequence( char *dst, size_t maxLen )
{
	size_t res = 0;
	BlobQueue bq( dst, maxLen, 0, false );
	bq.AppendSequence( *this );
	return bq.Size();
}

std::string BlobQueue::ToString( const std::string &sep, int verbosity ) const
{
	std::string dst;
	StrUtils::Printf( dst, "cap: %ld%ssize: %ld",
		Capacity(), sep.c_str(),
		Size() );

	if( verbosity >= 1 ) {
		dst.append( sep );
		if( !isWrapped() ) {
			if(outPtr != queue ) {
				StrUtils::Printf( dst, "GAP( %ld )%s", outPtr - queue, sep.c_str() );
			}
			StrUtils::Printf( dst, "DATA( %ld )%s", inPtr - outPtr, sep.c_str() );
			StrUtils::Printf( dst, "GAP( %ld )", getMaxWrapPtr() - inPtr );
		} else {
			StrUtils::Printf( dst, "DATA( %ld )%s", inPtr - queue, sep.c_str() );
			StrUtils::Printf( dst, "GAP( %ld )%s", outPtr - inPtr, sep.c_str() );
			StrUtils::Printf( dst, "DATA( %ld )%s", wrapPtr - outPtr, sep.c_str() );
			StrUtils::Printf( dst, "GAP( %ld )", getMaxWrapPtr() - wrapPtr );
		}
	}
	return dst;
}

