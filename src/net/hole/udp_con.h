/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Udp_Con_h
#define _Udp_Con_h 1

#include "udp_pack_pool.h"

using boost::asio::ip::udp;

class UdpCon
{
public:
	static const size_t MAX_UDP_DATA_LEN = 2048;

	UdpCon(	boost::asio::io_service &_ioService,
				unsigned short _localPort )
	: ioService(_ioService),
	localPort(_localPort),		//0 means let the OS pick a port number
	udpSocket(_ioService, boost::asio::ip::udp::endpoint(udp::v4(), _localPort ) ),
	inDataBuffer( inDataRaw, sizeof(inDataRaw ) )
	{};

	~UdpCon() { Stop(); };

	bool	Start();
	bool	Stop();
	bool	Restart();

	bool Send( boost::asio::ip::udp::endpoint &ep, const char *buf, size_t len );

	unsigned short GetPort()
	{
		boost::asio::ip::udp::endpoint ep = udpSocket.local_endpoint();
		return ep.port();
	};

protected:
	virtual void receive( boost::asio::ip::udp::endpoint &ep, char *buf, size_t len ) {};

protected:
	void handleReceiveFrom( const boost::system::error_code& errc, size_t bytesRecvd );
	void listen();

protected:
	boost::asio::io_service			&ioService;
	boost::asio::ip::udp::socket	udpSocket;
	unsigned short				localPort;

	boost::asio::ip::udp::endpoint	inEp;

	UdpPackPool		pool;

	char								inDataRaw[ MAX_UDP_DATA_LEN ];
	boost::asio::mutable_buffers_1		inDataBuffer;
};

#endif //_Udp_Con_h
