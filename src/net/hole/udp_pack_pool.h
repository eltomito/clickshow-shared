/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Udp_Pack_Pool_h
#define _Udp_Pack_Pool_h 1

#include "boost/asio.hpp"
#include "shared/src/async_pool.h"
#include "udp_pack.h"

class UdpPackPool : public AsyncPool<UdpPack>
{
public:
	UdpPackPool() {};
	~UdpPackPool() {};

	UdpPack *Add( const boost::asio::ip::udp::endpoint &ep, const char *buf, size_t len )
	{
		UdpPack *np = new UdpPack( this, ep, buf, len );
		if( np == NULL ) {
			return NULL;
		}
		if( !AsyncPool<UdpPack>::AddCon( np ) ) {
			return NULL;
		}
		return np;
	};
};

#endif //_Udp_Pack_Pool_h
