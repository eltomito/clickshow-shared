/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "udp_con.h"
#include "shared/src/net/asio_utils.h"

#if 0
bool UdpCon::Restart()
{
	try {
		udpSocket = boost::asio::ip::udp::socket(ioService, boost::asio::ip::udp::endpoint(udp::v4(), localPort ) );
/*
		udpSocket.open( boost::asio::ip::udp::v4() );
		udpSocket.connect( boost::asio::ip::udp::endpoint( udp::v4(), localPort ) );
*/
	} catch(...) {
		return false;
	}
	listen();
	return true;
}
#endif //0

bool UdpCon::Start()
{
	listen();
	return true;
}

bool UdpCon::Stop()
{
	try {
		udpSocket.shutdown( boost::asio::ip::tcp::socket::shutdown_send );
		udpSocket.close();
	} catch(...) {
		return false;
	}
	return true;
}

void UdpCon::listen()
{
	udpSocket.async_receive_from(
		inDataBuffer,
		inEp,
		boost::bind(	&UdpCon::handleReceiveFrom, this,
							boost::asio::placeholders::error,
							boost::asio::placeholders::bytes_transferred
		)
	);
}

void UdpCon::handleReceiveFrom( const boost::system::error_code& errc, size_t bytesRecvd )
{
	LOG_HOLE_DETAIL("received udp, dataLen = %ld\n", bytesRecvd );

	if( (long)errc.value() != 0 ) {
		LOG_HOLE_ERROR("!!! error code %ld: \"%s\"\n", (long)errc.value(), errc.message().c_str() );
		if( errc == boost::asio::error::operation_aborted ) {
			LOG_HOLE_INFO("Abort -> not receiving anymore.\n");
			return;
		}

	} else {
		receive( inEp, inDataRaw, bytesRecvd );
	}
	listen();
}

bool UdpCon::Send( boost::asio::ip::udp::endpoint &ep, const char *buf, size_t len )
{
	if( !udpSocket.is_open() ) {
		LOG_HOLE_ERROR("Socket not open!\n");
		return false;
	}
	UdpPack *pk = pool.Add( ep, buf, len );
	if( pk == NULL ) {
		LOG_HOLE_ERROR("FAILED to add a new UdpPack to the pool!\n");
		return false;
	}
	LOG_HOLE_TRAFFIC("Sending packet to endpoint %s.\n", AsioUtils::EndpointToString(ep).c_str() );

	udpSocket.async_send_to(	boost::asio::buffer(	pk->Get(),
															pk->Size()
												),
								pk->GetEndpoint(),
								boost::bind(	&UdpPack::HandleSent, pk,
													boost::asio::placeholders::error,
													boost::asio::placeholders::bytes_transferred
												)
							);
	return true;
}
