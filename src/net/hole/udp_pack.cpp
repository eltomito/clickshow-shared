/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "udp_pack.h"
#include "udp_pack_pool.h"

void UdpPack::Set( const char *buf, size_t len )
{
	if( len > 0 ) {
		data.assign( buf, len );
	} else {
		data.clear();
	}
}

bool UdpPack::Write( char *dst, size_t maxLen )
{
	if( maxLen < Size() ) { return false; }
	memcpy( (void*)dst, (void*)Get(), Size() );
	return true;
}

void UdpPack::HandleSent( const boost::system::error_code& errc, size_t bytesSent )
{
	if( pool != NULL )
	{
		pool->DeleteMe( this );
	}
}
