/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Hole_Con_Pool_h
#define _Hole_Con_Pool_h 1

#include "boost/asio.hpp"
#include "shared/src/async_pool.h"
#include "hole_con.h"

class HoleConPool : public AsyncPool<HoleCon>
{
public:
	HoleConPool() : pathIndex(cons) {};
	~HoleConPool() {};

	HoleCon *FindByPath( const std::string &path )
	{
		LOCK_ME;
		HoleCon **hcpp = pathIndex.Find( path ); 
		return (hcpp != NULL ) ? *hcpp : NULL;
	};

protected:
	void onAdd( HoleCon *cptr, ConIterT it )
	{
		pathIndex.Insert( cptr->GetPath(), it );
	};

	void onRem( HoleCon *cptr )
	{
		pathIndex.Erase( cptr->GetPath() );
	};

	void onClear()
	{
		pathIndex.Clear();
	};
protected:
	ListIndexer< typename AsyncPool<HoleCon>::ConListT, HoleCon*, std::string> pathIndex;
};

#endif //_Hole_Con_Pool_h
