/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../blob_queue.h"


void appendStr( BlobQueue &bq, const std::string &str )
{
	bq.AppendOne( str.c_str(), str.size() );
}

void dumpStrBlob( const BQHead &bqh, bool metaData = false )
{
	std::string str( bqh.GetDataStart(), bqh.GetDataLen() );
	if( !metaData ) {
		printf("%s", str.c_str() );
	} else {
		printf("(%s)%s", bqh.ToString().c_str(), str.c_str() );
	}
}

void dumpStrQueue( const std::string &queueName, const BlobQueue &bq, bool metaData = false )
{
	printf("-- %s --\n", queueName.c_str() );
	printf("%s\n", bq.ToString().c_str() );
	printf("---------\n");
	
	BlobQueue::const_iterator it = bq.begin();
	BlobQueue::const_iterator endit = bq.end();

	while( it != endit )
	{
		dumpStrBlob( *it, metaData );
		if( metaData ) { printf("\n"); }
		++it;
	}
	printf("\n");
}

int main()
{
	printf("------------------\n");

	BlobQueue bq( 64 );
	appendStr( bq, "abcd" );
	appendStr( bq, "efgh" );
	appendStr( bq, "ijkl" );
	appendStr( bq, "mnop" );

	dumpStrQueue( "src", bq, true );
	BlobQueue res( 64 );
	res.AppendSequence( bq );
	dumpStrQueue( "res", res, true );

	printf("------------------\n");

	appendStr( bq, "ABCD" );
	dumpStrQueue( "src", bq, true );
	appendStr( bq, "EDFG" );
	dumpStrQueue( "src", bq, true );
	appendStr( bq, "IJKLMNOPQRST" );
	dumpStrQueue( "src", bq, true );
	appendStr( bq, "UVWX" );
	dumpStrQueue( "src", bq, true );

	res.AppendSequence( bq );
	dumpStrQueue( "res", res, true );

	printf("------------------\n");

	appendStr( bq, "1" );
	dumpStrQueue( "src", bq, true );
	appendStr( bq, "1234567" );
	dumpStrQueue( "src", bq, true );
	appendStr( bq, "8" );
	dumpStrQueue( "src", bq, true );
	appendStr( bq, "9abcdefghijklmnop" );
	dumpStrQueue( "src", bq, true );

	res.AppendSequence( bq );
	dumpStrQueue( "res", res, true );

	return 0;
}
