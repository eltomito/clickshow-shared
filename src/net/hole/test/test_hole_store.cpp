/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../hole_store.h"

class Mailer
{
public:
	Mailer(	size_t drillSize,
				size_t holeSize,
				size_t _drillWatermark )
	:	drill( drillSize ),
		hole( holeSize ),
		drillWatermark( _drillWatermark ) {};

	void DrillSend( std::string msg )
	{
		printf("DRILL SEND: %s\n", msg.c_str() );
		drill.DigestUdp( msg.c_str(), msg.size() );
		drillDoChores();
	};

	void HoleSend( std::string msg )
	{
		printf("HOLE SEND : %s\n", msg.c_str() );
		hole.DigestUdp( msg.c_str(), msg.size() );
	};

protected:
	void drillDoChores()
	{
		if( drill.OutSize() >= drillWatermark ) {
			size_t len = drill.ExtractHttp( drillBuf, sizeof(drillBuf) );
			if( len > 0 ) {
				hole.DigestHttp( drillBuf, len );
				holeReceiveHttp();
			}
		}
	};

	void holeReceiveHttp()
	{
		size_t len;
		while( hole.HasInData() ) {
			len = hole.ExtractUdp( holeBuf, sizeof( holeBuf ) );
			holeProcessUdp( holeBuf, len );
		};

		if( hole.HasOutData() ) {
			len = hole.ExtractHttp( holeBuf, sizeof(holeBuf) );
			drill.DigestHttp( holeBuf, len );
			drillReceiveHttp();
		};
	};

	void drillReceiveHttp() {
		size_t len;
		while( drill.HasInData() ) {
			len = drill.ExtractUdp( drillBuf, sizeof( drillBuf ) );
			drillProcessUdp( drillBuf, len );
		};
	};

	virtual void holeProcessUdp( char *buf, size_t len )
	{
		std::string str( buf, len);
		printf("HOLE REC  : %s\n", str.c_str() );
	};

	virtual void drillProcessUdp( char *buf, size_t len )
	{
		std::string str( buf, len);
		printf("DRILL REC : %s\n", str.c_str() );
	};

protected:
	char drillBuf[10000];
	char holeBuf[10000];

	size_t drillWatermark;
	HoleStore drill;
	HoleStore hole;
};

int main()
{
	Mailer m( 64, 64, 20 );
	m.HoleSend("Mam tu pro tebe spoustu veci!");
	m.HoleSend("Treba kalhoty,");
	m.HoleSend("pak taky backory,");
	m.HoleSend("a vybornej hovezi ovar.");

	m.DrillSend("Ahoj!");
	m.DrillSend("Jak se mas?");
	m.DrillSend("Reknes");
	m.DrillSend("mi");
	m.DrillSend("uz neco?");

	return 0;
}
