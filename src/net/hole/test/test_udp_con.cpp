/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../udp_con.h"
#include "shared/src/net/asio_utils.h"
#include <boost/bind.hpp>

boost::asio::ip::udp::endpoint outEp;
bool autoReply = true;

class UdpC : public UdpCon
{
public:
	UdpC(	boost::asio::io_service &_ioService,
			unsigned short _localPort )
	: UdpCon( _ioService, _localPort) {};
	~UdpC() {};

protected:
	void receive(  boost::asio::ip::udp::endpoint &ep, char *buf, size_t len )
	{
		printf("RECEIVED from %s: %s\n", AsioUtils::EndpointToString(ep).c_str(), std::string( buf, len ).c_str() );
		if( autoReply ) {
			outEp = ep;
		}
	};
};

int main( int argc, char *argv[] )
{
	unsigned short localPort = 12345;

	std::string epStr;


	if( argc > 1 ) {
		localPort = atoi( argv[1] );
		if( localPort == 0 ) {
			printf("Bad local port: %s\n", argv[1] );
			exit(1);
		}
	}

	if( !AsioUtils::StringToEndpoint( "127.0.0.1:12345", outEp, 12345 ) ) {
		printf("FAILED to create endpoint.\n");
		exit(1);
	};

	//----

	std::vector<std::string> gunAmmo;
	bool capAmmo = false;

	boost::asio::io_service ioService;
	UdpC uc( ioService, localPort ); 

	uc.Start();

	boost::thread *netThread = new boost::thread( boost::bind( &boost::asio::io_service::run, &ioService ) );
	if( netThread == NULL ) {
		printf("Thread creation failed.\n");
		exit(1);
	}

	printf("Listening on port %u. Type /h for help.\n", localPort );

	int i;

   char inBuf[ 255 ];
   ssize_t inlen;
   bool alive = true;
	while( alive ) {
		inlen = read( 0, inBuf, sizeof(inBuf) );
		if(inlen > 0) {
			if( ( inlen > 1 )&&( inBuf[0] == '/' ) ) {
				switch( inBuf[1] )
				{
					case 'q':
						printf("Bye!\n");
						alive = false;
					break;
					case 's':
						printf("Stopping UdpCon.\n");
						uc.Stop();
					break;
					case 'g':
						printf("=== FIRE! === (%ld)\n", gunAmmo.size() );
						for( i = 0; i < gunAmmo.size(); i++ ) {
							//printf("->%s\n", gunAmmo[i].c_str() );
							uc.Send( outEp, gunAmmo[i].c_str(), gunAmmo[i].size() );
						}
						printf("==== END FIRE ====\n");
					break;
					case 'c':
						capAmmo = !capAmmo;
						printf("capture is %s\n", capAmmo ? "ON" : "OFF" );
					break;
					case 'e':
						epStr.assign( &inBuf[2], inlen-2 );
						if( !AsioUtils::StringToEndpoint( epStr, outEp, 12345 ) ) {
							printf("FAILED to create endpoint from %s.\n", epStr.c_str() );
						}
					break;
					case 'a':
						autoReply = !autoReply;
						printf("autoreply is %s\n", autoReply ? "ON" : "OFF" );
					break;
					default:
						printf("Commands:\n\
/e<address> - start sending to <address>\n\
/a - turn auto reply on/off. The initial setting is on.\n\
/s - Stop UdpCon\n\
/q - Quit.\n");
					break;
				}
			} else {
				inBuf[inlen-1] = 0;
				if( capAmmo ) {
					gunAmmo.push_back( std::string(inBuf) );
				}
				printf("SENDING%s: %s\n", capAmmo ? "(captured)" : "", inBuf );
				uc.Send( outEp, inBuf, inlen );
			}
		}
	};

	uc.Stop();

	delete netThread;

	return 0;
}
