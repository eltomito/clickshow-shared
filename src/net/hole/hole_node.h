/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Hole_Node_h
#define _Hole_Node_h 1

#include "boost/asio.hpp"

class HoleNode
{
protected:
	HoleNode( boost::asio::io_service &_ioService,
				long _tickMs = 100 )
	:	ioService(_ioService),
		tickTimer(_ioService),
		tickMs(_tickMs) {};
	~HoleNode() {};

	void startTickTimer();
	void handleTimer(const boost::system::error_code& /*e*/);

	virtual void doChores() {};

protected:
	boost::asio::io_service		&ioService;
	boost::asio::deadline_timer	tickTimer;
	long				tickMs;
};

void HoleNode::startTickTimer()
{
	tickTimer.expires_from_now( boost::posix_time::milliseconds( tickMs ) );
	tickTimer.async_wait( boost::bind( &HoleNode::handleTimer, this, boost::asio::placeholders::error ) );
}

void HoleNode::handleTimer(const boost::system::error_code& /*e*/)
{
	doChores();
	startTickTimer();
}

#endif //_Hole_Node_h
