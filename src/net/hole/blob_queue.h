/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Blob_queue_h
#define _Blob_queue_h 1

#include "shared/src/str_utils.h"
#include <cstdlib>
#include <string>
#include <cassert>
#include "boost/cstdint.hpp"

class BQHead
{
public:
	BQHead( size_t _dataLen = 0 )
	: dataLen(_dataLen) {};
	~BQHead() {};

	char *GetDataStart() const { return (char*)this + sizeof(BQHead); };
	size_t GetDataLen() const { return dataLen; };
	size_t GetTotalLen() const { return dataLen + sizeof(BQHead); };
	BQHead *GetNext() const { return (BQHead*)( ((char*)this) + paddedLenFor( (char *)this, dataLen ) ); };
	size_t WriteTo( char *dst, size_t maxLen ) const;

	static BQHead *CreateAt( char *place, size_t len ) { return new (place)BQHead( len ); };
	static size_t GetSizeFor( size_t len ) { return sizeof(BQHead) + len; };

	static const size_t paddingTo = 2;	//FIXME: this should be 4 and there should be (Un)Serialize functions

	std::string ToString( const std::string &sep = "," ) const;

protected:
	static size_t paddedLenFor( char *ptr, size_t len );

protected:
	//size_t dataLen;
	boost::uint64_t dataLen;
};

//--- call BlobQueue ---

class BlobQueue
{
public:
	typedef unsigned int BQFlagsT;
	static const BQFlagsT BQF_OWN = 1;
	static const BQFlagsT BQF_CAN_WRAP = 2;

	class const_iterator
	{
	public:
		const_iterator( const BlobQueue *_bq, const char *_ptr, bool _isPastWrap ) { bq = _bq; ptr = _ptr; isPastWrap = _isPastWrap; };
		const_iterator( const const_iterator &other ) { bq = other.bq; ptr = other.ptr; isPastWrap = other.isPastWrap; };

		const_iterator &operator++()
		{
			ptr = (char*)((BQHead*)ptr)->GetNext();
			if( bq->isWrapped() ) {
				if( !isPastWrap && ( ptr != bq->inPtr) && ( ptr >= bq->wrapPtr )) {
					ptr = bq->queue;
					isPastWrap = true;
				}
			}
			return *this;
		};
		const BQHead &operator*() { return *((BQHead*)ptr); };
		bool operator==( const const_iterator &other ) { return (( ptr == other.ptr )&&( isPastWrap = other.isPastWrap )); };
		bool operator!=( const const_iterator &other ) { return (( ptr != other.ptr)||( isPastWrap != other.isPastWrap )); };
	protected:
		const char *ptr;
		bool	isPastWrap;
		const BlobQueue *bq;
	};

	BlobQueue( size_t maxSize = 100000 );
	BlobQueue( char *_queue, size_t _len, BQFlagsT _flags, bool keepData );
	BlobQueue( const BlobQueue &other ) { assert( false ); }; //don't use! it would corrupt the heap.

	~BlobQueue();

	size_t Size() const;
	bool Empty() const { return ( !isWrapped() && (inPtr <= outPtr)); };
	size_t Capacity() const { return queueSize; };
	void Clear();

	bool CanWrap() { return ( 0 != ( flags & BQF_CAN_WRAP ) ); };
	bool OwnsBuffer() { return ( 0 != ( flags & BQF_OWN ) ); };

	bool AppendOne( const char *buf, size_t len );
	bool AppendOne( const BQHead &bqh );
	int AppendSequence( BlobQueue &other );
	int AppendSequence( const char *start, size_t dataLen );

	size_t ExtractOne( char *dst, size_t maxLen );
	size_t ExtractSequence( char *dst, size_t maxLen );

	std::string ToString( const std::string &sep = ",", int verbosity = 1 ) const;

	const_iterator begin() const { return const_iterator( this, outPtr, false ); };
	const_iterator end() const { return const_iterator( this, inPtr, isWrapped() ); };

protected:
	bool isWrapped() const { return ( wrapPtr != NULL ); };
	size_t getSpaceAtInPtr() const { return isWrapped() ? (size_t)(outPtr - inPtr) : (size_t)(getMaxWrapPtr() - inPtr); };
	size_t getSpaceAtStart() const { return isWrapped() ? 0 : (outPtr - queue); };
	size_t getSpaceForPushBack() const
	{
		size_t atIn = getSpaceAtInPtr();
		size_t atStart = getSpaceAtStart();
		return ( atIn >= atStart ) ? atIn : atStart;
	};

	BQHead *pop_front();
	BQHead *push_back( size_t len );

	char		*getMaxWrapPtr() const { return queue + Capacity(); };
	//size_t	getMaxSizeAtInPtr();
	void allocQueue( size_t _len );

protected:
	char		*queue;
	size_t	queueSize;
	char		*inPtr, *outPtr, *wrapPtr;
	BQFlagsT	flags;
};

#endif //_Blob_queue_h
