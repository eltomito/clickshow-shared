/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Hole_Con_h
#define _Hole_Con_h 1

#include "udp_con.h"
#include "shared/src/semaph.h"
#include "hole_store.h"

class HoleCon : public UdpCon, public Lockable
{
public:
	HoleCon(	boost::asio::io_service &_ioService,
				unsigned short _localPort,
				const std::string &_path,
				bool _autoEndpoint = true,
				const boost::asio::ip::udp::endpoint &_outEp = boost::asio::ip::udp::endpoint()
			) :	UdpCon( _ioService, _localPort ),
				 	path(_path),
					outEp(_outEp),
					autoEp(_autoEndpoint)
		{};
	~HoleCon() {};

	void 			ReceiveHttp( const char *buf, size_t len );
	size_t		SendHttp( char *dst, size_t len );
	bool			HasHttpData() { return store.HasOutData(); };
	void			DoChores();

	const std::string &GetPath() { return path; };
	void			SetPath( const std::string &_path ) { path = _path; };

	std::string	ToString( const std::string &sep = ",");

protected:
	void sendUdp();
	void receive( boost::asio::ip::udp::endpoint &ep, char *buf, size_t len );

protected:
	HoleStore					store;
	std::string					path;
	boost::asio::ip::udp::endpoint	outEp;
	bool							autoEp;

	char							outData[ MAX_UDP_DATA_LEN ];
};

#endif // _Hole_Con_h
