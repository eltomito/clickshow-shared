/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "hole_con.h"
#include "shared/src/net/asio_utils.h"

#define DUMP_BUF( nnname, bbbuf, lllen )\
if( lllen < 128 ) {\
std::string tttmpstr;\
StrUtils::HexDump( tttmpstr, (void*)bbbuf, lllen );\
LOG_HOLE_DETAIL2("--- %s ( %ld bytes ) ---\n%s\n--- ---\n", nnname, lllen, tttmpstr.c_str() );\
}

void  HoleCon::ReceiveHttp( const char *buf, size_t len )
{
	LOCK_ME;

	DUMP_BUF("in http",buf,len);

	store.DigestHttp( buf, len );
	sendUdp();
}

size_t HoleCon::SendHttp( char *dst, size_t len )
{
	LOCK_ME;

	if( !store.HasOutData() ) {
		return 0;
	}
	size_t outLen = store.ExtractHttp( dst, len );

	DUMP_BUF("out http",dst,outLen);

	return outLen;
}

void HoleCon::receive( boost::asio::ip::udp::endpoint &ep, char *buf, size_t len )
{
	LOCK_ME;

	if( len == 0 ) { return; }

	if( autoEp ) {
		outEp = ep;
	} else {
		//TODO: check if the ep is the right sender
	}

	DUMP_BUF("in udp",buf, len);

	store.DigestUdp( buf, len );
}

void HoleCon::sendUdp()
{
	if( !store.HasInData() ) {
		return;
	}

	LOG_HOLE_DETAIL("Sending udp. InSize() = %ld\n", store.InSize() );

	size_t len = store.ExtractUdp( outData, sizeof( outData ) );
	while( len > 0 ) {

		DUMP_BUF("out udp", outData, len);

		Send( outEp, &outData[0], len );
		len = store.ExtractUdp( outData, sizeof( outData ) );
	}
}

void HoleCon::DoChores() {
	LOCK_ME;

	sendUdp();
}

std::string	HoleCon::ToString( const std::string &sep )
{
	std::string dst;
	std::string epStr = AsioUtils::EndpointToString( outEp );
	StrUtils::Printf( dst, "path: \"%s\", ep: %s%s", path.c_str(), epStr.c_str(), autoEp ? "(automatic}" : "(manually set)" );
	return dst;
}
