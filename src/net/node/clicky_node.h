/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Clicky_Node_h
#define _Clicky_Node_h

#include "shared/src/semaph.h"
#include "shared/src/net/packets/udp_packets.h"
#include "shared/src/str_utils.h"
#include "shared/src/default_log.h"
#include "node_user_pool.h"
#include "node_con_pool.h"
#include "out_packet_pool.h"
#include "shared/src/clicky_time_adjustment.h"

#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/system/error_code.hpp>
#include <boost/thread.hpp>

using boost::asio::ip::udp;

template <class ConT>
class ClickyNode : public Lockable
{
public:
	static const unsigned long DEFAULT_TICK_MS = 250;
	static const unsigned long MAX_IN_DATA_LENGTH = 4096;
	static const unsigned short DEFAULT_NODE_PORT = 0;	//Let the system choose a port number

	typedef boost::shared_ptr<ConT> ConPtrT;

	ClickyNode( boost::asio::io_service &_ioService, unsigned short port = DEFAULT_NODE_PORT, bool _autoAdjustTime = false );
	~ClickyNode();

	unsigned short GetLocalUdpPort();

	void Start();
	void Stop();

	ConPtrT	AddCon( const udp::endpoint &_endpoint );
	void	RemCon( ConPtrT cl );

	bool	Login( ConPtrT cl, const std::string &name, const std::string &pwd );
//	bool	Send( ConPtrT cl, UdpPacket &pkt, bool setSentTime = true, bool autoOrigTime = true );

//	bool	Login( ConT *cl, const std::string &name, const std::string &pwd );
	bool	Send( ConT *cl, UdpPacket &pkt, bool setSentTime = true, bool autoOrigTime = true );

	void	ResetTimeAdjustment() { timeAdjustment.Clear(); };

	void ForEachCon( NCPForEachF (&doWhat)(ConPtrT cl, void *_data), void *data ) { cons.ForEach( doWhat, data ); };

	std::string ToString( const std::string &sep = "" );

protected:

/*
	void takeBuf();
	void dropBuf();
	int bufCount;
	Semaph bufmaphore;
*/
	int decipherUserName( std::string &name );

	void	stop();
	void	remCon( ConPtrT cl );
	ConPtrT	addCon( const udp::endpoint &_endpoint );

	void startTickTimer();

	//handlers
	bool _handleReceiveFrom( const boost::system::error_code& errc, size_t bytesRecvd );
	void handleTimer(const boost::system::error_code& /*e*/);

	//* This is called after a client is successfully logged in.
	virtual void onLogin( ConPtrT cl ) {};
	virtual void onLogout( ConPtrT cl, NodeLogoutReasonT why ) {};
	virtual void onNewConnection( ConPtrT cl ) {};

	void receiveLoop();
	static NCPForEachF conChoreFunction( ConPtrT cl, void *_conChoreData );

protected:
	ClickyTimeAdjustment	timeAdjustment;		//apply this to the UTC time as reported by this machine to get the nework UTC time.
	bool						autoAdjustTime;

	NodeUserPool			users;
	NodeConPool<ConT>		cons;
	OutPacketPool			outPackets;

	bool						alive;
	bool						timerDead;
	unsigned int			tickMilliseconds;

	boost::thread			*netThread;
	boost::thread			*receiveThread;

	boost::asio::io_service		&ioService;
	boost::asio::deadline_timer	tickTimer;
	udp::socket				udpSocket;		//The socket this node listens for incoming udp packets on.
	udp::endpoint			udpEndpoint;	//The endpoint the most recent udp packet was received from.
	char						inDataRaw[ MAX_IN_DATA_LENGTH ];
	boost::asio::mutable_buffers_1		inDataBuffer;
	UdpPacketAny			inPacket;
};

#include "clicky_node_impl.h"

#endif //_Clicky_Node_h
