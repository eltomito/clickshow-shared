/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Out_Packet_h
#define _Out_Packet_h 1

#include "shared/src/net/packets/udp_packets.h"
#include "shared/src/str_utils.h"
#include "shared/src/net/asio_utils.h"
#include "shared/src/default_log.h"
#include "boost/asio.hpp"

using boost::asio::ip::udp;

class OutPacketPool;

class OutPacket
{
	friend class OutPacketPool;
public:
	OutPacket( OutPacketPool *_pool = NULL, const UdpPacket *pkt = NULL ) { Init( _pool, pkt ); };
	~OutPacket() {};
	void SetPacket( const UdpPacket *pkt );
	void SetPacket( const UdpPacket &pkt );


	void SetPool( OutPacketPool *_pool ) { pool = _pool; };
	void Init( OutPacketPool *_pool, const UdpPacket *pkt ) { SetPool(_pool); SetPacket( pkt ); };

	void Send( udp::socket &sock, const udp::endpoint &ep );

	std::string ToString( const std::string &separator = "\n", bool includingPacket = true,  bool includingHexdump = false );

protected:
	void handle_send_to( const boost::system::error_code &errc, size_t bytesSent );
	void freeMe();

protected:
	udp::endpoint	endpoint;
	OutPacketPool	*pool;
	size_t			rawLen;
	char				rawData[ MAX_UDP_PAYLOAD_SIZE ];
};

#endif //_Out_Packet_h
