/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Node_User_Pool_h
#define _Node_User_Pool_h 1

#include <list>
#include "node_user.h"
#include "shared/src/semaph.h"
#include "shared/src/list_searcher.h"

class NodeUserPool : public Lockable
{
protected:
	typedef std::list<NodeUser> UserListT;
	typedef UserListT::iterator uliterT;
	//typedef ListSearcher<UserListT, NodeUser::findByName, const std::string&, NodeUser> SearcherByName;

public:
	NodeUserPool() : nameIndex(users) { users.clear(); nameIndex.Clear(); };
	~NodeUserPool() {};
	NodeUser *AddUser( const NodeUser &newUser ) {
		NodeUser *oldUser = FindByName( newUser.GetName() );
		if( oldUser != NULL ) {
			LOG_NET_ERROR("User already exists: %s\n", oldUser->ToString(",").c_str() );
			return NULL;
		};
		users.push_front( newUser );
		uliterT it = users.begin();
		nameIndex.Insert( it->GetName(), it );
		return &(*it);
	};
	NodeUser *AddUser( const std::string &_name, const std::string &_pwd, UserPermsT _perms = 0 )
	{
		NodeUser nu( _name, _pwd, _perms );
		return AddUser( nu );
	};
	//NodeUser *FindByName( const std::string _name ) const { return SearcherByName::findPtr( _name, users ); }; 
	NodeUser *FindByName( const std::string &_name ) { return nameIndex.Find( _name ); }; 
protected:
	UserListT users;
	ListIndexer<UserListT, NodeUser, std::string> nameIndex;
};

#endif //_Node_User_Pool_h
