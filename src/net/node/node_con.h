/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Node_Con_h
#define _Node_Con_h 1

#include <string>
#include "shared/src/str_utils.h"
#include "shared/src/net/asio_utils.h"
#include "shared/src/net/packets/udp_packets.h"
#include "shared/src/net/node/convars/packet_queue.h"
#include "shared/src/net/net_types.h"

#include "boost/asio.hpp"

#include "node_user.h"

using boost::asio::ip::udp;

enum ConnT {
	DISCONNECTED = 0,
	CONNECTING,
	CONNECTED,
	DISCONNECTING
};

class NodeCon
{
public:
	static const boost::int64_t DEFAULT_RESEND_TIME = 1000; //milliseconds
	static const boost::int64_t DEFAULT_FILE_RESEND_TIME = 2000; //milliseconds
	static const boost::int64_t DEFAULT_TOUCH_TIME = 5000; //milliseconds
	static const boost::int64_t DEFAULT_RETOUCH_TIME = 2000; //milliseconds
	static const boost::int64_t DEFAULT_CONNECTION_TIMEOUT = 15000; //milliseconds

	NodeCon( const udp::endpoint &_endpoint, void *_node, NodeConIdT _id )
		: endpoint(_endpoint),
		node(_node),
		user(NULL),
		id(_id),
		alive(true),
		lastRecTime(true),
		lastTouchSentTime(true),
		lastTouchNumber(0),
		connState(DISCONNECTED),
		deathCause( WHY_LOGOUT_UNKNOWN ),
		protocolVersion(1)
	{};
	NodeCon()
	:	user(NULL),
		node(NULL),
		id(0),
		alive(true),
		lastRecTime(false),
		lastTouchSentTime(false),
		lastTouchNumber(0),
		connState(DISCONNECTED),
		deathCause( WHY_LOGOUT_UNKNOWN ),
		protocolVersion(1)
	{};
	~NodeCon() {};

	void SetUser( NodeUser &_user ) { user = &_user; };
	NodeUser *GetUser() const { return user; };

	void SetVenue( const std::string &_venue ) { venue = _venue; };
	const std::string &GetVenue() const { return venue; };

	bool IsLoggedIn() const { return user != NULL; };
	udp::endpoint &GetEndpoint() { return endpoint; };
	const udp::endpoint &GetConstEndpoint() const { return endpoint; };

	NodeConIdT GetId() const { return id; };
	void *GetNode() const { return node; };
	NodeLogoutReasonT GetDeathCause() const { return deathCause; };

	virtual bool Send( UdpPacket &pkt, bool setSentTime = true ) { LOG_UNIMP; return false; };

	bool IsAlive() const { return alive; };
	void Kill( NodeLogoutReasonT reason = WHY_LOGOUT_UNKNOWN )
	{
		SetConnected( DISCONNECTED );
		deathCause = reason;
		alive = false;
	};

	bool IsConnected() const { return connState == CONNECTED; };
	bool IsConnecting() const { return connState == CONNECTING; };
	bool IsDisconnected() const { return connState == DISCONNECTED; };
	bool IsDisconnecting() const { return connState == DISCONNECTING; };
	void SetConnected( ConnT state )
	{
		ConnT oldState = connState;
		connState = state;
		handleConnectionStateChange( oldState, state );
	};
	ConnT GetConnState() const { return connState; };

	void SetProtocolVersion( int v ) { protocolVersion = v; };
	int GetProtocolVersion() const { return protocolVersion; };

	virtual std::string ToString( const std::string &separator = "\n", int verbosity = 1 ) const
	{
		std::string dst;
		if( user != NULL ) {
			StrUtils::Printf(	dst, "Con: id=%ld%sname=\"%s\"%srights=0x%lx%saddr=%s%svenue=\"%s\"%salive=%s,ver=%i",
									(long)GetId(), separator.c_str(),
									user->GetName().c_str(), separator.c_str(),
									user->GetPerms(), separator.c_str(),
									AsioUtils::EndpointToString( endpoint ).c_str(), separator.c_str(),
									GetVenue().c_str(), separator.c_str(),
									alive ? "TRUE":"FALSE", protocolVersion );
		} else {
			StrUtils::Printf(	dst, "Con: id=%ld%s<no user>%saddr=%s",
									(long)GetId(), separator.c_str(),
									separator.c_str(),
									AsioUtils::EndpointToString( endpoint ).c_str() );
		}
		return dst;
	}

	void handlePacket( UdpPacket &pkt ) {
		LOG_NET_DETAIL("NodeCon: dispatchig packet by type. Packet: %s\n", pkt.ToString(",").c_str() );
		dispatchByType( pkt );
	};

	/** Do everything that needs to be done at regular intervals.
	 * Overload this method to get rid of standard chore handling.
	 * @param adjNowTime - the adjusted (server) time to be used as now.
	 */
	virtual void DoChores( const ClickyTime &adjNowTime ) {

		ClickyTime nowTime(true);	//the local now time

		//connection timeout
		if( (nowTime - lastRecTime) > DEFAULT_CONNECTION_TIMEOUT ) {
			Kill( WHY_LOGOUT_TIMEOUT );
			return;
		};

		//touch
		if( IsConnected() ) {
			if( NULL != GetUser() ) {
				if(( (nowTime - lastRecTime) > DEFAULT_TOUCH_TIME ) && ( (nowTime - lastTouchSentTime) > DEFAULT_RETOUCH_TIME) ) {
					LOG_NET_TRAFFIC("Touch - Last: %s Now : %s\nSending Touch to keep in touch.\n", lastRecTime.ToString().c_str(), nowTime.ToString().c_str() );
					SendTouch();
				};
			}
		}

		doChores( adjNowTime );
	};

	virtual void checkIntegrity( int n = 0 ) {};

protected:
	void SendTouch() {
		UdpPacketTouch pkt;
		lastTouchSentTime.SetToNow();
		pkt.number = lastTouchNumber;
		pkt.IncNum();
		lastTouchNumber = pkt.number;
		Send( pkt );
	};

	void SendTouchAck( UdpPacketTouch &pkt ) {
		pkt.SetAck();
		Send( pkt );
	};

#define HANDLE_PKT( _tp ) handle##_tp ( *((UdpPacket##_tp *)&pkt) );
//handleHi( *( (UdpPacketHi *)&pkt ) );

protected:
	void dispatchByType( UdpPacket &pkt )
	{
		if( pkt.IsValidType() ) {
			lastRecTime.SetToNow();
		};
		switch( pkt.type ) {
			case HI:
				HANDLE_PKT( Hi )
			break;
			case BYE:
				HANDLE_PKT( Bye )
			break;
			case SHOW:
				HANDLE_PKT( Show )
			break;
			case PLAY:
				HANDLE_PKT( Play )
			break;
			case CHAT:
				HANDLE_PKT( Chat )
			break;
			case PKT_MSG:
				HANDLE_PKT( Msg )
			break;
			case PERMS:
				HANDLE_PKT( Perms )
			break;
			case FILEHEAD:
				HANDLE_PKT( FileHeader )
			break;
			case FILEBODY:
				HANDLE_PKT( FileBody )
			break;
			case TOUCH:
				HANDLE_PKT( Touch )
			break;
			default:
				handleInvalid( pkt );
			break;
		};
	};

	//the packets are non-const, so the handlers are free to trash them.
	virtual void handleHi( UdpPacketHi &pkt ) {};
	virtual void handleBye( UdpPacketBye &pkt ) {};
	virtual void handleShow( UdpPacketShow &pkt ) {};
	virtual void handlePlay( UdpPacketPlay &pkt ) {};
	virtual void handleChat( UdpPacketChat &pkt ) {};
	virtual void handleMsg( UdpPacketMsg &pkt ) {};
	virtual void handlePerms( UdpPacketPerms &pkt ) {};
	virtual void handleFileHeader( UdpPacketFileHeader &pkt ) {};
	virtual void handleFileBody( UdpPacketFileBody &pkt ) {};
	virtual void handleTouch( UdpPacketTouch &pkt )
	{ 	//think twice before changing this one
		if(( !pkt.IsAck() )&&( IsConnected() )) {
			SendTouchAck( pkt );
		};
	};
	virtual void handleInvalid( UdpPacket &pkt ) {};
	virtual void handleConnectionStateChange( ConnT oldState, ConnT newState ) {};

	/**
	 * Overload this to use standard chore handling.
	 * @param nowTime - the adjusted (server) time that should be used as now.
	 */
	virtual void doChores( const ClickyTime &nowTime ) {};

protected:
	NodeConIdT	id;

	NodeUser		*user;
	std::string	venue;	//this is the id string from the HI packet

	udp::endpoint endpoint;
	void	*node;	//points to the ClickyNode this Con(nection) object belongs to.
						//WATCH OUT! It can be NULL for automatically created instances.
	bool					alive;
	ConnT 				connState;
	NodeLogoutReasonT	deathCause;

	ClickyTime		lastRecTime;			//unadjusted (local) time!
	ClickyTime		lastTouchSentTime;	//unadjusted (local) time!
	unsigned int	lastTouchNumber;

	int				protocolVersion;
};

#endif //_Node_Con_h
