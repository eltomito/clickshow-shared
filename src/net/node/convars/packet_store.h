/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Packet_Store_h
#define _Packet_Store_h 1

#include <list>
#include <map>
#include <utility>

/** Stores packets, makes them easy to find by their number and allows iteration.
 */
template <class pktT>
class PacketStore
{
protected:
	typedef std::list<pktT> listT;
	typedef typename listT::iterator literT;
	typedef std::map<unsigned int, literT> mapT;
	typedef typename mapT::iterator miterT;

protected:
	listT		pktList;
	mapT		pktMap;
	literT	nextInPkt;
	miterT	lastSeqPkt;

public:
	PacketStore()
	{
		nextInPkt = pktList.end();
		lastSeqPkt = pktMap.end();
	};
	~PacketStore() {};

	bool add( const pktT &pkt )
	{
		if( nextInPkt == pktList.end() ) {
			pktList.push_back( pkt );
			nextInPkt = pktList.end();
			--nextInPkt;
		} else {
			(*nextInPkt) = pkt;
		}
		std::pair<miterT,bool> res = pktMap.insert( std::pair<unsigned long, literT>(pkt.number, nextInPkt) );
		if( !res.second ) {
			return false;
		};
		nextInPkt = pktList.end();
		return true;
	};

	pktT *find( unsigned int num )
	{
		miterT mit = pktMap.find( num );
		if( mit != pktMap.end() ) {
			lastSeqPkt = mit;
			return &*(mit->second);
		}
		return NULL;
	};

	void Reset()
	{
		pktMap.clear();
		pktList.clear();
		nextInPkt = pktList.end();
		lastSeqPkt = pktMap.end();
	};


	/** Get the first packet in the store.
	 */
	pktT *getFirst()
	{
		if( pktMap.size() == 0 ) { return NULL; }
		lastSeqPkt = pktMap.begin(); 
		return &*lastSeqPkt->second;
	};

	/** Get the next packet in the store after packet #num.
	 * @return pointer to a stored packet or NULL if the end of the packet store has been reached.
	 */
	pktT *getNextByNumber( unsigned int num )
	{
		if(( lastSeqPkt == pktMap.end() )||( lastSeqPkt->first != num )) {
			lastSeqPkt = pktMap.find( num );
			if( lastSeqPkt == pktMap.end() ) {
				LOG_NET_WARN("Called getNextByNumber(%d) but there is no such packet number!\n", num );
				return NULL;
			}
		}
		++lastSeqPkt;
		return ( lastSeqPkt != pktMap.end() ) ? &*lastSeqPkt->second : NULL;
	}

	pktT *getPrevByNumber( unsigned int num )
	{
		if(( lastSeqPkt == pktMap.end() )||( lastSeqPkt->first != num )) {
			lastSeqPkt = pktMap.find( num );
			if( lastSeqPkt == pktMap.end() ) {
				LOG_NET_WARN("Called getNextByNumber(%d) but there is no such packet number!\n", num );
				return NULL;
			}
		}
		if( lastSeqPkt != pktMap.begin() ) {
			--lastSeqPkt;
			return &*lastSeqPkt->second;
		}
		lastSeqPkt == pktMap.end();
		return NULL;
	}
		
	size_t size() { return pktMap.size(); };
};

#endif //_Packet_Store_h
