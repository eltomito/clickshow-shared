/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Con_Var_File_Out_h
#define _Con_Var_File_Out_h 1

#include "con_var.h"
#include "shared/src/net/net_types.h"
#include "shared/src/net/packets/udp_packet_file_body.h"
#include "shared/src/net/packets/udp_packet_file_header.h"
#include "packet_queue_otmp.h"
#include "packet_queue_otop.h"
#include "shared/src/semaph.h"

class ConVarFileOut : public ConVar, public Lockable
{
protected:
	typedef OTMPQueue<UdpPacketFileBody> FileBodyQueueT;
	typedef OTOPQueue<UdpPacketFileHeader> FileHeaderQueueT;

public:
	ConVarFileOut(	NodeCon &_con,
						netFileRoleT _fileRole = FILE_ROLE_INVALID,
						long _resendMilli = 5000,
						netFileIdT _fileId = FILE_ID_INVALID,
						size_t _maxLen = NET_FILE_MAX_LEN )
		:	ConVar( _con ),
			queueBody( false ),
			fileId( _fileId ),
			fileRole( _fileRole ),
			fileLenAcked(0),
			fileLenTotal(0),
			maxLen(_maxLen),
			resendMilli(_resendMilli ) {};
	~ConVarFileOut() {};

	static const int MAX_RESEND_PACKETS = 20;

	void Reset( netFileIdT _fileId = FILE_ID_INVALID, size_t _fileLen = 0 );

	bool SendString( const std::string &str, netFileIdT id = FILE_ID_INVALID )
	{
		const char *data = str.c_str();
		size_t len = str.size() + 1;
		return SendData( data, len, id );
	};

	bool SendData( const char *data, size_t len, netFileIdT id, bool own = true );
	void Receive( UdpPacket &pkt );
	void DoChores( const ClickyTime &nowTime );

	void Send( UdpPacket &pkt, bool autoNum = true );
	void Resend( UdpPacket &pkt ) { Send( pkt, false ); };

	virtual ClickyTime GetLastResponseTime() { LOG_UNIMP; return ClickyTime(); };
	virtual unsigned int GetLastNumSent() { LOG_UNIMP; return UdpPacket::NO_NUMBER; };

	virtual void FileFinished() { LOG_UNIMP; };
	virtual void FileStarted() { LOG_UNIMP; };
	virtual void FileProgressed() { LOG_UNIMP; };
	virtual void FileAborted() { LOG_UNIMP; };

	netFileIdT GetFileId() { return fileId; };
	netFileIdT GetFileRole() { return fileRole; };

	size_t GetTotalSize() { return fileLenTotal; };
	size_t GetSentSize() { return fileLenTotal - unsentData.GetLen(); };
	size_t GetAckedSize() { return fileLenAcked; };
	bool IsComplete() { return fileLenTotal == fileLenAcked; };

	netFileIdT	GenerateFileId();

	std::string ToString( const std::string &sep = ",", int verbosity = 1 ) const;

protected:
	SPQActionT justReceived( UdpPacket &pkt );
	void justSent( const UdpPacket &pkt );

	void reset( netFileIdT _fileId = FILE_ID_INVALID, size_t _fileLen = 0 );
	void sendUnsent( const ClickyTime &nowTime, int maxPackets = 100 );
	bool sendAsPackets( NetChunk &data, const ClickyTime &startTime, long maxTime = -1, int maxPackets = -1 );
	bool sendOnePacket( NetChunk &data );
	void clearData();

protected:
	boost::int64_t				resendMilli;
	netFileIdT			fileId;
	netFileRoleT		fileRole;
	FileBodyQueueT		queueBody;
	FileHeaderQueueT 	queueControl;

	NetChunk				outData;
	NetChunk				unsentData;

	size_t		maxLen;
	size_t		fileLenAcked;
	size_t		fileLenTotal;
};

#endif //_Con_Var_File_Out_h
