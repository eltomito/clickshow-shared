/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Packet_Queue_OTMP_h
#define _Packet_Queue_OTMP_h

#include "packet_queue.h"
#include "packet_store.h"

/** One Type Multiple Packets queue
*/
template <class pktT>
class OTMPQueue : public PacketQueue
{
public:
	OTMPQueue( bool _incoming = false ) : incoming(_incoming)
	{
		Reset();
	};
	~OTMPQueue() {};

	virtual SPQActionT JustReceived( pktT &pkt )
	{
		if( IsIncoming() == pkt.IsAck() ) {
			LOG_NET_ERROR("This is an %s queue. It should never receive %s\n", IsIncoming() ? "incoming" : "outgoing", pkt.IsAck() ? "an ACK" : "a non-ACK" );
			return PQA_IGNORE;
		};
		pktT *oldPkt = packets.find( pkt.number );
		if( oldPkt != NULL ) {
			if( pkt.IsAck() ) {
				if( oldPkt->IsAck() ) {
					return PQA_IGNORE; //ignore duplicate acks
				}
				++ackedCount;
				oldPkt->SetAck();
				oldPkt->SetSentTime( pkt.GetSentTime() );
				//fix possible sloppy data in the ACK packet
				pkt = *oldPkt;
				return PROCESS; //only an outgoing queue can receive ACK packets.
			};
			LOG_NET_DETAIL("Received a duplicate packet:\nold: %s\nnew: %s. Asking caller to REACK.\n", oldPkt->ToString(",").c_str(), pkt.ToString(",").c_str() );
			return REACK;
		}
		//oldPkt == NULL
		if( pkt.IsAck() ) {
			LOG_NET_ERROR("WEIRD! Received an ACK for a packet I've never sent: %s\n", pkt.ToString(",").c_str() );
			return PQA_IGNORE;
		}
		packets.add( pkt );
		return PROCESS;
	};

	virtual void JustSent( const pktT &pkt )
	{
		if( IsIncoming() != pkt.IsAck() ) {
			LOG_NET_ERROR("This is an %s queue. It should never send %s", IsIncoming() ? "incoming" : "outgoing", pkt.IsAck() ? "an ACK" : "a non-ACK" );
			return;
		};
		pktT *oldPkt = packets.find( pkt.number );
		if( oldPkt != NULL ) {
			//ack or resend
			*oldPkt = pkt;
			return;
		}
		//oldPkt == NULL
		if( pkt.IsAck() ) {
			LOG_NET_ERROR("VERY BAD! I've just sent an ACK for a packet I've never received!: %s\n", pkt.ToString(",").c_str() );
			return;
		}
		packets.add( pkt );
		lastNumSent = pkt.number;
		++sentCount;
		return;
	};

	/** Returns the next packet to resend or NULL if there is none.
 	 */
	virtual UdpPacket *GetPacketToResend( const ClickyTime &nowTime, boost::int64_t resendMilli )
	{
		if( IsIncoming() ) { return NULL; }

		UdpPacket *pkt;
		if( lastResendNum == UdpPacket::NO_NUMBER ) {
			pkt = (UdpPacket *)packets.getFirst();
		} else {
			pkt = (UdpPacket *)packets.getNextByNumber( lastResendNum );
		}
		while(( pkt != NULL )&&( ( pkt->IsAck() )||( nowTime - pkt->GetSentTime() < resendMilli ) )) {
			pkt = (UdpPacket *)packets.getNextByNumber( pkt->number );
		}
		lastResendNum = (pkt != NULL) ? pkt->number : UdpPacket::NO_NUMBER;
		return pkt;
	};

	/** Finds the closest predecessor and successor to a given packet by number.
	 */
	void FindNeighbors( unsigned int num, pktT **pred, pktT **succ )
	{
		*pred = packets.getPrevByNumber( num );
		*succ = packets.getNextByNumber( num );
	};

	pktT *GetFirstPacket() { return packets.getFirst(); };
	pktT *GetNextByNumber( unsigned int num ) { return packets.getNextByNumber( num ); };

	virtual unsigned int GetLastNumSent() { return lastNumSent; };

	bool IsIncoming() { return incoming; };

	void Reset()
	{
		packets.Reset();
		lastResendNum = UdpPacket::NO_NUMBER;
		lastNumSent = UdpPacket::NO_NUMBER;
		sentCount = 0;
		ackedCount = 0;
	};

protected:
	PacketStore<pktT>	packets;
	bool incoming;
	unsigned int lastResendNum;
	unsigned int lastNumSent;
	
	size_t	sentCount;
	size_t	ackedCount;
};

#endif //_Packet_Queue_OTMP_h
