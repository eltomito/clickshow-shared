/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Packet_Queue_MTOP_h
#define _Packet_Queue_MTOP_h 1

#include "packet_queue.h"
#include <set>

/** Multiple Types, One Packet Queue
 *
 */
class MTOPQueue : public PacketQueue
{
public:
	MTOPQueue( PacketTypeT *_types = NULL )
	{
		SetTypes( _types );
		UdpPacketBye pkt;		//some packet with a valid header
		PacketReader::Copy( pkt, lastSent );
		PacketReader::Copy( pkt, lastReceived );
	};
	~MTOPQueue() {};

	void SetTypes( PacketTypeT *_types = NULL )
	{
		if( _types != NULL ) {
			while( *_types != INVALID ) {
				typesAllowed.insert( *_types );
				_types++;
			}
		} else {
			typesAllowed.clear();	
		}
	};

	SPQActionT JustReceived( const UdpPacket &pkt )
	{
		if( typesAllowed.find( pkt.type ) == typesAllowed.end() ) {
			LOG_NET_ERROR("Why are you sending me packets of a type I don't accept? %s\n", pkt.ToString(",").c_str() );
			return PQA_IGNORE;
		}
		if( !pkt.IsAck() ) {
			//--the incoming packet is a non-ack
			if( pkt.IsLaterIndexThan( getLastRec() ) ) {
				PacketReader::Copy( pkt, lastReceived );
				return PROCESS; //a fresh packet
			}
			return REACK; //an old packet
		}
		//--the incoming packet is an ack
		if( !getLastSent().IsAck() && !getLastSent().IsLaterIndexThan( pkt ) ) {
			getLastSent().SetAck();
		}
		return PQA_IGNORE; //duplicate ack or a bizarre ack from the future
	};

	void JustSent( const UdpPacket &pkt )
	{
		if( typesAllowed.find( pkt.type ) == typesAllowed.end() ) {
			LOG_NET_ERROR("Why are you sending me packets of a type I don't accept? %s\n", pkt.ToString(",").c_str() );
			return;
		}
		if( pkt.IsAck() ) { return; }
		if( !getLastSent().IsLaterIndexThan( pkt ) ) {
			PacketReader::Copy( pkt, lastSent );
		}
	};

	UdpPacket *GetPacketToResend( const ClickyTime &nowTime, boost::int64_t resendMilli )
	{
		if( getLastSent().HasNoNumber() ) { return NULL; }
		if( getLastSent().IsAck() ) { return NULL; }
		if( !getLastSent().GetSentTime().IsValid() ) { return NULL; }

		if( (nowTime - getLastSent().GetSentTime()) >= resendMilli ) {
			return &getLastSent();
		}
		return NULL;
	};

	unsigned int GetLastNumSent() { return getLastSent().number; };

protected:
	UdpPacket &getLastSent() const { return *((UdpPacket*)&lastSent); };
	UdpPacket &getLastRec() const { return *((UdpPacket*)&lastReceived); };

protected:
	std::set<PacketTypeT>	typesAllowed;

	UdpPacketAny	lastSent;
	UdpPacketAny	lastReceived;
};

#endif //_Packet_Queue_MTOP_h
