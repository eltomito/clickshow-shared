/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Con_Var_Load_h
#define _Con_Var_Load_h 1

#include "shared/src/net/node/convars/gen_con_var.h"
#include "shared/src/net/net_types.h"
#include "packet_queue_otop.h"

class ConVarLoad : public GenConVar<OTOPQueue<UdpPacketLoad> >
{
public:
	ConVarLoad(	NodeCon &_con, UserPermsT _perms = 0, long _resendMilli = 500 )
		: GenConVar<OTOPQueue<UdpPacketLoad> >( con, NORMAL, _perms, _resendMilli ) {};
	ConVarLoad(	NodeCon &con, const ConVarLoad &other )
		: GenConVar<OTOPQueue<UdpPacketLoad> >( con, other ) {};
	~ConVarLoad() {};

	void Send( netFileIdT _id );
	void receive(  UdpPacket &pkt, SPQActionT act, NodeUser *u );

	virtual void fileReceived( netFileIdT _id )
	{
		LOG_NET_INFO("fileReceived - id: %ld\n", _id );
		LOG_UNIMP;
	};
};

#endif //_Con_Var_Load_h
