/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Con_Var_Msg_h
#define _Con_Var_Msg_h 1

#include "gen_con_var.h"
#include "packet_queue_otop.h"

/** A Con(nection) variable which sends (error) net messages.
 *
 * Messages are sent and no care is taken whether they actually arrive or not.
 * They are only meant to inform the user on the other side
 * and nothing serious should happen if they don't arrive.
 */
class ConVarMsg : public GenConVar<OTOPQueue<UdpPacketMsg> >
{
public:
	ConVarMsg( NodeCon &_con ) : GenConVar<OTOPQueue<UdpPacketMsg> >( _con, NEEDS_USER, 0 ) {};
	ConVarMsg( NodeCon &_con, const ConVarMsg &other ) : GenConVar<OTOPQueue<UdpPacketMsg> >( _con, other ) {};
	~ConVarMsg() {};

	void Send( NetMsgT code );
	virtual void receive(  UdpPacket &pkt, SPQActionT act, NodeUser *u ) {};
};

class ClientVarMsg : public ConVarMsg
{
public:
	ClientVarMsg( NodeCon &_con ) : ConVarMsg( _con ) {};
	ClientVarMsg( NodeCon &_con, const ClientVarMsg &other ) : ConVarMsg( _con, other ) {};
	~ClientVarMsg() {};

	using ConVarMsg::Send;
	virtual void receive(  UdpPacket &pkt, SPQActionT act, NodeUser *u ) {};
};

#endif //_Con_Var_Msg_h
