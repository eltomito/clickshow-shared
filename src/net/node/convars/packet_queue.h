/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Packet_Queue_h
#define _Packet_Queue_h 1

#include "shared/src/default_log.h"
#include "shared/src/str_utils.h"
#include "shared/src/net/packets/packet_reader.h"

enum SPQActionT {
	PQA_IGNORE,
	PROCESS,
	REACK,
	STARTED,
	FINISHED,
	ABORTED
};

class PacketQueue
{
protected:
	PacketQueue() {};
	~PacketQueue() {};
public:
	virtual SPQActionT JustReceived( const UdpPacket &pkt ) { LOG_UNIMP; return PQA_IGNORE; };
	virtual void JustSent( const UdpPacket &pkt ) { LOG_UNIMP; };
	virtual UdpPacket *GetPacketToResend( const ClickyTime &nowTime, boost::int64_t resendMilli ) { LOG_UNIMP; return NULL; };
	virtual unsigned int GetLastNumSent() { LOG_UNIMP; return UdpPacket::NO_NUMBER; };
};

#endif //_Packet_Queue_h
