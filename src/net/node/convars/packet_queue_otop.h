/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Packet_Queue_OTOP_h
#define _Packet_Queue_OTOP_h 1

#include "packet_queue.h"

/** One Type, One Packet Queue
 *
 */
template<class pktT>
class OTOPQueue : public PacketQueue
{
public:
	OTOPQueue() {};
	~OTOPQueue() {};

	virtual SPQActionT JustReceived( const UdpPacket &pkt )
	{
		const pktT *pk = (pktT *)&pkt;

		LOG_NET_DETAIL2("pkt: %s\n%s\n", pk->ToString(",").c_str(), ToString("\n").c_str() );

		if( !pk->IsAck() ) {
			//--the incoming packet is a non-ack
			if( pk->IsLaterIndexThan( lastReceived ) ) {
				lastReceived = *pk;
				return PROCESS; //a fresh packet
			}
			return REACK; //an old packet
		}
		//--the incoming packet is an ack
		if( !lastSent.IsAck() && !lastSent.IsLaterIndexThan( *pk ) ) {
			lastSent.SetAck();
			lastSent.SetSentTime( pkt.GetSentTime() );
			return PROCESS;
		}
		return PQA_IGNORE; //duplicate ack or a bizarre ack from the future
	};

	virtual void JustSent( const UdpPacket &pkt )
	{
		const pktT *pk = (pktT *)&pkt;
		if( pkt.IsAck() ) { return; }
		if( !lastSent.IsLaterIndexThan( *pk ) ) {
			lastSent = *pk;
		}
		LOG_NET_DETAIL2("pkt: %s\n%s\n", pk->ToString(",").c_str(), ToString("\n").c_str() );
	};

	virtual UdpPacket *GetPacketToResend( const ClickyTime &nowTime, boost::int64_t resendMilli )
	{
		LOG_NET_DETAIL2("%s\n", ToString("\n").c_str() );

		if(( lastSent.IsAck() )||( lastSent.HasNoNumber() )) { return NULL; }
		if( (nowTime - lastSent.GetSentTime()) >= resendMilli ) {

			LOG_NET_DETAIL2("Let's RESEND: %s\n", lastSent.ToString(",").c_str() );

			return &lastSent;
		}
		return NULL;
	};

	virtual unsigned int GetLastNumSent() { return lastSent.number; };

	std::string ToString( const std::string &sep = ", " )
	{
		std::string dst;
		StrUtils::Printf( dst, "lastSent:%s%slastRec :%s", lastSent.ToString(",").c_str(), sep.c_str(), lastReceived.ToString(",").c_str() );
		return dst;
	};

protected:
	pktT	lastSent;
	pktT	lastReceived;
};

#endif //_Packet_Queue_OTOP_h
