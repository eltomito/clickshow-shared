/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Con_Var_File_In_h
#define _Con_Var_File_In_h 1

#include "con_var.h"
#include "shared/src/net/net_types.h"
#include "shared/src/net/packets/udp_packet_file_body.h"
#include "shared/src/net/packets/udp_packet_file_header.h"
#include "packet_queue_otmp.h"
#include "packet_queue_otop.h"
#include "shared/src/semaph.h"

class ConVarFileIn : public ConVar, public Lockable
{
protected:
	typedef OTMPQueue<UdpPacketFileBody> FileBodyQueueT;
	typedef OTOPQueue<UdpPacketFileHeader> FileHeaderQueueT;

public:
	ConVarFileIn(	NodeCon &_con,
						netFileRoleT _fileRole = FILE_ROLE_INVALID,
						long _resendMilli = 500,
						netFileIdT _fileId = FILE_ID_INVALID )
		:	ConVar( _con ),
			queueBody( true ),
			fileId( _fileId ),
			fileRole( _fileRole ),
			fileLenTotal(0),
			fileLenCurrent(0),
			resendMilli(_resendMilli ) {};
	~ConVarFileIn() {};

	void Reset( netFileIdT _fileId = FILE_ID_INVALID, size_t _fileLen = 0 ) {
		fileLenCurrent = 0;
		fileId = _fileId;
		fileLenTotal = _fileLen;
		queueBody.Reset();
	};

	void Receive( UdpPacket &pkt )
	{
		Lock();
		//TODO: take care of user rights
		SPQActionT act = JustReceived( pkt );
		Unlock();

		switch( act ) {
			case STARTED:
				FileStarted();
			break;
			case FINISHED:
				FileFinished();
			break;
			case PROCESS:
				FileProgressed();
			break;
			case ABORTED:
				FileAborted();
			break;
		};
	};

	/** Takes in a UdpPacketFileHead or a UdpPacketFileBody.
	 * returns: 
	 */
	SPQActionT JustReceived( UdpPacket &pkt )
	{
		UdpPacketFileHeader *head;
		UdpPacketFileBody *body;

		SPQActionT act = PQA_IGNORE;

		switch( pkt.type ) {

			case FILEHEAD:

				head = (UdpPacketFileHeader*)&pkt;
				if( head->role != fileRole ) {
					LOG_NET_ERROR("I want file role %ld but received file role %ld.\nPacket: %s\n", fileRole, head->role, pkt.ToString(",").c_str() );
					return PQA_IGNORE;
				}

				if(( head->IsAbort() )&&( head->id != fileId )) {
					LOG_NET_ERROR("CANNOT ABORT file with id = %ld. My fileId = %ld\n", head->id, fileId );
					return PQA_IGNORE;
				}

				act = queueControl.JustReceived( *head );
				if(( act == PROCESS )||( act == REACK )) {
					if( !pkt.IsAck() ) {
						pkt.SetAck();
						Send( pkt );
					}
					if( act == PROCESS ) {
						if( pkt.IsAbort() ) {
							Reset();
							act = ABORTED;
						} else {
							Reset( head->id, head->length );
							act = STARTED;
						}
					} else {
						act = PQA_IGNORE;
					}
					return act;
				}
			break;

			case FILEBODY:

				body = (UdpPacketFileBody*)&pkt;
				if( pkt.IsAck() ) {
					LOG_NET_WARN("BAD! This ConVarFileIn received an ACK body packet!\npkt: %s",
						pkt.ToString(",").c_str() );
					return PQA_IGNORE;
				}

				if( body->id != fileId ) {
					LOG_NET_ERROR("BAD! I want file id = %ld, but pkt: %s\n", fileId, pkt.ToString(",").c_str() );
					return PQA_IGNORE;
				}

				act = queueBody.JustReceived( *body );
				if(( act == REACK )||( act == PROCESS )) {
					if( !pkt.IsAck() ) {
						pkt.SetAck();
						Send( pkt );
					}
					if( act == PROCESS ) {
						if( Overlap( *body ) ) {
							LOG_NET_ERROR("PROBLEM! This packet overlaps previous packets! FILE CORRUPTION is likely!\npkt #: %d\n", pkt.number );
						}
						fileLenCurrent += body->data.GetLen();
						act = ( fileLenCurrent == fileLenTotal ) ? FINISHED : PROCESS;
					} else {
						act = PQA_IGNORE;
					}
				}
			break;

			default:
				LOG_NET_ERROR("UNIMPLEMENTED packet type received: %s\n", pkt.ToString(",").c_str() );
				act = PQA_IGNORE;
			break;
		}
		return act;
	};

	void JustSent( const UdpPacket &pkt )
	{
		switch( pkt.type ) {
			case FILEHEAD:
				queueControl.JustSent( *((UdpPacketFileHeader*)&pkt) );
			break;
			case FILEBODY:
				if( !pkt.IsAck() ) {
					LOG_NET_WARN("BAD! This ConVarFileIn just sent a non-ACK body packet!\npkt: %s", pkt.ToString(",").c_str() );
					return;
				}
				queueBody.JustSent( *((UdpPacketFileBody*)&pkt) );
			break;

			default:	
				LOG_NET_ERROR("UNIMPLEMENTED packet type received: %s\n", pkt.ToString(",").c_str() );
			break;
		}
	};

	void DoChores( const ClickyTime &nowTime )
	{
		Lock();

		UdpPacket *pkt = queueControl.GetPacketToResend( nowTime, resendMilli );
		if( pkt != NULL ) {
			Resend( *pkt );
		};

		Unlock();
	};

	void Send( UdpPacket &pkt, bool autoNum = true );
	void Resend( UdpPacket &pkt ) { Send( pkt, false ); };

	bool Overlap( UdpPacketFileBody &pkt )
	{
		UdpPacketFileBody *pred = NULL;
		UdpPacketFileBody *succ = NULL;
		queueBody.FindNeighbors( pkt.number, &pred, &succ );
		if( pkt.Overlap( pred ) ) {
			LOG_NET_ERROR("Overlapping packets (predecessor, new):\n%s\n%s\n", pred->ToString(",").c_str(), pkt.ToString(",").c_str() );
			return true;
		};
		if( pkt.Overlap( succ ) ) {
			LOG_NET_ERROR("Overlapping packets (new, successor):\n%s\n%s\n", pkt.ToString(",").c_str(), succ->ToString(",").c_str() );
			return true;
		};
		return false;
	};

	bool FileToString( std::string &dst ) {
		if( !IsComplete() ) { return false; };

		Lock();

		UdpPacketFileBody *pkt = queueBody.GetFirstPacket();
		while( pkt != NULL ) {
			pkt->Paste( dst );
			pkt = queueBody.GetNextByNumber( pkt->number );
		};
		
		Unlock();

		return true;
	};

	virtual void FileStarted() { LOG_UNIMP; };
	virtual void FileProgressed() { LOG_UNIMP; };
	virtual void FileFinished() { LOG_UNIMP; };
	virtual void FileAborted() { LOG_UNIMP; };

	virtual ClickyTime GetLastResponseTime() { LOG_UNIMP; return ClickyTime(); };
	virtual unsigned int GetLastNumSent() { LOG_UNIMP; return UdpPacket::NO_NUMBER; };

	netFileIdT GetFileId() { return fileId; };
	netFileRoleT GetFileRole() { return fileRole; };

	size_t GetTotalSize() { return fileLenTotal; };
	size_t GetCurrentSize() { return fileLenCurrent; };
	bool IsComplete() { return fileLenTotal == fileLenCurrent; };

	std::string ToString( const std::string &sep = ",", int verbosity = 1 ) const;

protected:
	boost::int64_t		resendMilli;
	netFileIdT			fileId;
	netFileRoleT		fileRole;
	FileBodyQueueT		queueBody;
	FileHeaderQueueT 	queueControl;

	size_t		fileLenTotal;
	size_t		fileLenCurrent;
};

#endif //_Con_Var_File_In_h
