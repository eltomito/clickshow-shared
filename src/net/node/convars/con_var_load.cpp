/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "con_var_load.h"
#include "shared/src/net/node/node_con.h"
//#include "clicky_node.h"

void ConVarLoad::Send( netFileIdT _id )
{
	UdpPacketLoad pkt( _id, GetLastNumSent() );
	pkt.IncNum();
	con.Send( pkt );
	JustSent( pkt );
}

void ConVarLoad::receive( UdpPacket &pkt, SPQActionT act, NodeUser *u )
{
	if( act == PROCESS ) {
		fileReceived( ((UdpPacketLoad*)&pkt)->fileId );
	}
}
