/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "con_var_file_out.h"
#include "shared/src/net/node/node_con.h"

void ConVarFileOut::Reset( netFileIdT _fileId, size_t _fileLen )
{
	LOCK_ME;

	reset( _fileId, _fileLen );
}

void ConVarFileOut::reset( netFileIdT _fileId, size_t _fileLen )
{
	fileLenAcked = 0;
	fileLenTotal = 0;
	fileId = _fileId;
	queueBody.Reset();
	outData.Clear();
	unsentData.Clear();
};

bool ConVarFileOut::SendData( const char *data, size_t len, netFileIdT id, bool own )
{
	if( id == FILE_ID_INVALID ) { id = GenerateFileId(); };

	if( len > maxLen ) {
		LOG_NET_ERROR("FAILED to send %ld bytes, because maxLen = %ld\n", len, maxLen );
		return false;
	}

	LOCK_ME;

	reset();
	fileId = id;

	outData.Copy( data, len, 0, own );
	unsentData = outData;
	fileLenTotal = len;

	UdpPacketFileHeader head( fileId, fileRole, len );
	Send( head );

	return true;
}

/**
 * FIXME: This needs to be more robust to avoid collisions.
 */
netFileIdT ConVarFileOut::GenerateFileId()
{
	ClickyTime nowTime(true);
	//0 is the invalid id, so let's make all generated ids odd to avoid 0.
	netFileIdT id = 1 + ( ( (netFileIdT)rand() ^ (netFileIdT)nowTime.GetMilli() ) & 0xfffffffe );
	LOG_NET_INFO("GENERATED file id: %ld\n", id);
	return id;
}

void ConVarFileOut::Receive( UdpPacket &pkt )
{
	Lock();

	//TODO: take care of user rights
	SPQActionT act = justReceived( pkt );

	Unlock();

	switch( act ) {
		case STARTED:
			FileStarted();
		break;
		case FINISHED:
			FileFinished();
		break;
		case PROCESS:
			FileProgressed();
		break;
		case ABORTED:
			FileAborted();
		break;
	};
}

/** Takes in a UdpPacketFileHead or a UdpPacketFileBody.
 */
SPQActionT ConVarFileOut::justReceived( UdpPacket &pkt )
{
	UdpPacketFileHeader *head;
	UdpPacketFileBody *body;
	SPQActionT act = PQA_IGNORE;
	bool isAck = pkt.IsAck();

	switch( pkt.type ) {

		case FILEHEAD:

			head = (UdpPacketFileHeader*)&pkt;

			if( head->id != fileId ) {
				LOG_NET_ERROR("Expected fileId=%ld, received fileId=%ld - ignoring.\n", fileId, head->id );
				act = PQA_IGNORE;
				break;
			}
			act = queueControl.JustReceived( *head );
			if( ( act == REACK ) || ( (act == PROCESS) && !isAck ) ) {
					pkt.SetAck();
					Send( pkt );
			}
			if( act == PROCESS ) {
				if( isAck ) {
					if( !head->IsAbort() ) {
						sendUnsent( ClickyTime(true) );
					}
				} else if( head->IsAbort() ) {
					reset();
					act = ABORTED;
				} else {
					LOG_NET_ERROR("Strange FileHeader received by OUT file: %s\n", head->ToString(",").c_str() );
					act = PQA_IGNORE;
				}
			}
		break;

		case FILEBODY:

			body = (UdpPacketFileBody*)&pkt;
			if( body->id != fileId ) {
				LOG_NET_ERROR("WRONG file id. Wanted: %ld, received: %s\n", fileId, pkt.ToString(",").c_str() );
				act = PQA_IGNORE;
				break;
			}

			if( !pkt.IsAck() ) {
				LOG_NET_ERROR("Ignoring a received non-ACK body packet: %s\n", pkt.ToString(",").c_str() );
				act = PQA_IGNORE;
				break;
			}

			act = queueBody.JustReceived( *body );
			if( act == PROCESS ) {
				fileLenAcked += body->data.GetLen();
				if( fileLenAcked == fileLenTotal ) {
					act = FINISHED;
					clearData();
				} else {
					sendUnsent( ClickyTime(true), 1 );
				}
			} else {
				act = PQA_IGNORE;
			}
		break;

		default:
			LOG_NET_ERROR("UNIMPLEMENTED packet type received: %s\n", pkt.ToString(",").c_str() );
			act = PQA_IGNORE;
		break;
	}

	return act;
}

void ConVarFileOut::justSent( const UdpPacket &pkt )
{
	switch( pkt.type ) {
		case FILEHEAD:
			queueControl.JustSent( *((UdpPacketFileHeader*)&pkt) );
		break;
		case FILEBODY:
			if( pkt.IsAck() ) {
				LOG_NET_WARN("BAD! This ConVarFileOut just sent an ACK body packet!\npkt: %s\n", pkt.ToString(",").c_str() );
				return;
			}
			queueBody.JustSent( *((UdpPacketFileBody*)&pkt) );
		break;

		default:
			LOG_NET_ERROR("UNIMPLEMENTED packet type received: %s\n", pkt.ToString(",").c_str() );
		break;
	}
}

void ConVarFileOut::DoChores( const ClickyTime &nowTime )
{
	LOCK_ME;

	UdpPacket *pkt = queueControl.GetPacketToResend( nowTime, resendMilli );
	if( pkt != NULL ) {
		Resend( *pkt );
	};

	pkt = queueBody.GetPacketToResend( nowTime, resendMilli );
	for( int i = MAX_RESEND_PACKETS; --i; i > 0 ) {
		if( !pkt ) { break; }
//	while( pkt != NULL ) {
		Resend( *pkt );
		pkt = queueBody.GetPacketToResend( nowTime, resendMilli );
	}

	sendUnsent( nowTime );
}

void ConVarFileOut::Send( UdpPacket &pkt, bool autoNum )
{
	if(( autoNum )&&( !pkt.IsAck() )) {
		pkt.number = (pkt.type == FILEBODY) ? queueBody.GetLastNumSent() : queueControl.GetLastNumSent();
		pkt.IncNum();
	}
	con.Send( pkt );
	justSent( pkt );
}

std::string ConVarFileOut::ToString( const std::string &sep, int verbosity ) const
{
	std::string dst;
	
	StrUtils::Printf( dst, "FileOut:%sid=%ld%srole=%ld%stotal=%ld%sacked=%ld%s",
							sep.c_str(),
							(long)fileId, sep.c_str(),
							(long)fileRole, sep.c_str(),
							(long)fileLenTotal, sep.c_str(),
							(long)fileLenAcked, sep.c_str() );

	return dst;
}

void ConVarFileOut::sendUnsent( const ClickyTime &nowTime, int maxPackets )
{
	sendAsPackets( unsentData, nowTime, (long)resendMilli / 8, maxPackets );
}

/** Send a data chunk in packets.
 * @param data - the data chunk to send
 * @param startTime - the time to measure time spend sending data from.
 * @param maxTime - the maximum time that can be spent sending data (in milliseconds). -1 means no limit.
 *                  Passing 0 as maxTime won't prevent the first packet from being sent.
 * @param maxPackets - the maximum number of packets that can be sent. -1 means no limit.
 * @returns true if everything went well. Right now, false would actually mean an unexpected data race has occured.
 */
bool ConVarFileOut::sendAsPackets( NetChunk &data, const ClickyTime &startTime, long maxTime, int maxPackets )
{
	ClickyTime	nowTime(false);
	long 			spentMs = 0;
	bool			ignoreTimeLimit = ( maxTime < 0 );

	maxPackets = (maxPackets < 0) ? INT_MAX : maxPackets;

	while(( data.GetLen() > 0 )&&( maxPackets > 0 )&&( ignoreTimeLimit || ( spentMs < maxTime ))) {
		if( !sendOnePacket( data ) ) { return false; }
		maxPackets--;
		if( !ignoreTimeLimit ) {
			nowTime.SetToNow();
			spentMs = (long)(nowTime - startTime);
		}
	};
	return true;
}

bool ConVarFileOut::sendOnePacket( NetChunk &data )
{
	size_t len = data.GetLen();
	if( len == 0 ) { return false; }

	size_t l = len > UdpPacketFileBody::MaxPayloadSize() ? UdpPacketFileBody::MaxPayloadSize() : len;

	NetChunk ch( data, 0, l );

	UdpPacketFileBody pkt( fileId, &ch );
	Send( pkt );
	data.TrimStart( l );

	return true;
}

void ConVarFileOut::clearData()
{
	if( unsentData.GetLen() > 0 ) {
		LOG_NET_ERROR("PROBLEM! cannot clear data with %ld unsent bytes!\n", unsentData.GetLen() );
		return;
	}
	LOG_NET_DETAIL("Clearing all data:\noutData   : %s\nunsentData: %s\n", outData.ToString(",").c_str(), unsentData.ToString(",").c_str() );
	outData.Clear();
	unsentData.Clear();
}
