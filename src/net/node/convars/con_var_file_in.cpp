/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "con_var_file_in.h"
#include "shared/src/net/node/node_con.h"

void ConVarFileIn::Send( UdpPacket &pkt, bool autoNum )
{
	if(( autoNum )&&( !pkt.IsAck() )) {
		pkt.number = pkt.type == FILEBODY ? queueBody.GetLastNumSent() : queueControl.GetLastNumSent();
		pkt.IncNum();
	}
	con.Send( pkt );
	JustSent( pkt );
}

std::string ConVarFileIn::ToString( const std::string &sep, int verbosity ) const
{
	std::string dst;
	
	StrUtils::Printf( dst, "FileIn:%sid=%ld%srole=%ld%stotal=%ld%scurrent=%ld%s",
							sep.c_str(),
							(long)fileId, sep.c_str(),
							(long)fileRole, sep.c_str(),
							(long)fileLenTotal, sep.c_str(),
							(long)fileLenCurrent, sep.c_str() );

	return dst;
}