/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Gen_Con_Var_h
#define _Gen_Con_Var_h 1

#include "../node_con.h"
#include "con_var.h"
#include "shared/src/net/packets/udp_packets.h"
#include "shared/src/default_log.h"
#include "shared/src/net/node/user_perms.h"

template<class queueT>
class GenConVar : public ConVar
{
public:
	using ConVar::FlagsT;

	static const int MAX_RESEND_PACKETS = 50;

	GenConVar( NodeCon &_con, FlagsT _flags, UserPermsT _permsNeeded = 0, long _resendMilli = 500 )
	:	ConVar( _con ),
		resendMilli( _resendMilli ),
		flags( _flags ),
		permsNeeded(_permsNeeded) {};

	GenConVar( NodeCon &_con, const GenConVar<queueT> &other )
		: ConVar( _con ),
		resendMilli( other.resendMilli ),
		flags( other.flags ),
		permsNeeded( other.permsNeeded ),
		queue( other.queue ) {};

	~GenConVar() {};

	bool AutoAck() { return 0 != (flags & AUTO_ACK); };
	bool ProcessAck() { return 0 != (flags & PROCESS_ACK); };
	bool AutoResend() { return 0 != (flags & AUTO_RESEND); };
	bool NeedsUser() { return 0 != (flags & NEEDS_USER); };

	void Receive( UdpPacket &pkt )
	{
		NodeUser *u = GetUser();
		if(( u == NULL )&&( NeedsUser() )) {
			LOG_NET_ERROR("This packet cannot come from a connection (%s) without a user! %s\n", con.ToString(",").c_str(), pkt.ToString(",").c_str() );
			return;
		}

		SPQActionT act = JustReceived( pkt );

		if( ( act == PROCESS ) && !pkt.IsAck() && ( u != NULL ) && ( permsNeeded != (u->GetPerms()&permsNeeded) ) ) {
			LOG_NET_ERROR("User %s on con %s tried to do something he isn't allowed to! %s\n", u->ToString(", ").c_str(), con.ToString(",").c_str(), pkt.ToString(",").c_str() );
			errorPerms( u->GetPerms() );
			return;
		}
		receive( pkt, act, u );
	};

	virtual SPQActionT JustReceived( const UdpPacket &pkt )
	{
		SPQActionT act = queue.JustReceived( pkt );

		//should we ack the packet?
		if( !pkt.IsAck() && AutoAck() && (( act == PROCESS )||( act == REACK )) ) {
			LOG_NET_DETAIL("Auto reacking packet: %s\n", pkt.ToString(",").c_str() );
			UdpPacketAny ackp;
			PacketReader::Copy( pkt, ackp );
			UdpPacket *p = (UdpPacket *)&ackp;
			p->SetAck();
			Send( *p );
			queue.JustSent( *p );
			if(act == REACK) {
				act = PQA_IGNORE;
			}
		} else if( pkt.IsAck() ) {
			act = ProcessAck() ? act : PQA_IGNORE;
		}
		return act;
	};
	virtual void JustSent( const UdpPacket &pkt ) { queue.JustSent( pkt ); };

	virtual void DoChores( const ClickyTime &nowTime )
	{
		if( !AutoResend() ) { return; };
		UdpPacket *pkt = NULL;
		for( int i = MAX_RESEND_PACKETS; --i; i > 0 ) {
//		while( true ) {
			pkt = queue.GetPacketToResend( nowTime, resendMilli );
			if( pkt == NULL ) { break; }
			LOG_NET_DETAIL( "Resending packet %s\n", pkt->ToString(",").c_str() );
			Send( *pkt );
			queue.JustSent( *pkt );
		}
	};
	virtual unsigned int GetLastNumSent() { return queue.GetLastNumSent(); };

	queueT &GetQueue() { return queue; };

protected:
	virtual void receive( UdpPacket &pkt, SPQActionT act, NodeUser *u ) {};
	virtual void errorPerms( UserPermsT _granted )
	{
		LOG_NET_ERROR("Unsufficient permissions: granted = %ld vs needed = %ld\n", _granted, permsNeeded );
	};

protected:
	boost::int64_t		resendMilli;
	queueT		queue;
	FlagsT		flags;
	UserPermsT	permsNeeded;
};

#endif //_Gen_Con_Var_h
