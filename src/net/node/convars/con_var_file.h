/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Con_Var_File_h
#define _Con_Var_File_h 1

#include "con_var_file_in.h"
#include "con_var_file_out.h"

/** Dispatches packets to a pair of incoming and outgoing file connection variables.
 */
class ConVarFile : public ConVar
{
protected:
	ConVarFileIn *varIn;
	ConVarFileOut *varOut;
public:
	ConVarFile(	NodeCon &_con, ConVarFileIn *_invar = NULL, ConVarFileOut *_outvar = NULL )
		:	ConVar(_con),
			varIn( _invar ),
			varOut( _outvar ) {};
	~ConVarFile() {};

	void ReceiveHeader( UdpPacketFileHeader &pkt )
	{
		if(( varOut != NULL )&&( varOut->GetFileId() == pkt.id )) {
			varOut->Receive( pkt );
		} else if( varIn != NULL ) {
			varIn->Receive( pkt );
		} else {
			LOG_NET_ERROR("NO IDEA what to do with this file header:\n%s\n", pkt.ToString(",").c_str() );
		}
	};

	void SetVars( ConVarFileIn *in = NULL, ConVarFileOut *out = NULL )
	{
		varIn = in;
		varOut = out;
	};

	void ReceiveBody( UdpPacketFileBody &pkt )
	{
		if(( varOut != NULL )&&( varOut->GetFileId() == pkt.id )) {
			varOut->Receive( pkt );
		} else if(( varIn != NULL )&&( varIn->GetFileId() == pkt.id )) {
			varIn->Receive( pkt );
		} else {
			LOG_NET_ERROR("NO IDEA what to do with this file body packet:\n%s\n", pkt.ToString(",").c_str() );
		}
	};

	virtual void DoChores( const ClickyTime &nowTime )
	{
		if( varIn != NULL ) { varIn->DoChores( nowTime ); }
		if( varOut !=NULL ) { varOut->DoChores( nowTime ); }
	};
};

#endif //_Con_Var_File_h
