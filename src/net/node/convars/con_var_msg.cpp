/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "con_var_msg.h"

/*
 * This code should go somewhere else.
 * It has no business being in the shared con_vars and referencing Client specifics.
#include "client/clicky_client.h"
#define CLIENT_PTR ((ClickyClient *)(con.GetNode()))

*/

void ConVarMsg::Send( NetMsgT code )
{
	UdpPacketMsg pkt( code, GetLastNumSent() );
	pkt.IncNum();
	con.Send( pkt );
	JustSent( pkt );
}

/*
 * Same here: move it in the client con var.
void ClientVarMsg::receive(  UdpPacket &pkt, SPQActionT act, NodeUser *u )
{
	if( pkt.IsAck() ) { return; }
	if( act == PROCESS ) {
		CLIENT_PTR->GetEmitter()->NetMsg( ((UdpPacketMsg *)&pkt)->msgCode );
	}
}
*/