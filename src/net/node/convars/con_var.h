/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Con_Var_h
#define _Con_Var_h 1

#include "packet_queue.h"
#include "shared/src/net/node/node_user.h"

class NodeCon;

/** Connection Variable
 *
 */
class ConVar
{
public:
	typedef unsigned int FlagsT;
	static const FlagsT	AUTO_ACK			= 1;			//automatically send ACK packets
	static const FlagsT	AUTO_RESEND		= 1 << 1;	//automatically resend unacked packets
	static const FlagsT	NEEDS_USER		= 1 << 2;	//this var needs a Con(nnection) with a user
	static const FlagsT	PROCESS_ACK	= 1 << 3;	//return PROCESS after a packet is first ACKed

	static const FlagsT	NORMAL = AUTO_ACK|AUTO_RESEND|NEEDS_USER;

protected:
	ConVar( NodeCon &_con ) : con(_con ) {
/*
		if( &_con == NULL ) { printf("%s got a NULL Con reference!\n", __PRETTY_FUNCTION__); }
		else { printf("%s got a GOOD Con reference!\n", __PRETTY_FUNCTION__); }
*/
	};
	~ConVar() {};
public:
	virtual SPQActionT JustReceived( UdpPacket &pkt ) { LOG_UNIMP; return PQA_IGNORE; };
	virtual void JustSent( const UdpPacket &pkt ) { LOG_UNIMP; };
	virtual void DoChores( const ClickyTime &nowTime ) { LOG_UNIMP; };
	virtual ClickyTime GetLastResponseTime() { LOG_UNIMP; return ClickyTime(); };
	virtual unsigned int GetLastNumSent() { LOG_UNIMP; return UdpPacket::NO_NUMBER; };
	virtual std::string ToString( const std::string &sep = ",", int verbosity = 1 ) const { return ""; };
	NodeCon &GetCon() { return con; };
protected:
	bool Send( UdpPacket &pkt, bool setSentTime = true );
	NodeUser *GetUser();
	NodeCon &con;
};

#endif //_Con_Var_h
