/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Out_Packet_Pool_h
#define _Out_Packet_Pool_h

#include "out_packet.h"
#include "shared/src/list_searcher.h"
#include "shared/src/default_log.h"
#include "shared/src/semaph.h"

#include <list>

//class ClickyNode;

class OutPacketPool : public Lockable
{
public:
	OutPacketPool() : freeMe(NULL), ptrIndex(packets) { packets.clear(); };
	~OutPacketPool() { deleteByPtr( freeMe ); freeMe = NULL; };

	OutPacket	*AllocPacket( const UdpPacket *pkt = NULL );
	OutPacket	*AllocPacket( const UdpPacket &pkt ) {	return AllocPacket( &pkt ); };
	void			FreePacket( OutPacket *pkt );
	size_t		size() { return packets.size(); };

	std::string ToString( const std::string &sep = "\n" )
	{
		LOCK_ME
		return _ToString( sep );
	};

protected:
	void deleteByPtr( OutPacket *pkt );
	std::string _ToString( const std::string &sep = "\n" );

protected:
	typedef std::list<OutPacket> PacketListT;
	typedef PacketListT::iterator pliterT;

	ListIndexer<PacketListT, OutPacket, OutPacket*> ptrIndex;

	PacketListT	packets;
	OutPacket	*freeMe;
};

#endif //_Out_Packet_Pool_h
