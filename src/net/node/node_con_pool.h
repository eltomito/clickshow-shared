/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Node_Con_Pool_h
#define _Node_Con_Pool_h 1

#include <list>

#include "node_con.h"
#include "shared/src/semaph.h"
#include "shared/src/list_searcher.h"
#include "shared/src/default_log.h"

#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/smart_ptr/make_shared.hpp>

enum NCPForEachF {
	NCP_CONTINUE = 0,
	NCP_STOP = 1,
	NCP_DELETE = 1 << 1
};

template <class ConT>
class NodeConPool : public Lockable
{
public:
	typedef boost::shared_ptr<ConT> ConPtrT;
	typedef std::list<ConPtrT> ConListT;
	typedef typename ConListT::iterator ConIterT;

private:
	ConPtrT derefConPtr( ConPtrT *p ) {
		return (p ? (*p) : ConPtrT() );
	}

public:
	NodeConPool( void *_node = NULL )
	:	epIndex(cons),
		idIndex(cons),
		node(_node),
		lastConId(0) { Clear(); };
	~NodeConPool() {};


	ConPtrT	AddCon( const udp::endpoint &_endpoint )
	{
		LOCK_ME;

		ConPtrT oldCon = derefConPtr( epIndex.Find( _endpoint ) ); //fixme! Find has to return a connection
		if( oldCon ) {
			LOG_NET_ERROR("A Con(nection) with this endpoint already exists: %s\n", oldCon->ToString(",").c_str() );
			return ConPtrT();
		}
//		ConPtrT nc = boost::make_shared<ConT>( _endpoint, node, generateConId() );
		ConT *p = new ConT( _endpoint, node, generateConId() );
		p->checkIntegrity( 0 );
		ConPtrT nc = boost::shared_ptr<ConT>( p );
		nc->checkIntegrity( 1 );
		if( !nc ) {
			LOG_NET_ERROR("Failed to create Con(nnection)!\n");
			return ConPtrT();
		}
		cons.push_front( nc );
		ConIterT it = cons.begin();
		epIndex.Insert( _endpoint, it );
		idIndex.Insert( nc->GetId(), it );
		return *it;
	};

	void Clear()
	{
		LOCK_ME;

		epIndex.Clear();
		idIndex.Clear();
		cons.clear();
	};

	ConPtrT 		FindById( NodeConIdT id ) { LOCK_ME; return derefConPtr( idIndex.Find( id ) ); };
	ConPtrT 		FindByEndpoint( const udp::endpoint &ep ) { LOCK_ME; return derefConPtr( epIndex.Find( ep ) ); };

	ConPtrT	GetFirst() { return ( cons.size() == 0 ) ? ConPtrT() : *cons.begin(); }

	void ForEach( NCPForEachF (&doWhat)(ConPtrT cl, void *_data), void *data )
	{
		LOCK_ME;

		ConIterT it = cons.begin();
		ConIterT killit;
		NCPForEachF res;
		while( it != cons.end() ) {
			LOG_NET_DETAIL2("Iterating through cons: %s...\n", (*it)->ToString().c_str() );
			res = doWhat(*it, data);
			if( res & NCP_DELETE ) {
				killit = it;
				++it;
				epIndex.Erase( (*killit)->GetEndpoint() );
				idIndex.Erase( (*killit)->GetId() );
				cons.erase(killit);
			} else {
				++it;
			}
			if( res & NCP_STOP ) { break; }
		};
	};

	bool RemCon( const udp::endpoint &ep )
	{
		LOCK_ME;

		ConIterT it = findIterByEndpoint( ep );
		if( it == cons.end() ) {
			LOG_NET_ERROR("Trying to delete a con(nection) that doesn't exist. Endpoint: %s.\n", AsioUtils::EndpointToString( ep ).c_str() );
			return false;
		}
		NodeConIdT conid = (*it)->GetId(); 
		epIndex.Erase( ep );
		idIndex.Erase( conid );
		cons.erase( it );

		return true;
	}

	bool RemCon( ConPtrT cl )
	{
		if( !cl ) { //cl == NULL
			LOG_NET_ERROR("Trying to delete a NULL con(nection).\n");
			return false;
		}
		return RemCon( cl->GetEndpoint() );
	};

	std::string ToString( const std::string &sep = "\n" )
	{
		LOCK_ME;

		std::string dst;
		StrUtils::Printf( dst, "Con(nection) pool: %d cons.", cons.size() );
		if( cons.size() == 0 ) {
			return dst;
		}

		dst.append( sep ); 
		int num = 1;
		ConIterT it = cons.begin();
		ConIterT endit = cons.end();
		while( it != endit ) {
			StrUtils::Printf( dst, "#%d ", num );
			dst.append( (*it)->ToString( sep ) );
			++it;
			++num;
			if( it != endit ) { dst.append( sep ); };
		}

		return dst;
	};

protected:
	ConIterT findIterByEndpoint( const udp::endpoint &ep ) { return epIndex.FindIter( ep ); };

protected:
	NodeConIdT	generateConId()
	{
		++lastConId;
		if( lastConId == 0 ) { lastConId = 1; }
		return lastConId;
	};

protected:
	NodeConIdT	lastConId;
	void			*node;	//points to the ClickyNode that owns this pool
	ConListT		cons;

	ListIndexer<ConListT, ConPtrT, udp::endpoint> epIndex;
	ListIndexer<ConListT, ConPtrT, NodeConIdT> idIndex;
};

#endif //_Node_Con_Pool_h
