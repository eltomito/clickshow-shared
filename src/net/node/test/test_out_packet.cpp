/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "boost/asio.hpp"

#include "shared/src/net/asio_utils.h"
#include "shared/src/net/packets/udp_packets.h"
#include "shared/src/net/node/out_packet_pool.h"

int main() {
	UdpPacketHi hip("Kino Uchylu","Kamil","limak", 111);
	UdpPacketShow showp(1, "Paradni titulek", 2222);
	UdpPacketBye byep(333);
	UdpPacketTouch touchp(44444);
	UdpPacketInvalid invalidp;

	OutPacket opk1( NULL, &hip );
	OutPacket opk2( NULL, &showp );
	OutPacket opk3( NULL, &byep );
	OutPacket opk4( NULL, &touchp );
	OutPacket opk5( NULL, &invalidp );

	printf( "%s\n", opk1.ToString().c_str() );
	printf( "%s\n", opk2.ToString().c_str() );
	printf( "%s\n", opk3.ToString().c_str() );
	printf( "%s\n", opk4.ToString().c_str() );
	printf( "%s\n", opk5.ToString().c_str() );

	printf("\n======== let's test the packet pool now! Splash, splash! =============\n\n");

	ClickyNode fakeNode;

	OutPacketPool pool( fakeNode );
	OutPacket *op1 = pool.AllocPacket( UdpPacketShow( 2, "Druhej paradni titulek", 2223) );	
	OutPacket *op2 = pool.AllocPacket( UdpPacketShow( 3, "Treti paradni titulek", 2224) );	
	OutPacket *op3 = pool.AllocPacket( UdpPacketShow( 4, "A ctvrtej uplne debilni titulek", 2225) );	

	printf( "%s\n", pool.ToString().c_str() );
}
