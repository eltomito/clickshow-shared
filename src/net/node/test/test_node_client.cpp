/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "shared/src/net/node/node_con_pool.h"
#include "boost/asio.hpp"

#include "shared/src/net/asio_utils.h"

int main() {
	NodeConPool ncp;

	NodeUser u1("Honza");
	NodeUser u2("Pepa");
	NodeUser u3("Trandak");

	NodeCon *c = ncp.AddCon( AsioUtils::MakeEndpoint(192,0,0,1,80) );
	c->SetUser(u1);
	c = ncp.AddCon( AsioUtils::MakeEndpoint(80,90,100,200,8888) );
	c->SetUser(u2);
	c = ncp.AddCon( AsioUtils::MakeEndpoint(255,255,255,255,255) );
	c->SetUser(u3);

	printf( "%s\n", ncp.ToString().c_str() );
	
	ncp.RemCon( AsioUtils::MakeEndpoint(80,90,100,200,8888) );

	printf( "==============\n%s\n", ncp.ToString().c_str() );

}

