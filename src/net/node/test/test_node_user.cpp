/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "shared/src/net/node/node_user_pool.h"

int main() {
	NodeUserPool nup;
	nup.AddUser("Pepík","pepa", PERM_CLICK_SUBS);
	nup.AddUser("Ohňan","fire", PERMS_ALL);
	nup.AddUser("Emil","lime", 0);

	NodeUser *u;
	u = (NodeUser *)nup.FindByName("Pepík");
	printf("%s\n", u->ToString().c_str() );
//	nup.AddUser("Honza","aznoH",PERM_CLICK_SUBS);
}
