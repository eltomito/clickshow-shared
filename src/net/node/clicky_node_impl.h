/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "clicky_node.h"

#include <boost/thread/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/bind.hpp>
#include <algorithm>

#include "shared/src/net/packets/packet_reader.h"

//------------- ClickyNode ---------------

/**
 * @warning Throws an exception when the port is already in use!
 */
template <class ConT>
ClickyNode<ConT>::ClickyNode( boost::asio::io_service &_ioService, unsigned short port, bool _autoAdjustTime ) :
																//bufCount(0),
																cons((void *)this),
																alive(false),
																netThread(NULL),
																receiveThread(NULL),
																tickMilliseconds(DEFAULT_TICK_MS),
																ioService(_ioService),
																tickTimer( _ioService ),
																autoAdjustTime( _autoAdjustTime ),
																inDataBuffer( inDataRaw, sizeof(inDataRaw ) ),
																udpSocket( ioService, udp::endpoint(udp::v4(), port ) )
{
}

template <class ConT>
ClickyNode<ConT>::~ClickyNode()
{
	stop();
}

template <class ConT>
unsigned short ClickyNode<ConT>::GetLocalUdpPort()
{
	LOCK_ME;
	return udpSocket.local_endpoint().port();
}

template <class ConT>
void ClickyNode<ConT>::Start()
{
	if( netThread || receiveThread ) {
		LOG_NET_WARN("An old net thread is still running! (net: %s, receive: %s) Let's try to stop it!\n",
							netThread ? "running":"stopped", receiveThread ? "running":"stopped");
		stop();
	}

	LOCK_ME;
	alive = true;
	ioService.reset();
	startTickTimer();
	netThread = new boost::thread( boost::bind( &boost::asio::io_service::run, &ioService ) );
	receiveThread = new boost::thread( boost::bind( &ClickyNode<ConT>::receiveLoop, this ) );
}

template <class ConT>
void ClickyNode<ConT>::Stop()
{
	stop();
}

template <class ConT>
void ClickyNode<ConT>::stop()
{
//	LOCK_ME;

	Lock();
	timerDead = false;
	alive = false;
	Unlock();
	LOG_NET_INFO("Killing the timer...\n");
	tickTimer.cancel();
	for( int i = 0; i < 10; ++i ) {
		if( timerDead ) {
			LOG_NET_INFO("The timer is dead!\n");
			break;
			LOG_NET_INFO("Giving up in %i...!\n", i);
		}
		boost::this_thread::sleep_for( boost::chrono::seconds(1) );
	};

	if( receiveThread != NULL ) {
		LOG_NET_INFO("Interrupting receiveThread...\n");
		receiveThread->interrupt();
		LOG_NET_INFO("receiveThread is finished!\n");
		delete receiveThread;
		receiveThread = NULL;
	}

	ioService.stop();
	if( netThread != NULL ) {
		LOG_NET_INFO("Waiting for netThread to finish...\n");
		if( !netThread->try_join_for( boost::chrono::seconds(2) ) ) {
			LOG_NET_INFO("Interrupting netThread...\n");
			netThread->interrupt();
		}
		LOG_NET_INFO("netThread is finished!\n");
		delete netThread;
		netThread = NULL;
	}
	LOG_NET_INFO("ClickyNode 0x%lx STOPPED.\n", (unsigned long)this );
}

template <class ConT>
void	ClickyNode<ConT>::RemCon( ConPtrT cl )
{
	LOCK_ME;
	remCon( cl );
}

template <class ConT>
void	ClickyNode<ConT>::remCon( ConPtrT cl )
{
	LOG_NET_INFO("Deleting connection: %s\n", cl->ToString(",").c_str() );
	cons.RemCon( cl );
};

template <class ConT>
typename ClickyNode<ConT>::ConPtrT ClickyNode<ConT>::AddCon( const udp::endpoint &_endpoint )
{
	LOCK_ME;
	return addCon( _endpoint );
};

template <class ConT>
typename ClickyNode<ConT>::ConPtrT ClickyNode<ConT>::addCon( const udp::endpoint &_endpoint )
{
	ConPtrT cl;
	
	cl = cons.FindByEndpoint( _endpoint );
	if( cl != NULL ) {
		LOG_NET_ERROR("Cannot add two con(nection)s from the same endpoint (%s).\n", AsioUtils::EndpointToString( _endpoint ).c_str() );
	} else {
		cl = cons.AddCon( _endpoint );
		onNewConnection( cl );
	}
	return cl;
}

template <class ConT>
void ClickyNode<ConT>::startTickTimer()
{
	tickTimer.expires_from_now( boost::posix_time::milliseconds( tickMilliseconds ) );
	tickTimer.async_wait( boost::bind( &ClickyNode::handleTimer, this, boost::asio::placeholders::error ) );
}

template <class ConT>
void ClickyNode<ConT>::receiveLoop()
{
	bool go = true;
	while( go ) {
		boost::system::error_code errc;

		LOG_NET_DETAIL("Waiting for udp socket...\n");
		size_t bytesRecvd = udpSocket.receive_from( inDataBuffer, udpEndpoint, 0, errc );

		if( (long)errc.value() != 0 ) {
			LOG_NET_ERROR("error code %ld: \"%s\"\n", (long)errc.value(), errc.message().c_str() );
			if( errc == boost::system::errc::errc_t::connection_aborted ) {
				LOG_NET_INFO("Abort -> not receiving anymore.\n");
				return;
			}
			if( !alive ) {
				LOG_NET_INFO("This con is dead. Not receiving anymore.\n");
				return;
			}
		}

		LOG_NET_DETAIL("Received %i bytes! Let's handle that!\n", bytesRecvd);
		go = _handleReceiveFrom( errc, bytesRecvd );
	}
}

/**
 * @returns true if the caller should continue to listen()
 */
template <class ConT>
bool ClickyNode<ConT>::_handleReceiveFrom( const boost::system::error_code& errc, size_t bytesRecvd )
{
	if( (long)errc.value() != 0 ) {

		LOG_NET_ERROR("error code %ld: \"%s\"\n", (long)errc.value(), errc.message().c_str() );

		if( errc == boost::system::errc::errc_t::connection_aborted ) {
			LOG_NET_INFO("Abort -> not receiving anymore.\n");
			return false;
		}
		return alive;
	}

	//Leave the LOCK_ME here, don't try to move it
	//to the start of this method.
	//It makes the program crash on login cancel on Windows.
	LOCK_ME;

	if( !alive ) return false;

	LOG_NET_DETAIL("incoming raw dataLen = %ld\n", bytesRecvd );

#if LOG_LEVEL >= 5
	std::string hexdump;
	StrUtils::HexDump( hexdump, inDataRaw, bytesRecvd );
	LOG_NET_DETAIL2("-->Pkt raw data:\n%s\n", hexdump.c_str() );
#endif

	PacketTypeT pktype = PacketReader::Read( inDataRaw, bytesRecvd, inPacket );
	if( pktype != INVALID )
	{
		UdpPacket *pkt = (UdpPacket *)&inPacket;
		LOG_NET_TRAFFIC("-->Pkt: %s\n", pkt->ToString(",").c_str() );
		ConPtrT cl = cons.FindByEndpoint( udpEndpoint );

		LOG_NET_DETAIL("cl.get() = %lu\n", (unsigned long)cl.get() );

		if( !cl ) {
			LOG_NET_DETAIL("Adding Con...\n");
			cl = addCon( udpEndpoint );
			if( !cl ) {
				LOG_NET_ERROR("Failed to add a new con(nection)!!\n");
				return alive;
			};
			LOG_NET_INFO("Added a new con(nection): %s\n", cl->ToString(",").c_str() );
			cl->checkIntegrity( 2 );
		} else {
			LOG_NET_DETAIL("Using existing Con...\n");
			if( autoAdjustTime && pkt->IsAck() && pkt->GetOrigTime().IsValid() ) {
				timeAdjustment.Consider( pkt->GetOrigTime(), timeAdjustment.GetAdjustedNow(), pkt->GetSentTime() );
			}
		};
		cl->handlePacket( *pkt );
		if( !cl->IsAlive() ) {
			onLogout( cl, cl->GetDeathCause() );
			remCon( cl );
		}
	} else {
		LOG_NET_TRAFFIC("-->Received an INVALID packet.\n");
	}

	return alive;
}

class conChoreData
{
public:
	conChoreData( void *_node, ClickyTime *_nowTime ) : node(_node ), nowTime(_nowTime) {};

	ClickyTime *nowTime;
	void 			*node;
};

template <class ConT>
NCPForEachF ClickyNode<ConT>::conChoreFunction( ConPtrT cl, void *_conChoreData )
{
	conChoreData *ccd = (conChoreData*)_conChoreData;
	ClickyTime &nowTime = *(ccd->nowTime);
	cl->DoChores( nowTime );
	if( !cl->IsAlive() ) {
		((ClickyNode<ConT>*)ccd->node)->onLogout( cl, cl->GetDeathCause() );
		LOG_NET_INFO("Connection died: %s\n", cl->ToString(",").c_str() );
		return NCPForEachF::NCP_DELETE;
	}
	return NCPForEachF::NCP_CONTINUE;
};

template <class ConT>
void ClickyNode<ConT>::handleTimer(const boost::system::error_code& errc )
{
	LOCK_ME;

	if( (long)errc.value() != 0 ) {
		if( errc == boost::asio::error::operation_aborted ) {
			LOG_NET_INFO("Abort -> not restarting the timer.\n");
			timerDead = true;
			return;
		}
		LOG_NET_ERROR("error code %ld: \"%s\"\n", (long)errc.value(), errc.message().c_str() );
	} else {

		ClickyTime nowTime = timeAdjustment.GetAdjustedNow();
		conChoreData ccd( (void *)this, &nowTime );
		cons.ForEach( conChoreFunction, (void *)&ccd );
	}
	if( alive ) {
		startTickTimer();
	} else {
		timerDead = true;
	}
}

template <class ConT>
bool ClickyNode<ConT>::Send( ConT *cl, UdpPacket &pkt, bool setSentTime, bool autoOrigTime )
{
	if( !cl ) {
		LOG_NET_ERROR("Can't send a packet to the NULL con(nection)! Why would you want to do that anyway?!\n");
		return false;
	}

	if(( autoOrigTime )&&( pkt.IsAck() )) {
		pkt.SetOrigTime( pkt.GetSentTime() );
	}

	if( setSentTime ) {
		pkt.SetSentTime( timeAdjustment.GetAdjustedNow() );
	}

	OutPacket *opk = outPackets.AllocPacket( &pkt );
	if( opk == NULL ) {
		LOG_NET_ERROR("FAILED to allocate OutPacket!\n");
		return false;
	}
	LOG_NET_TRAFFIC("Sending packet %s to %s.\n", opk->ToString(",").c_str(), cl->ToString(",").c_str() );
	opk->Send( udpSocket, cl->GetEndpoint() ); //let's see how long I can get away with this.
	return true;
}

/*
 * name is modified by removing the protocol-version information.
 *
 * @returns the protocol version (1 or 2)
 */
template <class ConT>
int ClickyNode<ConT>::decipherUserName( std::string &name )
{
	int v = 1;
	if( name.size() < 3 ) return v;
	if( name[0] != '{' ) return v;
	int i = 1;
	int l = name.size();
	int num = 0;
	while( ( i < l )&&( name[i] >= '0' )&&( name[i] <= '9' ) ) {
		num *= 10;
		num += (int)(name[i] - '0');
		++i;
	}
	if( i >= l ) return v;
	if( name[i] != '}' ) return v;
	name.erase(0,i+1);
	return num;
}

template <class ConT>
bool ClickyNode<ConT>::Login( ConPtrT cl, const std::string &name, const std::string &pwd )
{
	std::string realName = name;
	int protVer = decipherUserName( realName );
	if(( protVer < 1 )||( protVer > 2 )) {
		LOG_NET_ERROR("Unknown protocol version: %i!\n", protVer );
		return false;
	}

	bool res = true;
	NodeUser *us = users.FindByName( realName );
	if( us == NULL ) {
		LOG_NET_ERROR("Cannot log in an unknown user: \"%s\".\n", realName.c_str() );
		res = false;
	} else {
		if( us->CheckPwd( pwd ) ) {
			LOG_NET_INFO("Logging in: \"%s\", protocol version %i.\n", realName.c_str(), protVer );
			cl->SetUser( *us );
			cl->SetProtocolVersion( protVer );
			onLogin( cl );
			res = true;
		} else {
			LOG_NET_ERROR("Bad password for user \"%s\".\n", realName.c_str() );
			res = false;
		}
	}
	return res;
};

template <class ConT>
std::string ClickyNode<ConT>::ToString( const std::string &sep )
{
	std::string dst;
	StrUtils::Printf(dst, "--- Clicky Node ---\nTime adjustment: %s ms\nConnections:\n", timeAdjustment.ToString().c_str() );
	dst.append( cons.ToString(sep).c_str() );
	return dst;
}
