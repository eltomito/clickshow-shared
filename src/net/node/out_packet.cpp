/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "out_packet.h"
#include "out_packet_pool.h"
#include "shared/src/net/packets/packet_reader.h"
#include "shared/src/str_utils.h"

#include <boost/bind.hpp>

void OutPacket::SetPacket( const UdpPacket &pkt )
{
	rawLen = pkt.Serialize( rawData, sizeof(rawData) );
}

void OutPacket::SetPacket( const UdpPacket *pkt )
{
	UdpPacketInvalid upi;
	pkt = (pkt == NULL) ? &upi : pkt;
	SetPacket( *pkt );
}

void OutPacket::Send( udp::socket &sock, const udp::endpoint &ep )
{
	LOG_NET_DETAIL2("-->Packet raw data:\n%s\n", ToString(",",true,true).c_str() );

	endpoint = ep;
	sock.async_send_to(	boost::asio::buffer(	rawData,
													rawLen
												),
								endpoint,
								boost::bind(	&OutPacket::handle_send_to, this,
													boost::asio::placeholders::error,
													boost::asio::placeholders::bytes_transferred
												)
							);
}

void OutPacket::freeMe()
{
	if( pool == NULL ) {
		LOG_NET_ERROR("Cannot free packet, pool == NULL! packet recipient: %s\n", AsioUtils::EndpointToString( endpoint ).c_str() );
		return;
	}
	pool->FreePacket( this );	
}

void OutPacket::handle_send_to( const boost::system::error_code& errc, size_t bytesSent )
{
	if( (long)errc.value() != 0 ) {
		LOG_NET_ERROR("Send() FAILED! errc=%ld: \"%s\"\n", (long)errc.value(), errc.message().c_str() );
	} else {
		LOG_NET_DETAIL2("Send() OK: bytesSent=%ld, rawLen=%ld\n", bytesSent, rawLen );
	}
	freeMe();
};

std::string OutPacket::ToString( const std::string &separator, bool includingPacket, bool includingHexdump )
{
	std::string dst;
	std::string pktStr = "...";

	if( includingPacket ) {
		UdpPacketAny ap;
		if( INVALID != PacketReader::Read( rawData, rawLen, ap ) ) {
			pktStr = ((UdpPacket*)&ap)->ToString(", ");
		} else {
			pktStr = "INVALID PACKET";
		}
	}

	StrUtils::Printf(dst, "OutPacket: rawlen: %d%sep: %s%spool: %lx%scontent: %s",
									rawLen, separator.c_str(),
									AsioUtils::EndpointToString(endpoint).c_str(), separator.c_str(),
									(unsigned long int)pool, separator.c_str(),
									pktStr.c_str() );

	if( includingHexdump ) {
		std::string hd;
		StrUtils::HexDump( hd, rawData, rawLen );
		StrUtils::Printf( dst, "serialized data:\n%s", hd.c_str() );
	}

	return dst;
}
