/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _User_Perms_h
#define _User_Perms_h 1

typedef unsigned long UserPermsT;
static const UserPermsT PERM_CLICK_SUBS 	= 1;
static const UserPermsT PERM_CHAT_TO_ALL	= 1 << 1;
static const UserPermsT PERM_SET_PERMS		= 1 << 2;
static const UserPermsT PERM_LOAD_SUBS		= 1 << 3;
static const UserPermsT PERM_NEXT			= 1 << 4;

static const UserPermsT PERMS_ALL = PERM_NEXT - 1;
static const UserPermsT PERMS_MASTER = PERM_CLICK_SUBS;
static const UserPermsT PERMS_SLAVE = 0;

#endif //_User_Perms_h
