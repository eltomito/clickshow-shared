/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Node_User_h
#define _Node_User_h 1

#include <string>
#include "shared/src/str_utils.h"
#include "user_perms.h"

class NodeUserPool;

class NodeUser
{
	friend class NodeUserPool;
public:
	NodeUser( const std::string &_name, const std::string &_pwd = "", UserPermsT _perms = 0 ) : perms(_perms) { name = _name; pwd = _pwd; };
	~NodeUser() {};

	bool CanClickSubs() const { return 0 != (perms & PERM_CLICK_SUBS); };
	bool CanChatToAll() const { return 0 != (perms & PERM_CHAT_TO_ALL); };
	UserPermsT GetPerms() const { return perms; };
	void SetPerms( UserPermsT _perms ) { perms = _perms; };
	void AddPerms( UserPermsT _perms ) { perms |= _perms; };
	void RemPerms( UserPermsT _perms ) { perms &= ~_perms; };

	const std::string &GetName() const { return name; };
	const bool CheckPwd( const std::string &_pwd ) const { return ( 0 == pwd.compare( _pwd ) ); };

	std::string ToString( const std::string &separator = "\n", bool includePwd = false )
	{
		std::string dst;
		StrUtils::Printf(dst, "NodeUser: name: %s%sperms: %x%spwd: \"%s\"",name.c_str(),separator.c_str(),perms,separator.c_str(), includePwd ? pwd.c_str() : "...");
		return dst;
	};

protected:
	class findByName
	{
	public:
		findByName( const std::string &_name ) : name(_name) {};
		bool operator()(const NodeUser &u) { return 0 == u.name.compare( name ); };
		const std::string &name;	
	};

	std::string name;
	std::string pwd;
	UserPermsT perms;		
};

#endif //_Node_User_h
