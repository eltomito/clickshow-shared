/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "out_packet_pool.h"

OutPacket *OutPacketPool::AllocPacket( const UdpPacket *pkt )
{
	LOCK_ME

	OutPacket outpkt( this, pkt );
	try {
		packets.push_front( outpkt );
	} catch(...) {
		LOG_NET_ERROR("FAILED to add an OutPacket to the list!\n");
		return NULL;
	}
	pliterT it = packets.begin();
	OutPacket *res = &(*it);
	ptrIndex.Insert( res, it );

	return res;
}

void OutPacketPool::FreePacket( OutPacket *pkt )
{
	LOCK_ME

	deleteByPtr( freeMe );
	freeMe = pkt;
}

void OutPacketPool::deleteByPtr( OutPacket *pkt )
{
	if( pkt == NULL ) {
		LOG_NET_DETAIL("Not deleting a NULL packet.\n");
		return;
	}
	LOG_NET_DETAIL2("About to delete this packet: %s from this pool: %s\n", pkt->ToString().c_str(), _ToString("\n").c_str() );	

	pliterT it = ptrIndex.FindIter( pkt );
	if( it == packets.end() ) {
		LOG_NET_WARN("OutPacket at %lx not found!\n", (unsigned long)pkt );
	} else {
		ptrIndex.Erase( pkt );
		packets.erase( it );
		LOG_NET_DETAIL("OutPacket at %lx found and deleted!\n", (unsigned long)pkt );
	}
};

std::string OutPacketPool::_ToString( const std::string &sep )
{
	std::string dst;
	StrUtils::Printf( dst, "OutPacket pool: %d packets.", packets.size() );
	if( packets.size() == 0 ) { return dst; }

	dst.append( sep ); 
	int num = 1;
	PacketListT::iterator it = packets.begin();
	PacketListT::iterator endit = packets.end();
	while( it != endit ) {
		StrUtils::Printf( dst, "#%d ", num );
		dst.append( it->ToString( sep ) );
		++it;
		++num;
		//if( it != endit ) { dst.append( sep ); };
	}
	return dst;
};
