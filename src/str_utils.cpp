/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "str_utils.h"
#include "default_log.h"
#include <climits>

bool StrUtils::Printf( std::string &dst, const char *fmt,... )
{

		char buf[ printBufLen ];
		int res = 0;
		bool retval = true;

		va_list args;
		va_start(args, fmt);

#ifndef WIN32
		res = vsnprintf( buf, sizeof( buf ), fmt, args );
#else
		res = _vsnprintf( buf, sizeof( buf ), fmt, args );
#endif //snprintf

		va_end( args );

		if( res < 0 ) {
			retval = false;
		} else {
			if( res >= sizeof( buf ) ) {
				buf[ sizeof(buf) - 1 ] = 0;
				retval = false;
			}
			dst.append( buf );
		}
		return retval;
	};

void StrUtils::HexDump( std::string &dst, void *addr, size_t len, int rowLen )
{
		int i;
		
#ifdef CPLUSPLUS11
		char hexBuf[ rowLen * 3 + 1 ];
		char charBuf[ rowLen + 1 ];
#else //CPLUSPLUS11
#define MAX_HEXDUMP_ROW_LEN 256
		char hexBuf[  MAX_HEXDUMP_ROW_LEN * 3 + 1 ];
		char charBuf[ MAX_HEXDUMP_ROW_LEN + 1 ];
		rowLen = ( rowLen <= MAX_HEXDUMP_ROW_LEN ) ? rowLen : MAX_HEXDUMP_ROW_LEN;
#endif //CPLUSPLUS11

		unsigned char *pc = (unsigned char*)addr;

		if (len <= 0) {
			dst.append("<no data>");
			return;
		}

		size_t rowCnt = 0;
		char *hexpos = hexBuf;
		char *charpos = charBuf;

		for( size_t i = 0; i < len; i++ ) {
			if( rowCnt == rowLen ) {
				hexpos[0] = 0;
				charpos[0] = 0;
				hexpos = hexBuf;
				charpos = charBuf;
				dst.append( hexBuf );
				dst.append( " " );
				dst.append( charBuf );
				dst.append("\n");
				rowCnt = 0;
			}
			sprintf( hexpos, "%02x ", pc[0] );
			hexpos += 3;
			if( !isprint( pc[0] ) ) {
				charpos[0] = '.';
			} else {
				charpos[0] = pc[0];
			}
			++charpos;
			++pc;
			++rowCnt;
		}
		
		if( rowCnt <= rowLen ) {
			for( int j = 0; j < (rowLen-rowCnt)*3; j++ )
			{
				hexpos[0] = ' ';
				++hexpos;
			}
	
			hexpos[0] = 0;
			charpos[0] = 0;
			dst.append( hexBuf );
			dst.append( " " );
			dst.append( charBuf );
			dst.append("\n");
		}
	};

	size_t StrUtils::RFind_First_Not_Of( const std::string str, const std::string &set )
	{
		if( str.empty() ) { return std::string::npos; }

		size_t strLen = str.size();
		size_t setLen = set.size();
		long pos = strLen - 1;
		while(( pos >= 0 )&&( std::string::npos != set.find( str.at( (size_t)pos ) ) )) { --pos; };
		return pos;
	};

	std::string &StrUtils::TrimInPlace( std::string &str )
	{
		if( str.empty() ) { return str; }
		size_t end = RFind_First_Not_Of(str, " \t\n\r");
		size_t endlen;
		if( end == std::string::npos ) {
			str.clear();
			return str;
		}
		endlen = str.size() - 1 - end;
		if( endlen > 0 ) {
			str.erase(end + 1, endlen);
		}

		size_t start = str.find_first_not_of(" \t\n\r");
		if( start > 0 ) {
			str.erase( 0, start );
		}

		return str;
	};

	std::string StrUtils::Trim( const std::string &str )
	{
		std::string res(str);
		TrimInPlace( res );
		return res;
	};

	std::string &StrUtils::PathAppend( std::string &first, const std::string &second )
	{
		if( second.size() == 0 ) {
			return first;
		}

		if( first.size() == 0 ) {
			first = second;
			return first;
		}

		int slashes = ( first[ first.size() - 1 ] == '/' ) ? 1 :0;
		slashes += ( second[0] == '/' ) ? 1 : 0;
		switch ( slashes ) {
			case 0:
				first.push_back('/');
			break;
			case 2:
				first.erase( first.size() - 1, 1 );
			break;
		};
		first.append( second );
		return first;
	};
	
	std::string &StrUtils::PrintInt64( std::string &dst, boost::int64_t ms )
	{
		if( ms > (boost::int64_t)LONG_MAX ) {
			long hi = ms / (boost::int64_t)ULONG_MAX;
			if( ms < 0 ) { ms = (boost::int64_t)(-1) * ms; }
			unsigned long lo = ms % (boost::int64_t)ULONG_MAX;
			StrUtils::Printf( dst, "%ld%lu", hi, lo );
		} else {
			StrUtils::Printf( dst, "%ld", (long) ms );
		}
		return dst;
	};


	long StrUtils::ParseLong( const std::string src, bool &res, int base )
	{
		char *end;
		long  l;

		if( src.empty() ) {
			res = false;
			return 0;
		}

		l = strtol(src.c_str(), &end, base);
		if(( errno == ERANGE )||( *end != 0 )) {
			res = false;
			return 0;
		}

		res = true;
		return l;
	};


/* Truncates a UTF-8 string to a given length so that it stays valid UTF-8.
 */
std::string StrUtils::TruncUTF8String( const std::string &str, size_t len )
{
	if( len >= str.size() ) { return str; }
	if( len == 0 ) { return ""; }
	if( ((unsigned char)str[ len - 1 ] & 0x80 ) == 0 ) {
		//the final character is a regular ascii one-byte code point.
		return str.substr( 0, len );
	}

	//let's find the beginning of the multi-byte code point. It's a byte that look like this: 11xxxxxx.
	int i = len - 2;
	while( ( i >= 0 )&&( ( ((unsigned char)str[i]) & 0xc0 ) != 0xc0 ) ) {
		i--;
	}
	if( i < 0 ) {
		return "";	//the string was invalid UTF-8
	}

	//let's find the length of this code point in bytes
	int cplen = 2;
	unsigned char byte1 = (unsigned char)str[i] << 2;
	while(( ( byte1 & 0x80 ) != 0 )&&( cplen < 4 )) {
		++cplen;
		byte1 = byte1 << 1;
	}

	if( (i + cplen) > len ) {
		//this multi-byte character extends past len, so let's truncate the string before it.
		return str.substr(0,i);
	}

	//we can leave the whole multi-byte character in the result string.
	return str.substr( 0, i + cplen );
}
