/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Errr_h
#define _Errr_h

class Errr
{
public:
	static const int OK = 0;		//Success!
	static const int FAILED = 1;	//Generic failure
	static const int ALLOC = 2;	//Cannot allocate memory
	static const int ASSERT = 3;
	static const int MISSING_DATA = 4;

	static const int	FILE_STAT = 100;
	static const int	FILE_OPEN = 101;
	static const int	FILE_READ = 102;
	static const int	FILE_WRITE = 103;
	static const int	BAD_COUNT = 104;

	static const int	SUB_FMT_UNKNOWN = 300;
	static const int	SUB_FMT_GARBAGE = 301;
	static const int	SUB_FMT_TIMING = 302;
	static const int	SUB_TIME_OVERFLOW = 303;
	static const int	SUB_NUM_OVERFLOW = 304;

	static const int ENCODING_UNKNOWN = 200;

#define ERRR_KNOWN_ERRORS 16

	static const char *Message( int error_code );

private:
	Errr() {};
	~Errr() {};

	static const int error_codes[ERRR_KNOWN_ERRORS];
	static const char *messages[ERRR_KNOWN_ERRORS];
};

#endif //_Errr_h
