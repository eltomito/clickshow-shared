/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "debug_fn.h"
#include "shared/src/default_log.h"
#include <cstdarg>
#include <cstring>
#include <cstdio>
#include <ctime>
#include <cstdlib>

/*
 */
void __clicky_debug_message2(
		const char* file,
		int line,
		const char* function,
		unsigned int logModule,
		const char *format, ...)
{
		char msgbuf[4096];
		struct tm *tms;
		time_t nowtime;
		int cnt;

		va_list args;

		if( format == NULL )
			return;

		const char *intro = "";
		switch( logModule ) {
			case LOGMOD_GEN:
				intro = " ";
			break;
			case LOGMOD_NET:
				intro = "N";
			break;
			case LOGMOD_RENDER:
				intro = "R";
			break;
			case LOGMOD_FILE:
				intro = "F";
			break;
			case LOGMOD_CANDY:
				intro = "C";
			break;
			case LOGMOD_HTTP:
				intro = "H";
			break;
			case LOGMOD_HOLE:
				intro = "O";
			break;
			case LOGMOD_GUI:
				intro = "G";
			break;
		};

		nowtime = time( NULL );
		tms = localtime( &nowtime );

#if 1
		va_start(args, format);
		cnt = vsnprintf( &msgbuf[0], sizeof(msgbuf), format, args);
		va_end(args);
		msgbuf[ sizeof(msgbuf) - 1 ] = 0;	//supposedly, there's a vsnprintf bug...
#else
		strncpy( msgbuf, format, sizeof(msgbuf) );
		msgbuf[ sizeof(msgbuf) - 1 ] = 0;
		cnt = 1;
#endif

		if( cnt > sizeof( msgbuf ) )
		{
			fprintf( stderr, "CLICKY DEBUG WARNING: msg buffer overflow, truncating output. (max size: %ld < string size: %d\n", sizeof(msgbuf), cnt );
		};

		//printf("%s:%d (%s) %s", file, line, function, &msgbuf[0]);
		fprintf( stderr, "%s: %02i-%02i-%02i %02i:%02i:%02i (%s) %s", intro,
			(tms->tm_year - 100), (tms->tm_mon + 1), tms->tm_mday,
			tms->tm_hour, tms->tm_min, tms->tm_sec, function, &msgbuf[0]);

		fflush(stderr);
}

/*
 *
 */
void __clicky_debug_message(
		const char* file,
		int line,
		const char* function,
		const char *format, ...)
{
		char msgbuf[2000];
		struct tm *tms;
		time_t nowtime;
		int cnt;

		va_list args;

		if( format == NULL )
			return;

		nowtime = time( NULL );
		tms = localtime( &nowtime );

		va_start(args, format);
		cnt = vsprintf( &msgbuf[0], format, args);
		va_end(args);

		if( cnt >= sizeof( msgbuf ) )
		{
			fprintf( stderr, "CLICKY DEBUG: msg buffer overflow!!! TERMINATING my glorious existence!!!\n");
			exit( 1 );
		};

		//printf("%s:%d (%s) %s", file, line, function, &msgbuf[0]);
		fprintf( stderr, "%2i:%2i:%2i (%s) %s", tms->tm_hour, tms->tm_min, tms->tm_sec, function, &msgbuf[0]);

		fflush(stderr);
}

/*
 */
 /*
void clicky_debug_list_group( Fl_Group *g )
{
	int cnt = (int)g->children();
	Fl_Widget **arr = (Fl_Widget **)g->array();
	Fl_Widget *w;

	for( int i = 0; i < cnt; i++ )
	{
		w = arr[i];
		clicky_debug_message("child %i: x=%i, y=%i, w=%i, h=%i, label = \"%s\"\n",
									i, w->x(), w->y(), w->w(), w->h(), w->label() );
	};
}
*/