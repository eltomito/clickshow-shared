#ifndef _flag_macro_h_
#define _flag_macro_h_ 1

#define CREATE_FLAG( name, varname, bitnum ) \
	static const unsigned int FLAG_##name = 1 << bitnum;\
	void set_##name ( bool state ) { varname = varname & ( ~FLAG_##name ); if( state ) { varname |= FLAG_##name ;} }; \
	void set_##name ( unsigned int &flags, bool state ) { flags = flags & ( ~FLAG_##name ); if( state ) { flags |= FLAG_##name ;} }; \
	bool is_##name () const { return varname & ( FLAG_##name ); };\
	bool is_##name ( unsigned int flags ) const { return flags & ( FLAG_##name ); };

#define CREATE_MASKED_FLAG( name, varname, maskvarname, bitnum ) \
	static const unsigned int FLAG_##name = 1 << bitnum;\
	void set_##name ( bool state ) { maskvarname |= FLAG_##name; varname = varname & ( ~FLAG_##name ); if( state ) { varname |= FLAG_##name ;} }; \
	void ignore_##name () { maskvarname &= ~FLAG_##name; }; \
	bool affects_##name () const { return FLAG_##name & maskvarname; };\
	bool sets_##name () const { return affects_##name() && (FLAG_##name & varname); };\
	bool clears_##name () const { return affects_##name() && !(FLAG_##name & varname); };\

#endif //_flag_macro_h_
