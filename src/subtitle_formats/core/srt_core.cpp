#include "srt_core.h"
#include <cstring>

long SrtCore::ParseTime( const char *str )
{
	char h1,h2,m1,m2,s1,s2,x,y,z;
	if( !str ) { return BADTIME; }
	char c = str[0];

	if(( c < '0' )||( c > '9' )) { return BADTIME; }
	h1 = (long)(c - '0');
	++str;
	c = str[0];
	if(( c < '0' )||( c > '9' )) { return BADTIME; }
	h2 = (long)(c - '0');
	++str;
	c = str[0];
	if( c != ':') { return BADTIME; }
	++str;

	c = str[0];
	if(( c < '0' )||( c > '5' )) { return BADTIME; }
	m1 = (long)(c - '0');
	++str;
	c = str[0];
	if(( c < '0' )||( c > '9' )) { return BADTIME; }
	m2 = (long)(c - '0');
	++str;
	c = str[0];
	if( c != ':') { return BADTIME; }
	++str;

	c = str[0];
	if(( c < '0' )||( c > '5' )) { return BADTIME; }
	s1 = (long)(c - '0');
	++str;
	c = str[0];
	if(( c < '0' )||( c > '9' )) { return BADTIME; }
	s2 = (long)(c - '0');
	++str;
	c = str[0];
	if( c != ',') { return BADTIME; }
	++str;

	c = str[0];
	if(( c < '0' )||( c > '9' )) { return BADTIME; }
	x = (long)(c - '0');
	++str;
	c = str[0];
	if(( c < '0' )||( c > '9' )) { return BADTIME; }
	y = (long)(c - '0');
	++str;
	c = str[0];
	if(( c < '0' )||( c > '9' )) { return BADTIME; }
	z = (long)(c - '0');

	long res = (((h1*10 + h2)*60 + (m1*10 + m2))*60 + (s1*10 + s2))*1000 + x*100 + y*10 + z;
//	LOG_CANDY_DETAIL2("parsed time: %i from intermediate %i%i:%i%i:%i%i,%i%i%i\n", res, h1,h2,m1,m2,s1,s2,x,y,z );
	return res;
};

int SrtCore::ParseTiming( const char *str, SubTiming &st )
{
	st.Reset();
	long start = ParseTime( str );
	if( start == BADTIME ) { return -1; };
	str += 12;
	if( 0 != strncmp( str, " --> ", 5 ) ) { return -1; };
	str += 5;
	long end = ParseTime( str );
	if( end == BADTIME ) { return -1; };
	str += 12;
	if(( str[0] != '\n' )&&( str[0] != 0 )) {
		return -1;
	}
	if(( start != UNTIMED_TIME )||( end != UNTIMED_TIME )) {
		st.SetStart( start );
		st.SetEnd( end );
	}

	return 29;
};

int SrtCore::ParseSubNum( const char *str, int &len, int max_len )
{
	char c;
	int res = 0;
	unsigned int newres;
	int cnt = 0;
	for( int i = 0; i < max_len; ++i ) {
		cnt = i;
		c = str[0];
		if(( c >= '0' )&&( c <= '9' )) {
			newres =((unsigned int)res) * 10 + (unsigned int)(c - '0');
			if(( newres > INT_MAX )||( newres > MAX_SUB_NUM )) {
				return BADSUBNUM;
			}
			res = (int)newres;
		} else {
			break;
		}
		++str;
	};
	len = cnt;
	if( cnt == 0 ) {
		return BADSUBNUM;
	}
	return res;
};

int SrtCore::TimeToParts( int t, int &hours, int &min, int &sec, int &milli )
{
	milli = t % 1000;
	t = (t - milli) / 1000;
	sec = t % 60;
	t = (t - sec) / 60;
	min = t % 60;
	t = (t - min) / 60;
	hours = t;
	return ( hours <= 99 ) ? Errr::OK : Errr::SUB_TIME_OVERFLOW;
};

int SrtCore::FormatSubHead( int num, const SubTiming &st, std::string &dst )
{
	int h1,m1,s1,mm1,h2,m2,s2,mm2;
	char str[ MAX_SUB_NUM_DIGITS + 1 + 12 + 5 + 12 + 1 ];
	size_t len = 0;

	if( num > MAX_SUB_NUM ) {
		return Errr::SUB_NUM_OVERFLOW;
	}

	if( st.IsSet() ) {
		int err = TimeToParts( (int)st.GetStart(), h1, m1 , s1, mm1 );
		if( err ) { return err; }
		err = TimeToParts( (int)st.GetEnd(), h2, m2 , s2, mm2 );
		if( err ) { return err; }
		len = sprintf( str, "%i\n%02i:%02i:%02i,%03i --> %02i:%02i:%02i,%03i\n",num,h1,m1,s1,mm1,h2,m2,s2,mm2 );
	} else {
		len = sprintf( str, "%i\n99:59:59,999 --> 99:59:59,999\n",num );
	}
	dst.append( str, len );

	return Errr::OK;
};
