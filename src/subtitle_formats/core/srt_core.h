/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Srt_Core_h_
#define _Srt_Core_h_

#include "shared/src/err.h"
#include "shared/src/err_desc.h"
#include "shared/src/subtitles/subtitle_timing.h"
#include <climits>

class SrtCore
{
public:
	static const long BADTIME = LONG_MIN;
	static const long BADSUBNUM = INT_MIN;
	static const long MAX_SUB_NUM_DIGITS = 11;
	static const long MAX_SUB_NUM = INT_MAX;
	static const long UNTIMED_TIME = 359999999;

	/*
	 * returns the number of characters parsed or -1 for error.
	 */
	static int ParseTiming( const char *str, SubTiming &st );

	/*
	 * returns the parsed time in milliseconds or BADTIME
	 */
	static long ParseTime( const char *str );

	/*
	 * returns the subtitle number or -1.
	 */
	static int ParseSubNum( const char *str, int &len, int max_len = MAX_SUB_NUM_DIGITS );

	/*
	 * returns an error code.
	 */
	static int TimeToParts( int t, int &hours, int &min, int &sec, int &milli );

	/*
	 * returns an error code.
	 */
	static int FormatSubHead( int num, const SubTiming &st, std::string &dst );

protected:
	SrtCore() {};
	~SrtCore() {};
};

#endif //_Srt_Core_h_
