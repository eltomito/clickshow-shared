/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "srt_stripper.h"
#include "../../str_line_iterator.h"

/**
 * @returns true if the source is valid srt, false otherwise.
 */
bool SrtStripper::SrtToPlain( const std::string &src, std::string *dst, bool strict, int max_subs )
{
	if( src.empty() ) { return true; }

	SubTiming timing;

	std::string line;
	std::string sub_text;
	int sub_cnt = 0;
	int sub_num;
	int state = 0;
	int parsedlen;

	str_line_iterator it( (char *)src.c_str() );
	str_line_iterator endit;

	while(( it != endit )&&( sub_cnt <= max_subs )) {
		line = *it;
		switch( state )
		{
			case 0: //looking for a subtitle number
				if( !line.empty() ) {
					sub_num = SrtCore::ParseSubNum( line.c_str(), parsedlen );
					if(( sub_num != SrtCore::BADSUBNUM )&&( parsedlen == line.size() )) {
						++state;
					} else {
						if( strict ) {
							return false; //garbage found instead of a subtitle number
						} else {
							if( dst ) {
								dst->append( line );
								dst->append( "\n" );
							}
							++state;
						}
					}
				}
			break;

			case 1: //expecting timing
				parsedlen = SrtCore::ParseTiming( line.c_str(), timing );
				if( parsedlen == line.size() ) {
					++state;
				} else {
					if( strict ) {
						return false; //garbage instead of timing
					} else {
						if( dst ) {
							dst->append( line );
							dst->append( "\n" );
						}
						++state;
					}
				}
			break;

			case 2: //subtitle text
				if( line.empty() ) {
					if( dst ) {
						dst->append( sub_text );
						dst->append( "\n\n" );
					}
					++sub_cnt;
					sub_text.clear();
					state = 0;
				} else {
					if( !sub_text.empty() ) { sub_text.push_back('\n'); }
					sub_text.append( line );
				}
			break;
		}
		++it;
	}
	if( state == 2 ) {
		if( dst ) {
			dst->append( sub_text );
			dst->append( "\n" );
		}
	}
	return true;
}
