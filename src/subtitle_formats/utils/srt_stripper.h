/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Srt_Stripper_h_
#define _Srt_Stripper_h_

#include <climits>
#include "../core/srt_core.h"

class SrtStripper
{
public:
	static bool SrtToPlain( const std::string &src, std::string *dst = NULL, bool strict = false, int max_subs = INT_MAX );
	static bool IsSrt( const std::string &substr ) { return SrtToPlain( substr, NULL, true, 10 ); };

protected:
	SrtStripper() {};
	~SrtStripper() {};
};

#endif //_Srt_Stripper_h_
