/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Player_State_h_
#define _Player_State_h_ 1

#include <cmath>

class PlayerState
{
public:
	PlayerState(	const ClickyTime *_startTimestamp = NULL,
						double _startTimecode = 0.0,
						double _delay = 0.0,
						double _speed = 1.0,
						bool _isPlaying = false,
						int _shownSub = -1,
						int _nextSub = -1 )
		: startTimestamp(_startTimestamp),startTimecode(_startTimecode),delay(_delay),
		speed(_speed),isPlaying(_isPlaying),shownSub(_shownSub),nextSub(_nextSub) {};
	~PlayerState() {};

	std::string ToString() const {
		std::string str;
		StrUtils::Printf( str, "stamp: %s, start tc: %lf, delay: %lf, speed: %lf, on: %i, shown: %i, next: %i\n",
			startTimestamp.ToString().c_str(), startTimecode, delay, speed, isPlaying, shownSub, nextSub );
		return str;
	};

	double AheadOf( const PlayerState &other ) const {
		double diff = (double)(startTimestamp - other.startTimestamp);
		diff += startTimecode - other.startTimecode;
		diff += delay - other.delay;
		return diff;
	};

	bool AlmostSameAs( const PlayerState &other, double diffSec = 1.0, double diffSpeed = 0.000001 ) const {
		if( isPlaying != other.isPlaying ) return false;
		if( fabs( speed - other.speed ) > diffSpeed ) return false;
		if(( shownSub != other.shownSub )||( nextSub != other.nextSub )) return false;
		return fabs( AheadOf( other ) ) <= diffSec;
	};

	ClickyTime	startTimestamp;
	double		startTimecode;
	double		delay;
	double		speed;
	bool			isPlaying;
	int			shownSub;
	int			nextSub;
};

#endif //_Player_State_h_
