/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Str_Utils_h
#define _Str_Utils_h 1

#include <cstdarg>
#include <string>
#include "boost/cstdint.hpp"

class StrUtils {

public:
	static const int printBufLen = 16384;

	/*
	 * this is printf for std::string
	 *
	 * returns: true if everything went well, false if there was an error.
	 *
	 * Even if there was an error, some string might have been appended to dst. 
	 */
	static bool Printf( std::string &dst, const char *fmt,... );
	static void HexDump( std::string &dst, void *addr, size_t len, int rowLen = 16 );
	static size_t RFind_First_Not_Of( const std::string str, const std::string &set );
	static std::string &TrimInPlace( std::string &str );
	static std::string Trim( const std::string &str );
	static std::string &PathAppend( std::string &first, const std::string &second );
	static std::string &PrintInt64( std::string &dst, boost::int64_t ms );
	static long ParseLong( const std::string src, bool &res, int base = 10 );
	static std::string TruncUTF8String( const std::string &str, size_t len );
};

#endif //_Str_Utils_h
