/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _List_Searcher_h
#define _List_Searcher_h 1

#include <algorithm>
#include <map>

template <class listT, class elemT, class keyT>
class ListIndexer
{
public:
	typedef typename listT::iterator listIt;
	typedef typename std::map<keyT, listIt> mapT;
	typedef typename mapT::iterator mapIt;
	typedef typename mapT::value_type mapValT;

	ListIndexer( listT &_list ) : list(_list) {};
	~ListIndexer() {};

	/** Insert a new list iterator in the index.
	 * @returns True if the key didn't exist yet and the list iterator was inserted,
	 *          false if the key already existed and nothing was inserted.
	 */
	bool Insert( const keyT key, listIt it )
	{
		std::pair<mapIt,bool> res = imap.insert( mapValT(key, it) );
		return res.second;
	}

	/** Erases the iterator which corresponds to key.
	 * @returns True if the key was found and erased, false otherwise.
	 */
	bool Erase( const keyT key ) { return( imap.erase( key ) > 0 ); };

	void Clear() { imap.clear(); };

	listIt FindIter( const keyT &key )
	{
		mapIt mapit = imap.find( key );
		if( mapit == imap.end() ) { return list.end(); }
		return mapit->second;
	};

	elemT *Find( const keyT &key )
	{
		mapIt mapit = imap.find( key );
		if( mapit == imap.end() ) { return NULL; }
		return &(*(mapit->second));
	};

protected:
	listT	&list;
	mapT	imap;
};

/*
template<typename listT, class isFoundFunctor, typename valueT, typename resultT>
class ListSearcher
{
public:
	ListSearcher( listT &_list ) : list(_list) {}; 

	typedef typename listT::iterator iterT;
	typedef typename listT::const_iterator citerT;

	static citerT findConstIter( valueT val, const listT &lst ) {
		isFoundFunctor isFound(val);
		return std::find_if( lst.begin(), lst.end(), isFound );
	};
	static iterT findIter( valueT val, listT &lst ) {
		isFoundFunctor isFound(val);
		iterT it = lst.begin();
		iterT endit = lst.end();
		while( it != endit ) {
			if( isFound(*it) ) { break; }
			++it;
		}
		return it;
	};

	static resultT *findPtr( valueT val, const listT &lst ) {
		citerT cit = findConstIter( val, lst );
		if( cit != lst.end() ) {
			return (resultT *)&(*cit);
		}
		return NULL;
		//return (cit != lst.end()) ? (resultT *)&(*cit) : NULL;
	}

	citerT FindConstIter( valueT val ) { return findConstIter( val, list ); };
	iterT FindIter( valueT val ) { return findIter( val, list ); };
	resultT *FindPtr( valueT val ) { return findPtr( val, list ); };
	//void ForEach( bool (&doWhat)(resultT &el) ) { forEach( list, doWhat ); };
	bool IsEndIter( const iterT &it ) { return it == list.end(); };

protected:
	listT &list;
};
*/

#endif //_List_Searcher_h
