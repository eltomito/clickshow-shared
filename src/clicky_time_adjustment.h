/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Clicky_Time_Adjustment_h
#define _Clicky_Time_Adjustment_h 1

#include "clicky_time.h"
#include "str_utils.h"
#include "default_log.h"

class ClickyTimeAdjustment
{
public:
	static const boost::int64_t DEFAULT_MAX_ALLOWED_ERROR = 50;	//in milliseconds.

	ClickyTimeAdjustment( boost::int64_t maxAllowedError = DEFAULT_MAX_ALLOWED_ERROR )
		: maxAllowedErrorMs( maxAllowedError )
	{ Clear(); };
	~ClickyTimeAdjustment() {};

	void Clear()
	{
		valueMs = 0;
		maxErrorMs = LONG_MAX;
	};

	ClickyTime &AdjustTimeInPlace( ClickyTime &ct, bool direction = true ) const
	{
		if( direction ) {
			ct += valueMs;
		} else {
			ct -= valueMs;
		}
		return ct;
	};

	ClickyTime AdjustTime( const ClickyTime &ct, bool direction = true ) const
	{
		ClickyTime adjusted(ct);
		return AdjustTimeInPlace( adjusted, direction );
	};

	bool NeedsCorrection() const { return maxErrorMs > maxAllowedErrorMs; };

	ClickyTime GetAdjustedNow( bool direction = true ) const { return AdjustTime( ClickyTime(true), direction ); };

	bool Consider( const ClickyTime &iSent, const ClickyTime &iRec, const ClickyTime &remoteReplied )
	{
		boost::int64_t oneWayMs = remoteReplied - iSent;
		if( ( remoteReplied < iSent )||( remoteReplied > iRec ) ) {
			modifyByEstimate( iSent, iRec, remoteReplied );
			return true;
		}
		if( NeedsCorrection() && ( iRec - iSent < maxErrorMs ) ) {
			maxErrorMs = iRec - iSent;
			return true;
		}
		return false;
	};

	void modifyByEstimate( const ClickyTime &iSent, const ClickyTime &iRec, const ClickyTime &remoteReplied )
	{
		//ClickyTime nowTime = timeAdjustment.GetAdjustedNow();	//best-guess server now
		boost::int64_t travelTime = iRec - iSent;
		ClickyTime assumedRemoteReplied( iSent );
		assumedRemoteReplied += travelTime / 2;
		boost::int64_t newAdjustment = remoteReplied - assumedRemoteReplied;
		valueMs += newAdjustment;
		maxErrorMs = iRec - iSent;

		LOG_NET_INFO("Adjusting time\n\
packet sent by me: %s (best-guess server time)\n\
acked by other   : %s (real server time time)\n\
received by me   : %s (best-guess server time)\n\
packet traveled  : %ld ms\n\
ack-time estimate: %s (best-guess server time)\n\
new guess offset : %ld ms\n",
				iSent.ToTimeOnlyString(true,'.').c_str(),
				remoteReplied.ToTimeOnlyString(true,'.').c_str(),
				iRec.ToTimeOnlyString(true,'.').c_str(),
				(long)travelTime,
				assumedRemoteReplied.ToTimeOnlyString(true,'.').c_str(),
				newAdjustment );

	};

	boost::int64_t Get() const { return valueMs; };
	boost::int64_t GetMaxError() const { return maxErrorMs; };

	std::string ToString( const std::string &sep = "," )
	{
		std::string dst;
		std::string adjStr;
		std::string errStr;
		StrUtils::Printf( dst, "adjustment = %s, maxError = %s",
			StrUtils::PrintInt64( adjStr, valueMs ).c_str(),
			StrUtils::PrintInt64( errStr, maxErrorMs ).c_str()
		);
		return dst;
	};

protected:
	boost::int64_t	valueMs;
	boost::int64_t	maxErrorMs;
	boost::int64_t	maxAllowedErrorMs;
};

#endif //_Clicky_Time_Adjustment_h
