/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "err.h"

const int Errr::error_codes[ERRR_KNOWN_ERRORS] = {	OK,
												FAILED,
												ALLOC,
												ASSERT,
												MISSING_DATA,
												FILE_STAT,
												FILE_OPEN,
												FILE_READ,
												FILE_WRITE,
												BAD_COUNT,
												ENCODING_UNKNOWN,
												SUB_FMT_UNKNOWN,
												SUB_FMT_GARBAGE,
												SUB_FMT_TIMING,
												SUB_TIME_OVERFLOW,
												SUB_NUM_OVERFLOW };

const char *Errr::messages[ERRR_KNOWN_ERRORS] =	{	"Ok",
										"Failed",
										"Out of memory",
										"An assertion failed",
										"No data",
										"Can't find file",
										"Can't open file",
										"Can't read file",
										"Can't write to file",
										"Found a different number of some things than expected",
										"Unknown encoding",
										"Unknown subtitle format",
										"This makes no sense",
										"Timing not understood",
										"Subtitle time out of range",
										"Subtitle number out of range"};

const char *Errr::Message( int error_code )
{
	if( error_code < 0 ) { return "8-? Bad (negative) error code!"; }
	const int *p = &error_codes[0];
	const int *endp = &error_codes[ sizeof(error_codes) ];
	while(( p[0] != error_code )&&( p < endp )) {
		++p;
	}
	if( p >= endp ) { return "8-/ Bad (non-existent) error code!"; }
	return messages[ p - error_codes ];
}
