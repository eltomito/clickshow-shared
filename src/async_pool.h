/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Async_Pool_h
#define _Async_Pool_h 1

#include <list>

#include "shared/src/semaph.h"
#include "shared/src/list_searcher.h"
#include "shared/src/default_log.h"
#include "shared/src/str_utils.h"

template<class ConT>
class AsyncPool : public Lockable
{
public:
	static const int ASP_CONTINUE = 0;
	static const int ASP_STOP = 1;
	static const int ASP_DELETE = 2;

	typedef std::list<ConT *> ConListT;
	typedef typename ConListT::iterator ConIterT;

public:
	AsyncPool() : freeMe(NULL), ptrIndex(cons) {};
	~AsyncPool() { Clear(); };

	bool	AddCon( ConT *newCon )
	{
		LOCK_ME;

		cons.push_front( newCon );
		ConIterT it = cons.begin();
		ptrIndex.Insert( newCon, it );

		onAdd( newCon, it );

		return true;
	};

	bool	RemCon( ConT *ptr )
	{
		LOCK_ME;
		return remCon( ptr );
	};

	ConIterT	FindIterByPtr( ConT *ptr ) { return ptrIndex.FindIter( ptr ); };
	ConT		*GetFirst() { return ( cons.size() == 0 ) ? NULL : *cons.begin(); }

	void DeleteMe( ConT *con )
	{
		LOCK_ME;
		if( freeMe != NULL ) {
			remCon( freeMe );
		}
		freeMe = con;
	};

	std::string ToString( const std::string &sep = "\n" )
	{
		LOCK_ME;
	
		std::string dst;
		StrUtils::Printf( dst, "AsyncPool: %d cons.", cons.size() );
		return dst;
	}

	class ConKiller
	{
	public:
		ConKiller() {};
		int operator()( ConT *cl )
		{
			delete( cl );
			return AsyncPool::ASP_DELETE;
		};
		static int CallBound( ConT *cl, void *obj ) {
			return ( *((ConKiller*)obj) )( cl );
		};
	};

	void	Clear()
	{
		LOCK_ME;
		ptrIndex.Clear();
		onClear();
		ConKiller cstp;
		forEach( ConKiller::CallBound, (void *)&cstp );
		freeMe = NULL;
	};

	void	ForEach( int (&doWhat)(ConT *cl, void *_data), void *data )
	{
		forEach( doWhat, data );
	};

protected:
	void	forEach( int (&doWhat)(ConT *cl, void *_data), void *data )
	{
		ConIterT it = cons.begin();
		ConIterT endit = cons.end();
		ConIterT killit;
		int res;
		while( it != endit ) {
			res = doWhat(*it, data);
			if( res & ASP_DELETE ) {
				killit = it;
				++it;
				cons.erase(killit);
			} else {
				++it;
			}
			if( res & ASP_STOP ) { break; }
		};
	};

	bool	remCon( ConT *ptr )
	{
		ConIterT it = FindIterByPtr( ptr );
		if( it == cons.end() ) {
			LOG_NET_ERROR("Trying to delete a con(nection) that doesn't exist.\n" );
			return false;
		}
		ptrIndex.Erase( ptr );
		cons.erase( it );

		onRem( ptr );

		delete( ptr );
		return true;
	};

protected:
	virtual void onAdd( ConT *cptr, ConIterT it ) {};
	virtual void onRem( ConT *cptr ) {};
	virtual void onClear() {};

protected:
	ConListT		cons;
	ConT			*freeMe;

	ListIndexer<ConListT, ConT, ConT*> ptrIndex;
};

#endif //_Async_Pool_h
