/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _DEFAULT_LOG_H_
#define _DEFAULT_LOG_H_

#include "debug_fn.h"

//FIXME: This should go somewhere else.
#if __cplusplus >= 201103L
#define CPLUSPLUS11 1
#endif //__cplusplus >= 201103L

#ifndef LOG_LEVEL
#ifdef DEBUG
#define LOG_LEVEL 4
#define LOG_MASK (LOGMOD_PLAYER|LOGMOD_GEN|LOGMOD_NET)
#else //DEBUG
#define LOG_LEVEL 0
#define LOG_MASK (LOGMOD_ALL ^ LOGMOD_LOCKME)
#endif
#endif //LOG_LEVEL

/*
#ifndef LOG_MSG
#define LOG_MSG( pri, ... ) if( pri <= LOG_LEVEL ) { printf( __VA_ARGS__ ); }
#endif
*/

#define LOGMOD_ALL		0xffff
#define LOGMOD_GEN		16384
#define LOGMOD_NET		1
#define LOGMOD_RENDER	2
#define LOGMOD_FILE		4
#define LOGMOD_CANDY		8
#define LOGMOD_FLG		16	//file logger
#define LOGMOD_HTTP		32
#define LOGMOD_HOLE		64
#define LOGMOD_LOCKME	128
#define LOGMOD_GUI		256
#define LOGMOD_BB			512
#define LOGMOD_PLAYER	1024

#define LOG_MOD_MSG( _logModule, _priority, ... ) \
if(( _priority <= LOG_LEVEL )&&( ( LOG_MASK & _logModule ) != 0 )) { clicky_debug_message2( _logModule, __VA_ARGS__ ); }

#define LOG_LOCKME(...) LOG_MOD_MSG( LOGMOD_LOCKME, 1, __VA_ARGS__ )

#define LOG_M_ERROR( _mod, ... ) LOG_MOD_MSG( _mod, 0,  __VA_ARGS__ )
#define LOG_M_INFO( _mod, ... ) LOG_MOD_MSG( _mod, 1, __VA_ARGS__ )
#define LOG_M_WARN( _mod, ... ) LOG_MOD_MSG( _mod, 2, __VA_ARGS__ )
#define LOG_M_TRAFFIC( _mod, ... ) LOG_MOD_MSG( _mod, 3, __VA_ARGS__ )
#define LOG_M_DETAIL( _mod, ... ) LOG_MOD_MSG( _mod, 4, __VA_ARGS__ )
#define LOG_M_DETAIL2( _mod, ... ) LOG_MOD_MSG( _mod, 5, __VA_ARGS__ )
#define LOG_M_UNIMP(_mod ) LOG_M_ERROR("UNIMPLEMENTED!\n")

#define LOG_NET_ERROR( ... ) LOG_MOD_MSG( LOGMOD_NET, 0,  __VA_ARGS__ )
#define LOG_NET_INFO( ... ) LOG_MOD_MSG( LOGMOD_NET, 1, __VA_ARGS__ )
#define LOG_NET_WARN( ... ) LOG_MOD_MSG( LOGMOD_NET, 2, __VA_ARGS__ )
#define LOG_NET_TRAFFIC( ... ) LOG_MOD_MSG( LOGMOD_NET, 3, __VA_ARGS__ )
#define LOG_NET_DETAIL( ... ) LOG_MOD_MSG( LOGMOD_NET, 4, __VA_ARGS__ )
#define LOG_NET_DETAIL2( ... ) LOG_MOD_MSG( LOGMOD_NET, 5, __VA_ARGS__ )
#define LOG_NET_UNIMP LOG_NET_ERROR("UNIMPLEMENTED!\n");

#define LOG_CANDY_ERROR( ... ) LOG_MOD_MSG( LOGMOD_CANDY, 0,  __VA_ARGS__ )
#define LOG_CANDY_INFO( ... ) LOG_MOD_MSG( LOGMOD_CANDY, 1, __VA_ARGS__ )
#define LOG_CANDY_WARN( ... ) LOG_MOD_MSG( LOGMOD_CANDY, 2, __VA_ARGS__ )
#define LOG_CANDY_TRAFFIC( ... ) LOG_MOD_MSG( LOGMOD_CANDY, 3, __VA_ARGS__ )
#define LOG_CANDY_DETAIL( ... ) LOG_MOD_MSG( LOGMOD_CANDY, 4, __VA_ARGS__ )
#define LOG_CANDY_DETAIL2( ... ) LOG_MOD_MSG( LOGMOD_CANDY, 5, __VA_ARGS__ )
#define LOG_CANDY_UNIMP LOG_CANDY_ERROR("UNIMPLEMENTED!\n");

#define LOG_RENDER_ERROR( ... ) LOG_MOD_MSG( LOGMOD_RENDER, 0,  __VA_ARGS__ )
#define LOG_RENDER_INFO( ... ) LOG_MOD_MSG( LOGMOD_RENDER, 1, __VA_ARGS__ )
#define LOG_RENDER_WARN( ... ) LOG_MOD_MSG( LOGMOD_RENDER, 2, __VA_ARGS__ )
#define LOG_RENDER_TRAFFIC( ... ) LOG_MOD_MSG( LOGMOD_RENDER, 3, __VA_ARGS__ )
#define LOG_RENDER_DETAIL( ... ) LOG_MOD_MSG( LOGMOD_RENDER, 4, __VA_ARGS__ )
#define LOG_RENDER_DETAIL2( ... ) LOG_MOD_MSG( LOGMOD_RENDER, 5, __VA_ARGS__ )
#define LOG_RENDER_UNIMP LOG_RENDER_ERROR("UNIMPLEMENTED!\n");

#define LOG_FILE_ERROR( ... ) LOG_MOD_MSG( LOGMOD_FILE, 0,  __VA_ARGS__ )
#define LOG_FILE_INFO( ... ) LOG_MOD_MSG( LOGMOD_FILE, 1, __VA_ARGS__ )
#define LOG_FILE_WARN( ... ) LOG_MOD_MSG( LOGMOD_FILE, 2, __VA_ARGS__ )
#define LOG_FILE_TRAFFIC( ... ) LOG_MOD_MSG( LOGMOD_FILE, 3, __VA_ARGS__ )
#define LOG_FILE_DETAIL( ... ) LOG_MOD_MSG( LOGMOD_FILE, 4, __VA_ARGS__ )
#define LOG_FILE_DETAIL2( ... ) LOG_MOD_MSG( LOGMOD_FILE, 5, __VA_ARGS__ )
#define LOG_FILE_UNIMP LOG_FILE_ERROR("UNIMPLEMENTED!\n");

#define LOG_GEN_ERROR( ... ) LOG_MOD_MSG( LOGMOD_GEN, 0,  __VA_ARGS__ )
#define LOG_GEN_INFO( ... ) LOG_MOD_MSG( LOGMOD_GEN, 1, __VA_ARGS__ )
#define LOG_GEN_WARN( ... ) LOG_MOD_MSG( LOGMOD_GEN, 2, __VA_ARGS__ )
#define LOG_GEN_TRAFFIC( ... ) LOG_MOD_MSG( LOGMOD_GEN, 3, __VA_ARGS__ )
#define LOG_GEN_DETAIL( ... ) LOG_MOD_MSG( LOGMOD_GEN, 4, __VA_ARGS__ )
#define LOG_GEN_DETAIL2( ... ) LOG_MOD_MSG( LOGMOD_GEN, 5, __VA_ARGS__ )
#define LOG_UNIMP LOG_GEN_ERROR("UNIMPLEMENTED!\n");

#define LOG_FLG_ERROR( ... ) LOG_MOD_MSG( LOGMOD_FLG, 0,  __VA_ARGS__ )
#define LOG_FLG_INFO( ... ) LOG_MOD_MSG( LOGMOD_FLG, 1, __VA_ARGS__ )
#define LOG_FLG_WARN( ... ) LOG_MOD_MSG( LOGMOD_FLG, 2, __VA_ARGS__ )
#define LOG_FLG_TRAFFIC( ... ) LOG_MOD_MSG( LOGMOD_FLG, 3, __VA_ARGS__ )
#define LOG_FLG_DETAIL( ... ) LOG_MOD_MSG( LOGMOD_FLG, 4, __VA_ARGS__ )
#define LOG_FLG_DETAIL2( ... ) LOG_MOD_MSG( LOGMOD_FLG, 5, __VA_ARGS__ )

#define LOG_HTTP_ERROR( ... ) LOG_MOD_MSG( LOGMOD_HTTP, 0,  __VA_ARGS__ )
#define LOG_HTTP_INFO( ... ) LOG_MOD_MSG( LOGMOD_HTTP, 1, __VA_ARGS__ )
#define LOG_HTTP_WARN( ... ) LOG_MOD_MSG( LOGMOD_HTTP, 2, __VA_ARGS__ )
#define LOG_HTTP_TRAFFIC( ... ) LOG_MOD_MSG( LOGMOD_HTTP, 3, __VA_ARGS__ )
#define LOG_HTTP_DETAIL( ... ) LOG_MOD_MSG( LOGMOD_HTTP, 4, __VA_ARGS__ )
#define LOG_HTTP_DETAIL2( ... ) LOG_MOD_MSG( LOGMOD_HTTP, 5, __VA_ARGS__ )

#define LOG_HOLE_ERROR( ... ) LOG_MOD_MSG( LOGMOD_HOLE, 0,  __VA_ARGS__ )
#define LOG_HOLE_INFO( ... ) LOG_MOD_MSG( LOGMOD_HOLE, 1, __VA_ARGS__ )
#define LOG_HOLE_WARN( ... ) LOG_MOD_MSG( LOGMOD_HOLE, 2, __VA_ARGS__ )
#define LOG_HOLE_TRAFFIC( ... ) LOG_MOD_MSG( LOGMOD_HOLE, 3, __VA_ARGS__ )
#define LOG_HOLE_DETAIL( ... ) LOG_MOD_MSG( LOGMOD_HOLE, 4, __VA_ARGS__ )
#define LOG_HOLE_DETAIL2( ... ) LOG_MOD_MSG( LOGMOD_HOLE, 5, __VA_ARGS__ )

#define LOG_GUI_ERROR( ... ) LOG_MOD_MSG( LOGMOD_GUI, 0,  __VA_ARGS__ )
#define LOG_GUI_INFO( ... ) LOG_MOD_MSG( LOGMOD_GUI, 1, __VA_ARGS__ )
#define LOG_GUI_WARN( ... ) LOG_MOD_MSG( LOGMOD_GUI, 2, __VA_ARGS__ )
#define LOG_GUI_TRAFFIC( ... ) LOG_MOD_MSG( LOGMOD_GUI, 3, __VA_ARGS__ )
#define LOG_GUI_DETAIL( ... ) LOG_MOD_MSG( LOGMOD_GUI, 4, __VA_ARGS__ )
#define LOG_GUI_DETAIL2( ... ) LOG_MOD_MSG( LOGMOD_GUI, 5, __VA_ARGS__ )

#define LOG_BB_ERROR( ... ) LOG_MOD_MSG( LOGMOD_BB, 0,  __VA_ARGS__ )
#define LOG_BB_INFO( ... ) LOG_MOD_MSG( LOGMOD_BB, 1, __VA_ARGS__ )
#define LOG_BB_WARN( ... ) LOG_MOD_MSG( LOGMOD_BB, 2, __VA_ARGS__ )
#define LOG_BB_TRAFFIC( ... ) LOG_MOD_MSG( LOGMOD_BB, 3, __VA_ARGS__ )
#define LOG_BB_DETAIL( ... ) LOG_MOD_MSG( LOGMOD_BB, 4, __VA_ARGS__ )
#define LOG_BB_DETAIL2( ... ) LOG_MOD_MSG( LOGMOD_BB, 5, __VA_ARGS__ )

#define LOG_PLAYER_ERROR( ... ) LOG_MOD_MSG( LOGMOD_PLAYER, 0,  __VA_ARGS__ )
#define LOG_PLAYER_INFO( ... ) LOG_MOD_MSG( LOGMOD_PLAYER, 1, __VA_ARGS__ )
#define LOG_PLAYER_WARN( ... ) LOG_MOD_MSG( LOGMOD_PLAYER, 2, __VA_ARGS__ )
#define LOG_PLAYER_TRAFFIC( ... ) LOG_MOD_MSG( LOGMOD_PLAYER, 3, __VA_ARGS__ )
#define LOG_PLAYER_DETAIL( ... ) LOG_MOD_MSG( LOGMOD_PLAYER, 4, __VA_ARGS__ )
#define LOG_PLAYER_DETAIL2( ... ) LOG_MOD_MSG( LOGMOD_PLAYER, 5, __VA_ARGS__ )

#endif //_DEFAULT_LOG_H_
