/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Sub_Timing_h_
#define _Sub_Timing_h_

#include <climits>

class SubTiming
{
public:
	SubTiming( long _start = LONG_MIN, long _dur = 0 ) : start(start), dur(dur) {};
	~SubTiming() {};

	SubTiming &operator=( SubTiming &st ) {
		start = st.start;
		dur = st.dur;
		return *this;
	};

	void AddToStart( long val ) { start += val; };
	void SetStart( long _start ) { start = _start; };
	long GetStart() const { return start; };
	void SetDur( long _dur ) { dur = _dur; };
	long GetDur() const { return dur; };
	void SetEnd( long end ) { dur = end - start; };
	long GetEnd() const { return start + dur; };

	void Reset() { start = LONG_MIN; };
	bool IsSet() const { return start != LONG_MIN; };

	void operator=( const SubTiming &other ) { start = other.start; dur = other.dur; };

private:
	long start, dur;
};

#endif //_Sub_Timing_h_
