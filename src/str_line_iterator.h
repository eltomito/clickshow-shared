/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Str_Line_Iterator_h
#define _Str_Line_Iterator_h 1

#include <string>

class str_line_iterator
{
public:
	str_line_iterator( const char *_str ) : str(_str), pos(0) {};
	str_line_iterator() : str(NULL), pos(-1) {};
	~str_line_iterator() {};

	void operator=( const str_line_iterator &other ) { str = other.str; pos = other.pos; };
	str_line_iterator &operator++();

	const std::string &operator *();
	std::string get() const;
	int offset() const { return pos; };
	int size() const;

	bool operator<( const str_line_iterator &other ) const { return (( str == other.str ) && ( pos < other.pos )); };
	bool operator>( const str_line_iterator &other ) const { return (( str == other.str ) && ( pos > other.pos )); };
	bool operator<=( const str_line_iterator &other ) const { return (( str == other.str ) && ( pos <= other.pos )); };
	bool operator>=( const str_line_iterator &other ) const { return (( str == other.str ) && ( pos >= other.pos )); };

	bool operator==( const str_line_iterator &other ) const;
	bool operator!=( const str_line_iterator &other ) const { return !( *this == other ); };

private:
	const char *str;
	int pos;
	std::string line;
};

#endif //Line_Iterator
