/*
 * This file is part of ClickShow
 * Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "err_desc.h"
#include "str_utils.h"

const char *ErrDesc::loc_type_names[NUM_LOC_TYPES + 1] = {"location","line","subtitle"};

void ErrDesc::ToString( std::string &dst ) const
{
	int loc = loc_type;
	if(( loc < 0 )||( loc >= NUM_LOC_TYPES )) {
		loc = 0;
	} else {
		++loc;
	}
	StrUtils::Printf( dst, "%s %i: %s", loc_type_names[loc], where, Errr::Message(err) );
	if( !line.empty() ) {
		dst.append(": \"");
		dst.append( line );
		dst.append("\"");
	}
}

void ErrDesc::DescsToString( const ErrDescsT &errs, std::string &dst )
{
	int l = errs.size();
	for( int i = 0; i < l; ++i ) {
		errs[i].ToString( dst );
		dst.push_back('\n');
	}
	if( l > 0 ) {
		dst.erase( dst.size() - 1 );
	}
}
