#include "line_iterator.h"

const_line_iterator &const_line_iterator::operator++()
{
	if( pos < 0 ) { return *this; };
	size_t p = str->find('\n', pos );
	if( p == std::string::npos ) {
		pos = -1;
	} else {
		++p;
		if( p >= str->size() ) {
			pos =-1;
		} else {
			pos = p;
		}
	}
	return *this;
}

std::string const_line_iterator::get() const
{
	if( pos < 0 ) { return ""; };
	size_t p = str->find('\n', pos );
	if( p == std::string::npos ) {
		return str->substr( pos );
	}
	return str->substr( pos, p - pos );
}

const std::string &const_line_iterator::operator *()
{
	line = get();
	return line;
}

bool const_line_iterator::operator==( const const_line_iterator &other ) const
{
	if(( str == NULL )||( other.str == NULL )) {
		return ( pos == -1)&&( other.pos == -1 );
	}
	return (( str->c_str() == other.str->c_str() ) && ( pos == other.pos ));
};
