#ifndef _serialize_double_h_
#define _serialize_double_h_ 1

#include <cfloat>
#include <cmath>
#include <limits>
#include <boost/math/special_functions/fpclassify.hpp>

//--- assumptions --
#define DBL_EXP_BYTES 2
#define DBL_SGD_BYTES 7

#if FLT_RADIX != 2
#error YOUR SYSTEM IS TOO WEIRD! SORRY! :( (The float radix must be 2)
#endif

#if ( DBL_MANT_DIG / 8 ) > DBL_SGD_BYTES
#error SORRY, I CAN ONLY SERIALIZE DOUBLES WITH SIGNIFICANTS UP TO 7 BYTES WIDE
#endif

#if DBL_MAX_EXP >= ( 1 << (DBL_EXP_BYTES * 8) )
#error SORRY! THE EXPONENT HAS TO FIT IN 2 BYTES!
#endif

//pre-calculated significand multiplier
static const double SERIALIZED_DOUBLE_DSIG_MULT = (double)((long long)1 << DBL_MANT_DIG);

//the size of a serialized double
#define DBL_SERIALIZED_BYTES ((size_t)(DBL_EXP_BYTES + DBL_SGD_BYTES + 1))

class SerializeDouble
{
public:
	static size_t serialize_double( double d, unsigned char *dst, size_t max_size )
	{
		if( max_size < DBL_SERIALIZED_BYTES ) return 0;
	
		unsigned char sig = 0;
		unsigned int iexp = 0;
		double dsgd = 0;
		unsigned long long llsgd = 0.0;
	
		if( boost::math::isnan( d ) ) {
			sig = 2;
		} else if( boost::math::isinf( d ) ) {
			sig = ( d < 0 ) ? 3 : 4;
		} else {
			if( d >= 0 ) {
				sig = 0;
			} else {
				sig = 1;
				d = -d;
			}
			dsgd = frexp( d, (int*)&iexp );
			llsgd = (unsigned long long)( dsgd * SERIALIZED_DOUBLE_DSIG_MULT );
		}
		dst[0] = sig;
		dst[1] = iexp & 0xff;
		dst[2] = (iexp >> 8) & 0xff;
		dst += 3;
	
		for( int i = 0; i < DBL_SGD_BYTES; ++i ) {
			dst[0] = llsgd & 0xff;
			llsgd = llsgd >> 8;
			++dst;
		}
		return DBL_SERIALIZED_BYTES;
	}
	
	static double deserialize_double( const unsigned char *src, size_t src_len, size_t &bytes_used )
	{
		if( src_len < DBL_SERIALIZED_BYTES ) {
			bytes_used = 0;
			return std::numeric_limits<double>::quiet_NaN();
		}
		unsigned char sig = src[0];
		if( sig > 4 ) {
			bytes_used = 0;
			return std::numeric_limits<double>::quiet_NaN();
		}
		bytes_used = DBL_SERIALIZED_BYTES;
		double d = 0.0;
		int iexp = 0;
		unsigned long long llsgd = 0;
		double dsgd;
		switch( sig ) {
			case 2:
				d = std::numeric_limits<double>::quiet_NaN();
			break;
			case 3:
				d = (double) DBL_MIN / (double) 0.00001;
			break;
			case 4:
				d = (double) DBL_MAX / (double) 0.00001;
			break;
			default:
				iexp = ( ((unsigned int)src[2]) << 8 ) + (int)src[1];
				if( iexp > 32768 ) {
					iexp = iexp - 65536;
				}
				src += 3 + DBL_SGD_BYTES - 1;
				for( int i = 0; i < DBL_SGD_BYTES; ++i ) {
					llsgd = llsgd << 8;
					llsgd += (unsigned long long)src[0];
					--src;
				}
				dsgd = (double)llsgd;
				dsgd = dsgd / SERIALIZED_DOUBLE_DSIG_MULT;
				d = ldexp( dsgd, iexp );
				if( sig == 1 ) {
					d = -d;
				}
			break;
		}
		return d;
	}
};

#endif //_serialize_double_h_
